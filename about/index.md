---
layout: page
title: About
class: 'post'
navigation: True
logo: 'assets/images/ghost.png'
current: about
---

One once said to try new things.
Blogging's been done; and now we have pages.
Hello world.

I have for now adopted a Jekyll version of Ghost's old theme as provided by gracious GitLab.
My laziness can be gauged by my willingness to deviate from the defaults.
