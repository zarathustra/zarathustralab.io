---
layout: post
comments: true
title: The Intentional Life Hack 
date:   2020-01-15 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- personal development
- self help
- spirituality
- logical fallacy
- rationality
- intentional
- life hack
- do nothing
---

Hello. You've likely heard that "[Humans weren't designed to be rational"](https://qz.com/922924/humans-werent-designed-to-be-rational-and-we-are-better-thinkers-for-it/)" (moreover instincts and emotions make for good heuristics: trust them).  

And [our choices are apparent](https://futurism.com/neuroscientists-predict-decisions-11-seconds) via [fMRI](https://en.wikipedia.org/wiki/Functional_magnetic_resonance_imaging) prior to our conscious awareness of the "decision procedure".     
(One common misinterpretation of these studies is due to not recognizing that there will of course be "unconscious" parts of our brain's decision procedure.  Are the fMRI machines reading our "true subconcsious decision" before we "consciously fool ourselves that we're making a choice" or are the fMRI machines peering between the shades to "look at our brain in the process of making the choice" -- and lo-and-behold, looking at the process the outcome can be predicted.) 


You can also find abundant advice on "[Rational Decision Making](https://blog.hubspot.com/marketing/rational-decision-making)": if we compile all possible relevant facts and follow a algorithmic decision procedure, then we bypass our semi-rational heuristics and make solid choices.  
But can you do this for _every single choice_?   
Moreover, can you be sure you haven't cut any corners? Maybe your brain is subconsciously working out the conclusion for you, letting you know where you can play loose with the logic and the data.  

You can survey long lists of [Cognitive Biases](https://en.wikipedia.org/wiki/List_of_cognitive_biases) and [Logical Fallacies](https://en.wikipedia.org/wiki/List_of_fallacies) that you might be making.   
I wonder if we'll learn to detect via fMRI or next-gen brain imaging which cognitive biases will be used before the fact. Or in other words to catch cognitive biases red-handed in the act!   
[Confabulation](https://www.edge.org/response-detail/11513) is an even cooler concept. Basically: how do you know the stories you tell yourself are _actually true_?  Let's be generous and say you generally know what you did, but do you _actually know why you did it_?  

Enter split-brain patient experiments where right and left brain hemispheres have been disconnected. The doctor asks the patient's left hemisphere to grab the glass of water and then asks the right hemisphere "why did you grab the glass of water?". -- "Because I was thirsty."   
🤔 Hmm 🤔  
Something's fishy, ain't it?

The patient confidently explains vis actions, yet we have clear evidence ve's wrong.  
(A question occurs to me now: when did the patient generate vis story?  Did the patient's ignorant hemisphere start generating the intentional-narrative as the arm reached for the glass? Or did the patient only generate the confabulation when asked "why"? -- If you know the answer, please tell me!)  

Now you may wonder whether this occurs normally: is it the tip of the iceberg?  

Apparently it is. Two examples:  
> Women are less likely to call their fathers (but equally likely to call their mothers) during the fertile phase of their menstrual cycle, reflecting a means of incest avoidance.  

> Students indicate a closer bond to their mother when holding hot coffee versus iced coffee, reflecting the metaphor of a "warm" relationship.  

How then can we trust ourselves to make truly "rational decisions" even by following a careful procedure?  
Lists of Pros and Cons have so many points-of-entry for errors -- how do we determine what makes it on the list? What word do we use? What unbiased measure do we use to "weigh the pros and cons"?  [oro-oro](https://www.youtube.com/watch?v=Ajxc31v3c24)   
How about the "[7-Step Process for Making Logical Decisions](https://www.youtube.com/watch?v=Ajxc31v3c24)" linked to above?  It doesn't fare much better.  Of course biases can slip in when defining the problem -- maybe I'm making the decision in a foul smeling room and use exaggeratedly negative terminology!  Brainstorming possible solutions doesn't fare much better, unless as with Pros and Cons, I have a systematic method for generating all possible solutions and a clear, objective metric for measuring them.  

These processes may help us to make "more rational" decisions, pending the definition of what it means for a decision to be "more or less rational" (rather than a black-and-white "either rational or irrational".

---

So what is this hack to live a rational life?

First we need to do what any good AI would do: fiddle around with the definitions. (Nyan~)

Let's start with an intentional life, for how can I live a rational life otherwise? 

The __intentional life__ is one in which 100% of your actions are taken in alignment with prior conscious intention. 

First quip: does this involve physiological processes such as digestion, sweating, [heart beating](https://skepticmeditations.com/2015/05/17/can-yogis-stop-their-heart/)[1](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3573539/), breathing, etc?   
To start let's focus on the external actions :-)

Are you ready for the hack?

Step 1: stop doing anything.

Step 2: pat yourself on the back.


[![When somebody asks a Zen monk what do you do in meditation, he replies, 
"I do nothing. I just sit empty and that's all I do."]({{"/assets/images/Monk.jpg"}})](https://templeilluminatus.com/forum/topics/zen-meditation-lesson?groupUrl=meditationthezenroom)

When somebody asks a Zen monk what do you do in meditation, he replies, 
> "I do nothing. I just sit empty and that's all I do."

See the Zen monk gets it: he's doing precisely what he consciously intends to do.  

And to rub it in further: he probably holds a worldview and value-system in which this is a very rational course of action.

Now, now, you may say you want to live an Intentional life DOING THINGS!   
-- other than nothing

---

Good news: this is only the beginning. 

Good news: at least you're not [reinforcing your bad habits](https://shellypruittjohnson.wordpress.com/2018/01/27/the-very-precise-science-of-reinforcing-a-bad-habit/).

Good news: as you watch your [thoughts float by like clouds in the sky](https://mindful.stanford.edu/2015/06/thought-clouds/), you're reinforcing the habit of inaction.

The idea is very simple.

Now that you are living intentionally and rationally, you can build up a foundation of carefully thought out intentional actions.  Ideally you have a wealthy patron and cook so that you don't have to worry about life-sustenance as you learn how to live again.


![Swoosh, swoosh]({{"/assets/images/monk_leaves.jpg"}})

You can start with simple, necessary actions such as sweeping leaves in the forest.  
-- Or fuck the wealthy patron and maid, start by hyper-intentionally cleaning your dishes and scrubbing your toilet.  
Rationally someone has got to do these actions. ☑️  

Let any other spontaneous urges or habits go -- ignore them and continue sponging that pan.  

Take every single step like you mean it.  
Don't permit a single un-intentional scratch of an itch!  

... 

Eventually the intentional foundations build up, and you have thoroughly trained your brain to "only take action following conscious intention".  

Okay, so this probably works quite well for daily chores.  
The rationality of the actions is downright obvious, so there's not too much room for cognitive biases and logical fallacies to sneak in (unless you're seeking excuses not to do them, but after a few decades of building your intentional life from scratch meditating on chores, you'll have no problem with this!).

Gradually you will re-develop yourself and go about home, family, social, and professional life 'as before' only intentionally.  
Hopefully the process becomes naturalized and you don't look like a stiff human-puppet (drawing your own strings).

---

The rationality is a bit more tricky.  
Sure, you got practice observing urges and not acting on them, but can you ignore all cognitive biases?  
Probably not.  

It might be good to formalize all reasoning you do in an Interactive Theorem Proving system such as [The Coq Proof Assistant](https://coq.inria.fr/) or [Lean Theorem Prover](https://leanprover.github.io/).  
Learn to set-up all reasoning problems clearly so you don't mess up questions like:  
> Bob is taller than sally. Alice is shorter than Bob. Is Alice shorter than sally?

Reframe it: Bob > sally. Bob > Alice. Sally > Alice? -- __Inconclusive!__

Load programs of careful, formal reason into your brain, into your mind.  
And as you are an intentionally acting mind now, do not allow yourself to proceed with any informal reasoning.  
 
This should largely save you from the evil [Logical Fallacies](https://en.wikipedia.org/wiki/List_of_fallacies).

Cognitive biases are iffier.  
Many can be dealt with simply by formally modeling the situation prior to drawing any conclusions.  Namely, intentionality must be shifted from external actions to mental actions.  

Many can be dealt with by developing case-by-case intentional gymnastics to counteract the temptation.  

Maybe in the spirit of consciousness hacking we can program neurofeedback equipment to ping every time "initiation of cognitive bias" is observed?

Just remember: 
> When in doubt, do nothing.


 
