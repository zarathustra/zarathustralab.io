---
layout: post
comments: true
title: Writing
date:   2018-06-21 11:51:00 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- writing
- stories
- bukowski
- out there
- life
---

Back to the topic of Writing. I started my <a href="https://zariuq.wordpress.com/">prior blog</a> with the intent to put ideas out there, to entangle my thought with that of others. 
Who am I to say what will catch and with whom? How will anything get started if I don't put the content out? It had some good traction for me, and there was some engagement, some fun when e-friends would look at parts and enjoy the ideas presented. Good for getting stuff off of my chest. In some ways it forced -- writing publicly -- clarity more than writing in me diary.

Yet I have not written a blog post yet this year. 

The energy had faded. 

I had many quite interesting thougths and ideas, all on the tip of the tongue waiting to be finalized.

Perhaps I'm waiting to really "get there" and then I can wrap things up. Somehow done with writing impressions and thought-chunks on the path.

I note I wrote (rather dictated to Benben Daddad Goertzelius) a <a href="http://goertzel.org/family/zar/zar.html">bunch of stories</a> when younger. for much of my life I yearned back to this day of inspired creativity. 

Even tried to force it a bit. Not getting too far, or taking the story writing quite seriously. 
Perhaps in similar spirit to [Games](/games).

Once again, I'm inspired a bit. Perhaps this is thanks to Bukowski and his <a href="https://www.goodreads.com/review/show/2430767297">Women</a>. 

The dude has a marvelous way of inspiring one to "be oneself". 

Everyone says "just be yourself" and life will solve itself for you on a silver platter. Charlie boi Hank shows you how. 

Le Big Howski. 

Underneath all the complaints about how life sucks, the difficulties, the rats that seem more decent than your flophouse mates (ok, this is in reference to a Poem in "Last Day of The Earth Poems"), the beer that won't stay down, there's an unceasing acceptance. An unbending determination to keep existing so long as you can (cuz, hey, maybe you can be child-raped by an 18-year old on your 80th birthday -- never know!) Somehow taking life as it comes, bitching all the way. The light beyond giving up on fixing things. 

Funny. When I heard the name, I generally thought of posh 'n pompous poetry, that of the sort that Bukowski claims to despise. Ironic he came off as that which he hates, huh? Maybe that's what it is. In reading about existentially supportive mirthful misery one feels one has now come to tems with "the down and out". One's guilt for all the first-world-problems they struggle with is eased by the Big Throbbing Bukowski Salve. He ends up making rich, spoiled, artsy kids feel good about themselves. Women feel strong for putting up with a smelly pos like him, when they're actually equally fucked up in different ways. If we're all fucked up, then we're equal, right? :)

Perhaps I am instilled with this vibrant, wry hope. This sense that if I merely put out my impressions, my life, then I'll be all big and popular too. 

If there's anything I learned from reading my self-help bloggers and Women, it's that blogging does miracles. Maybe he'd be a blogger nowadays. 
It seems to work for <a href="https://www.stevepavlina.com">Mr. Steve Pavlina</a>.


Put yourself out there. Write about the charms, the strong emotions, your dreams, your experiences. Put your soul into it. 
And the women who respond, who get heated up inside thinking about, relating to your spirit, will find you. 
After-all, one can't really go through more than 5000 women anyway. 
Talking to them in bars.
On the street.
At work.
... 
...
...
All are a poor way to play the numbers game. Better cast a net that captures them based on that ineffable light in your eyes. 
Find the truly special ones. 
Find the ones who truly have mutual growth experiences in mind.
Find the healthy vegans. 

What better way to find them than publish? Putting oneself out there. 
- Telling them exactly the beautiful experiences they'll find. 
- Reassuring them you know how to let go -- so they can feel absolutely free with you. 

Clearly rambling to one's hearts content is the way to bring women to one's dick, tongue, fingers, and arms. 
 Bring their energy into yours.

Life lesson #2: to be oneself just follow your dick. 

It'll take you on a marvelous adventure. 

Eh, there's a strange, mysterious power in writing so that others may read. 
As is there that strange power in conversing with others. 

Here I am learning to live 'n love with my fellow peeps on this here Planet as we face the Singularity and creation of minds beyond our own. 
Simply writing off letter by letter, mixing energies, discussing ideas, -- <a href="https://arxiv.org/abs/1505.06366">making sense</a>.


- - - 
Edit 
- - -

Another funny remark on writing. 
Sometimes I feel there's not so much to say on a topic. 
Better just code. Experiment. Or get working otherwise.
Yet now here I sit writing rather than working. Connecting. 

 
