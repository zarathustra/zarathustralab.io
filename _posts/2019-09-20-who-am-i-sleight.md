---
layout: post
comments: true
title: The "Who am I?" Sleight of Hand 
date:   2019-09-20 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- personal development
- self help
- spirituality
- logical fallacy
- homunculus
---

"Who am I?" is a method of spiritual guidance (akin to finding polynomial roots?) whereby one successively cuts down answers until the answer remains.

It was popularized by [Ramana Maharshi](https://www.youtube.com/watch?v=J2VcZWypI9c) (to the best of my off-the-cuff knowledge).  
However, as I have not read Ramana's works or listened to his talks directly, perhaps he does not use the meditation technique in the rhetorical manner I write of here.  
:)

The same technique was used by Nisargadatta Maharaj in [I AM THAT](https://www.goodreads.com/book/show/299869.I_Am_That).

It goes something like this:

Bob> Who are you?  
Zar> I am Zar.  
Bob> "Zar"? Are you ['Z', 'a', 'r'], the sequence of letters?  
Zar> No, that's just my name. I am this person standing here.  
Bob> Oh, so were you this same person at 3 years old? Will you be as much Zar if you lose an arm?  
Zar> Well, yes, I kind of will. I am my own unique complex of thoughts and feelings.  
Bob> Try this meditation technique. Don't you see that you still exist even without thoughts or feelings?  
Zar> Hmm, you're right. What is my Élan vital? My eternal, constant, homunculus-like essence?   
Bob> You see, it's awareness. Pure awareness. 

The conclusion then is that I am simply consciousness or awareness -- not my thoughts, not my experiences, not any particular state of being.


To exemplify the sleight of hand here, take Mx Buttocks below (courtesy of WolframAlpha):


![My life is the curve!]({{"/assets/images/Buttocks.png"}})

Bob>      Who are you?  
Buttocks> I am Buttocks.   
Bob>      ['B', 'u', 't', 't', 'o', 'c', 'k', 's']?   
Buttocks> No, that's just the name humans assigned to me. I am this digit here (@ \\(x = 0\\)), \\(3\\), you see?   
Bob> But weren't you \\(-1\\) just \\(\sqrt 2\\) ago?  
Buttocks> Well, yes, my value was different then.  
Bob> Try moving on to \\(1\\). Do you see that you are now \\(0\\), nothing?  
Buttocks> Hmm, you're right. My nature does not depend on any meaningful value.  
Bob> You see, you are pure awareness. You are simply a drop in the sea of Consciousness (-- err, \\(\mathbb{R}^2\\)).  

Instead of internal and external experiences, I've crudely used simple values. 

The answer Bob arrives at is even correct, as in the human case (to the best of my knowledge).  

-- -- -- 

Why call it a __sleight of hand__ then?  

Well, because the flow of the dialog creates the impression that all other answers have been ruled out as "lacking", leaving the one core, true answer.  
When the question is actually fairly ill-posed.  
Bob and Buttocks proceed as if Buttocks is either a fixed value or nothing more than "of \\(\mathbb{R}^2\\) (the (x,y) plane)". 

There's a fairly good answer that fits all possible values Buttocks can explore or inhabit (and rules out almost all possible values of Consciousness (\\(\mathbb{R}^2\\)):

Buttocks '__is__' \\(x^4 - 4x^2 + 3\\). 


Bob's rhetoric, this "Who Am I?" game, is in a sort of [Straw Man](https://en.wikipedia.org/wiki/Straw_man) style arguing against a fixed essence model of self. 

Actually, rather than shifting how we view "self" in light of the "fixed essence of self" model falling apart, Bob keeps using the "I am that which is constant throughout all of my existence, all of me being me" 'definition' of self.

Given most of "Zar" (and Buttocks) changes over time (over x from \\(- \infty \\)), most of Zar other than the core Awareness or Beingness is thus "not Zar". 

Unlike for Zar, for Buttocks we have a nice, clean constant in addition to being Conscious (of the \\((x,y)\\)-plane): namely, \\(Buttocks =  x^4 - 4x^2 + 3\\).

Thus we can clearly say what Buttocks is, even as his value goes up and down, wheee.

Has Bob ruled this out for __you__, for humans yet?   
Not really. The space becomes 4-dimensional (or [11-dimensional](https://www.youtube.com/watch?v=p4Gotl9vRGs) if you go with M-theory).   
Moreover, rather than one point at each time, you are a complex configuration of about ~ [\\(7 * 10^{27}\\) atoms](https://www.thoughtco.com/how-many-atoms-are-in-human-body-603872) (which are still not 'points').    

What Bob has ascertained or argued is that Awareness or Consciousness is a constant of my existence.  
(Sri Nisargadatta Maharaj has claimed that we can learn to be aware through sleep as well, so that little caveat melts away.)

Whoever I am, I am conscious. 

-- -- --

### What am I?

"Who" am I is actually a misleading question from the outset (often with the aim at bamboozling you into identifying with "the observer" or "the witness").

To pry pedantically into this question as if "Awareness" is some entity observing everything going on inside and out (that is not "me") reeks of the [Homunculus Theory of Self](https://en.wikipedia.org/wiki/Homunculus_argument):

![Courtesy of Wikipedia]({{"/assets/images/homunculus.png"}})

This idea that there is some "true me" inside watching everything.  

This property-less true witness (Atman?) is almost a refined Homunculus in the "[God of the gaps](https://en.wikipedia.org/wiki/God_of_the_gaps)" style where the essence of "self"ness remains despite any actual properties of the inner-"I" being culled.

One can, as some Buddhists do, take the next step an conclude "I am no one" (as I can find no unchanging identity) rather than to "identify as Consciousness" (which isn't much of a self anyway). 

Or one can switch over to "what am I?" and start the investigations.  
Does answering to full satisfaction require a __Theory of Everything__?

Even if there is a clear answer to "what I am" as in the case of \\(Buttocks =  x^4 - 4x^2 + 3\\), can we __find__ it?

Moreover, Buttocks is entirely self-contained. I interact with my environment.   
So one must answer "what is my environment?" and "how do I interact with my environment?" as well.   
Or is there some constant fixed point to my being regardless the environment (setting aside icepicks to the cranium)?

If we know that Buttocks is a polynomial, then [Lagrange interpolation](https://en.wikipedia.org/wiki/Lagrange_polynomial) will provide the exact answer given 5 unique points (samples).  
If we know the degree of Buttocks, then we can even know how much data we need to collect to know "what Buttocks truly is".  
(-- Ok, if Buttocks can freely move around its curve, perhaps we need the function determining this movement too! Egads! Now it's complicated like us?)

What if we extend polynomials to [analytic functions](https://en.wikipedia.org/wiki/Analytic_function) or [power series](https://en.wikipedia.org/wiki/Power_series)?  

For example, now we can represent any real number in \\(\mathbb{R}\\).   
For example \\(Buttocks = 3.0401\\) or \\(3 + 0x + 4x^2 + 0x^3 + 1 x^4\\).   
The exact value being found for \\(x = \frac{1}{10}\\).

And then there's his friend Pi, who exists as \\(\pi = 3 + 1x + 4x^2 + 1x^3 + 5x^4 + 9x^5 + 2x^6 + \cdots\\), which is well defined given \\(|x| < 1\\) and is computable.  
Yet what number of samples would be needed to be \\(100\%\\) sure of what Pi is?  
If you compute digits in order, then there are problems like:   
* \\(3.141592\\)  
* \\(3.141591\\)  
You will mess up if you thought 6 digits (samples) was enough. 

Of course, we don't really have to get infinite for problems.   
Provided N may be __very, very large__ relative to the amount of data/experience we can gather, we are liable to run into similar difficulties. 

What does this mean?

Well, Buttocks shows that "change" does not mean there is no fixedness or constancy to "who I am" aside from "being conscious or aware".  
Moreover, the inability to interpolate or compactly describe __exactly__ what "I am" does not mean there isn't anything either.  
(Nor, of course, does this mean there necessarily is.)  

### What next?

One response is to loosen up about what exactly "self" or "I" am.  
These folk using this fallacious rhetoric are still Absolutists with sticks up their asses.  
Perhaps embrace [Fuzzy Truth Values](https://en.wikipedia.org/wiki/Fuzzy_logic) and ask to what degree various self-models seem to wark?  

What if I am a different person at different points in my life?  
There are prominant some [Strange Loops](https://www.goodreads.com/review/show/2982799734) and [Attractors](https://en.wikipedia.org/wiki/Attractor) from 13 to 19 and then somewhat different ones from 22 to 25, 25 to 28 and from 28-29.  
They more-or-less dwarf the commonalities spanning the epochs of Zarathustra: I am better modeled as a progression of 6 related "selves".

It's worth recalling that, as Alan Watts points out in [Out of Your Mind](https://www.goodreads.com/review/show/2412719677), such sleights of hand as this "Who am I?" game can be may not be done literally.  
The "Guru as Trickster" idea indicates the focus may be more on busting faulty fixed or cultural concepts of identity than on "actually finding true identity".  
-- In which case I am merely complaining about people who missed the memo.   
Except, well, if you choose a cryptic style of communication where no one can quite be sure what your point is... **shrug** 


When speaking on Absolute terms, I'm reminded how profound \\(1 + 1 = 2\\) can seem to me.  
Clearly two apples are not identical, and it's hard to specify the exact boundary of the apple on the scale of [quarks](https://en.wikipedia.org/wiki/Quark).  
So is the [truth value](https://en.wikipedia.org/wiki/Truth_value) of \\(1 + 1 = 2\\) always less than \\(1\\) (absolute truth) in __reality__?   
Except while it's hard to specify exactly, the apples are clearly discernible objects on our macro scale.   

But then "Zarathustra" is fairly discernible from likely all other humans on Earth (to someone who knows him adequately).  
So what bearing do these musings on uncertainty and the absolute have on "daily life"?  
Beyond saying "don't be so rigid about identity"..


-- -- --
### Aside on Open Individuality

This Homunculus of Awareness idea has also been called [Open Individualism](https://en.wikipedia.org/wiki/Open_individualism) as seen in this image:

[![I am that I am]({{"/assets/images/open_individualism.png"}})](https://opentheory.net/)

__Empty Individualism__ is like taking every value of Buttocks as its own entity.  
__Closed Individualism__ recognizes Buttocks as the polynomial it is.  
__Open Individualism__ considers the \\((x,y)\\)-plane as the "subject of experience" rather than Buttocks itself.

Is it correct to consider the \\((x,y)\\)-plane as a Subject in and of itself?  
Does Awareness being a constant of existent entities imply Awareness itself is such an entity? Not really.  
As the \\((x,y)\\)-plane being the space in which single-variable polynomials live does not make the \\((x,y)\\)-plane itself such a polynomial.  

(Gee, I love me some anologies.)

Clearly this Empty-Closed-Open Individualism image presents a false-trichotomy.  

Our [microbiome](https://en.wikipedia.org/wiki/Human_microbiome) is full of bacteria, small individual life forms.  
And looking larger than humans, we see that individual humans can make up part of nascent [Global Brains](https://en.wikipedia.org/wiki/Global_brain#Evolutionary_cybernetics). 

The interdependence with our environment makes us not fully closed like Buttocks, but we still get mostly-closed individuals nested inside larger mostly-closed individuals nested inside \\(\cdots\\)

Anyway, so far as I can tell, it's wrong to say that "there is only one subject of experience, __who__ is everyone".  
At this point the "who" has been lost, leaving just awareness.  
The plot has been lost. 

Ah, then we come to fun questions such as "what is subjectless experience?".   
Does all experience or awareness or consciousness involve an "experiencer", an "observer", a "subject"?  
Or is that "experience of being a blank, thoughtless, feelingless [monad](https://en.wikipedia.org/wiki/Monad) of subjective awareness" still a very particular form of experience?  

Adopting a [Panexperientialist](http://www.eoht.info/page/Panexperientialism) or [Panpsychist](https://en.wikipedia.org/wiki/Panpsychism) lens where "there is something it's like to be anything".  
There is some quality of experience to electrons, rocks, etc.  
Is this "experience" worth being called "conscious experience" or "subjective experience"?  Perhaps not. 

-- -- --

In summary: [Help I'm a Rock](https://www.youtube.com/watch?v=Ukbu9dmmzJg).




