---
layout: post
comments: true
title: "Persistent Non-Symbolic Experience"
date:   2020-09-23 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- Awakening
- PNSE
- Personal Development
- Meditation 
---
---

_I have awakened_. 

Hello World 🙂

Only to Location One, perhaps the most basic form.  Apparently this is sufficient.

As promised, it's interesting.  Everything changes, yet everything remains the same. 

---

I was afraid of transitioning, to be honest.  According to rumors and reports, practically anything can happen when one awakens.  One may be blissful no matter what happens to oneself and the world.  'Self' will evaporate as if it never was, desire will fade away, and then one realizes nothing ever needed to be done in the first place. 

Jeez.  DO WE EVEN WANT THAT?!

I wrote extensively on these fears and unappetizing reports of _enlightenment_ in my lengthy [meditation](/meditation) post. 
The fear of `awakening in the wrong way`



Apparently such fears are a big roadblock for curious Seekers and Psychonauts.

I also wrote about being intruged by Jeffery Martin's [Finder's Course](http://finderscourse.com/awakening.php), which claims to be experimentally put together based on interviews and studies of hundreds of awakened people around the globe.  Alas, the price was increasing from the 500$ range to the 3000$ range over the years.  He recently published a new paper: [Clusters of Individuals Experiences form a Continuum of Persistent Non-Symbolic Experiences in Adults](https://digitalcommons.ciis.edu/conscjournal/vol8/iss8/1/).

Then in April, thanks to the Angel of Death inspired lockdowns, Jeffery had the free time to start a next series of experiments: take the most successful methods from the Finder's Course and try to accelerate the process from 3-4 months to 45 days.  All for a price of 120$ as we are now guinea pigs.

Well shucks.  Am I ready to do this to myself, assuming it works?  That's fairly risky... and it seems potentially scammy, but when else will I get the opportunity to try it out?  Here goes nothing ~

---

Enter the [45 Days to Awakening](https://45daystoawakening.com) course.

(I will endeavor not to divulge too many details.)

We had to fill out a few hours of surveys on wellbeing and our quality of consciousness.  Some of the questions asking about non-symbolic experience were quite weird to me.  For example,  
> Have you ever experienced a state of timelessness and spacenessless?
 
The course consisted of roughly 4 meditation practices on a schedule.  One straight hour per day.  As promised 😉

We also had morning and evening positive psychology routines.  I found these surprisingly helpful. 

Apparently a major _point_ of meditation is to give glimpses of (desired) non-symbolic experience.  Once I experience such an NSE, I can choose to sink into the experience and try the clothes on your size 🧘‍♂️ 😊

3 out of 4 of the meditations worked really well for me, and the fourth was kinda interesting.  (There was an option to try the next batch of meditation techniques for _another 30 days_ if I didn't find my fit yet.)

I may have been helped by spending 1-7 hours walking through parks while reading, thinking, or basking in experience.

Meditation isn't the only way to glimpse NSEs (non-symbolic experiences).  After reading [The Finders](https://www.goodreads.com/review/show/2797259221) about people who experience PNSE (persistent non-symbolic experience), I tested out my capacity to experience "spacious silence" within which my thoughts come and go.  This seemed like a cool inversion: often we think of silent gaps in mental activity, but from this spacious place, silence seemed like the baseline from which pixies of thought fluttered around.  To me [Jimi Hendrix's guitar](https://youtu.be/TLV4_xaYynY) looks like this. 

For me intellectual understanding of the quality of experience seems to help a lot.  A book describing core facets of "what it's like to be awakened" without the paradoxical mysticism really helps.  Moreover, a variety of ways to experience awakening are conveyed, with a decomposition into comprehensible pieces.  This helps with making sense of and navigating my own experiential terrain.  


For about 6 days near the middle I experienced _non-dual perception_, which was really cool and can be distinguished from a non-duality without _identity_ or _sense of agency_.  At first I wasn't quite sure, as I clearly had a _sense of self_ and I couldn't quite recall what _dual perception_ is like.  Jeffery's feedback really helped to clarify things. 

This state reminds me most of the _Resting in Open Awareness_ meditation practice: 
> One takes in sound, touch, sight, smell.  One tries to take it in without interpreting it, just being aware. One also focuses internally.  One welcomes all the sensations at once (even behind the head to the degree possible).  One rests in this open awareness.  I felt quite a strange sensation sitting there as all these senses, my sensorium, blended together.  Internal and external senses seemed part of a continuous ocean of sense.  I’m just there, this bubble in an ocean of awareness.  Multi-modal bubble of awarenes.

My locus of identity had shifted to being a 'ball of awareness centered on my body', and my brain seemed to try and fill in the gaps behind me more than usual.  

Nature appeared truly beautiful and pristine.  Tree leaves and branches appeared as if they were 'just there' where they were in awareness, rather than situated in relation to me.  At times bushes seemed laid out flat in an expensive world, as if I'm just seeing a 3D rendered bubble of the simulation 😜.  Another weird effect was how it seemed strange to me that I couldn't 'sense' behind my laptop screen or inside my laptop.  They're within my bubble of awareness!  Why can't I sense inside them?!

That seems a bit delusional, right?  As does trying to internally represent the world behind me; although due to the regularity of the world, my brain can do a fairly good job. 


In _dual perception_, there's a sense of subject-\>object perception: I'm here looking out through these binocular eyes at the world.  There's a sense of my body's wholeness as a unit other than the word out there I interact with. 

In _non-dual perception_, the subject\<-\>object distinction was mostly lacking.  Sense data are present and interrelated.  The relation between two branches doesn't need to be represented via a projection to me, the special subject.  The separateness moved to, "the bubble of awareness and extrapolated reality (such as object intetnals)", rather than "subject and object".  For me, this wasn't experienced as _Oneness_ per se.   

After a meditation one day, I realized I was snapped back into dual perception, and felt it quite cool!  The distinction was then more clear :-)

I kept up with the practices, and one of the later one gave me some deeper understanding of how a 'further location' might be like. 

More or less, I've managed to settle into somewhere between Location 1 and 2, like 1.75, since then.

That's it?  Just do some practices for 45 days, sink in, and enjoy the results?  

... Yup!  


### What is this 'awakening' really?


First, there seems to be a common core to being awakened (in addition to experiencing life from a non-symbolic place more than a symbolic place). 

1. A sense of _Fundamental Wellbeing_ or okayness.  Whether deep down or on the surface, I know that everything is all right.
2. _Narrative Self_ is taken less seriously, or appears absent algother.

That's the jist of it.  

Weird how experiencing life non-symobolically, with symbols dancing in the non-symbolic spaciousness, seems to lead to a lense of Fundamental Wellbeing, huh. 

'Narrative Self' is essentially one sense of the word '_Ego_', which I generally dislike using for its negative connotations and being a suitcase word (with many interpretations).  Narrative Self involves the stories we tell ourselves about, well, ourselves, other creatures, and the world.  Story is one method of sense-making.   

In the [Intentional Life Hack](/intentional-life-hack) post, I discussed [Confabulation](https://www.edge.org/response-detail/11513) where people create stories to explain their actions, even when they actually _do not know why they took those actions_.  This shows how endemic the Narrative Self is to how we operate in the world.  Even when the honest response is, "gee, I don't know why I did that.  I can probably brainstorm some hypotheses, but...," we try to concoct some evocative narrative.  

It's not surprising dropping this or taking it less seriously can have profound ripple effects 😉

I started accepting when I don't know why I did something before this course, actually.  Feels pretty nice, yo.  
> Oh, shit, I don't need to know why I do what I do!

The Fundamental Wellbeing bit really looks like a red flag to me.  Every futurist knows about [wireheading](https://en.wikipedia.org/wiki/Wirehead_(science_fiction)) where one gets wired to feel blissful no matter what happens.  By the way, Andres has a good article on [how to do wire-heading right](https://qualiacomputing.com/2016/08/20/wireheading_done_right/) that discusses the pitfalls and some ideas for avoiding them.  I find the mantra, 'Fast Euphoria -> Slow Euphoria', very helpful.  Often one can become anxious when losing a 'high' and then shift into 'Slow Dysphoria' instead.  I think this is relevant for awakening as well.  Often glimpses of NSE are very intense -- actually they can be so intense and overwhelming that you _probably wouldn't want them to be persistent_.  The persistent form is often more low-key, with some highs and peaks.  Thus Maslow's term, 'plateau experience', rather than, 'peak experience'.

I found myself naturally doing a 'positive thinking' practice that had often annoyed me.  When comparing two options, I tended to feel that, "things will be _perfectly fine_ either way.  Sure, I have a very, very strong preference for one over the other, but it's not so bad if it doesn't happen, is it?"  Positive thinkers will instruct me to think this way in the belief that this will make me feel better and render the desired outcome more likely.  It turns out they're putting the _chicken before the egg_!  Once you awaken, this form of thinking becomes second-nature.  It's simply hard to lose oneself in deep negative troughs when experiencing a deep sense of Fundamental Wellbeing. 

I indeed find wallowing in despair to be somewhat difficult.  In the past I experienced dreadful despair for a few months after a break-up.  Now even if there's something to despair, I still experience it during the peaks throughout the day, but most of the day's moments are spent enjoying life.   It does seem that sufficiently terrible events, such as the death of a loved one or a divorce, can cause profound grief and even (temporarily) knock one out of PNSE. 

It's kinda weird, and occasionally a bit disturbing, to be honest.  I do still feel the full spectrum of positive and negative emotions, unless you consider persistent despair one.  Am I truly okay maintaining a state of being in which persistent despair is _not on the table as an option_?  I'm not fully sure.  Maybe?  It's a hard choice for me.

Apparently in-line with _wireheading_, motivation can crumble when awakening.  This makes some sense.  If my goals were motivated by some fancy story I told myself or rooted in fear-management, then my old goals just don't make sense from the perspective of Fundamental Wellbeing and playing with my Narrative Self 🧐 😛.

For example, take a nice guy who helps people out when asked in fear of rebuke.  Sometimes he grudgingly says 'yes', consenting to help out when he doesn't want to.  He doesn't even have much room to consider when he does and doesn't want to help others because he's too caught up in avoiding criticism.  Then he awakens and mysteriously feels perfectly fine saynig 'no' when asked for help!  The old motivation had the rug pulled out of under it.  Now he gets to build from the ground up again 🙂.

1. Lecation 1 is what I've described so far.
2. Location 2 is the same but with non-dual perception thrown into the mix.
3. Location 3 is like 'Christ consciousness': one meta-emotion of love/joy/compassion, feeling of unity with the universe, and dual perception returns.
4. Location 4 is an alien place of non-dual perception, no emotions (but feelings are ok), no sense of aggency -- the spooky stuff one hears about. 

And then there's apparently a big chasm to pass (permanently?) on the road to Location 5 where emotions return in a non-personal manner for some people.  At Location 9+, reports of PK (psychokinesis) and other weird phenomena become more common.  These people are more likely to report that they are the universe without individual identity, etc.  

The traits for Locations 1-4 are fairly clear.  Beyond 5, the details seem murkier.

I find the following image quite useful:

![Ang Sang Wahe Guru]({{"/assets/images/continuum.png"}})

### Additional observations and commentary


I seem to be able to fluidly shift in the non-dual perception direction when at parties.  I find the space really cool, and it works well with imagining dancing spirals and shapes to the music 🙂

Another curious phenomena is that the act of describing what PNSE is like leads me to sink in deeper.   Pretty much this is how one can learn to '_lock on_' to various aspects of experience and explore the state space of consciousness :-D

I also noticed that it's easier for me to recognize 'bad habits' in myself.  If I read about how some childhood experience may aversely affect me, it's easier to just look at myself and ask, "hmm, to what extent does this seem present?"  I suspect this is what people refer to as "propping one's ego up".  One wants to identify good traits that make one seem good and avoid any bad traits that make one seem bad.  When I feel fundamentally fine as is, it doesn't matter much if I have some bad habits.  I can examine them, and maybe do so something about it (-- or maybe not 😋).

In arguments, I appear less reactive and more capable of guiding the discussion harmoniously.

There seems to be greater capacity for conscious self-modification, which feels more like simple reconfiguring or mind administration.  I haven't done rigorous testing if this is actually the case or just delusional :D :D :D.  Ideally a general intelligence will be capable of choosing which sub-routines to automate to unsconsciousness; it will be able to elevate any sub-routine for conscious analysis and tweaking, before returning it to the unconscious level.  Maybe becoming more like an Idealized General Intelligence is an answer to "why meditate?". 

I seem to more often _just do_ what I want to do and believe is the best course of action.  I've often struggled with this in the past.  So this is pretty cool 🙂

All-in-all, I seem to have landed in exactly the location I'd want to (and my fears appear, so far, mostly ill-founded).  But I'd say that no matter where I landed, wouldn't I!?

At the moment I don't really want to check out Location 3+.  I'm not sure if I find dual or non-dual perception cooler to hang out in.  So Location 1.75 it is for now.

I want to explore imagination and dreamscapes more than 'going further on the continuum".  The core traits for the Locations seem helpful, but I think there are more dimensions than this continuum, and I'd like to explore them while _more human_.

---

Many people seem to model awakening in terms of 'letting go' and doing with less.  They may say that we let go of the narrative self, of personal identity, of emotions, etc., and then we are freed to feel better and focus our energy on compassionate love ~

I prefer to model awakening in terms of building new structures and patterns of mental organization.

For example, when doing body-awareness practices, I feel as if I'm bringing more and more of my physical being _online_.  Moreover, most of my body is doing pretty well most of the time, so simply integrating my body into my persistent conscious awareness should render a degree of increased wellbeing!  

Awareness-based practices seem to be strengthing/adding a short self-refererential or self-aware loop into my ongoing conscious experience. 

This is in addition to the normal stuff.  Personal narratives may seem less important from the perspective of whole-body integration or expansive awareness.

When I first heard of Location 4, it seemed a bit too dissociative.  Are they just tapping out of life to bliss out on autopilot?  However, there is another possibility.  Try engaging fully with all of your senses at once.  Take in as many perceptual details as possible.  If you're looking at a stone wall, take in every little bump and ridge, every gradiation in color.  If you're in the city, take in every sound around you.  Take in every physical sensation; if you're talking, feel the vibrations of your vocal chords.  Try to grab a cup of tea while giving more attention to the physical process than the 'high-level action' you're taking.  

I find this state highly exhilarating, a bit trippy, and overwhelming.  So much is sensually going on that there's not much room for 'self-referential thoughts'; they might just get drowned in the deluge of sensory experience!  Might the same apply to the sense of agency?  If I truly enjoy living life like this as an unfolding of felicitous experience, then maybe letting my agency be automated is fine?

I think the, 'feelings but no emotions', aspect may fit this model too.  In personal development workshops I'm often instructed to pay attention to where I feel emotions in the body.  Maybe in this state I feel the bodily responses associated with emotions, but there just isn't much room for the actual emotions to do their thing.  I can still respond to physiological cues 😉.

And then even Location 4 which is described in terms of "losing many normal aspects of experience" may actually be seen as an overabundance of other aspects of experience.  A bit imbalanced, perhaps? 

I suppose some things do just fall away, such as severe multi-year depression?

---

Anyway, life isn't perfect here at Location 1.75 even after awakening.  I experience highs and lows.  I still feel motivational confusion and am not sure what to do about the state of the world, but I no longer feel I need to _figure it all out_ before enjoying life and doing my things (: reading, learning, dancing, socializing, researching, coding, etc).

Cheers o/


