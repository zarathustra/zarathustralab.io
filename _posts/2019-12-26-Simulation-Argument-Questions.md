---
layout: post
comments: true
title: Simulation Argument Questions
date:   2019-12-26 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- logical fallacy
- Eray Ozkural
- Nick Bostrom
- Simulation Argument
- Metaphysics
- Philosophy
- Unknown
---

(Disclaimer: I am not a super genius or expert in physicsor computer theory. Feel free to correct any naive errors.)

The Simulation Hypothesis \[[1](https://en.wikipedia.org/wiki/Simulation_hypothesis#Ancestor_simulation)\]  sets up a trichotomy:
1. Almost no civilizations become capable of high-fidelity ancestor simulations (for whatever reasons).  
2. Almost no capable civs want to produce ancestor simulations.  
3. We almost certainly are in a simulation.

As usual, dichotomies and k-chotomies should give off glaring red-flags.  
Some important details are almost necessarily being glossed over for the sake of 'clarity'.

Moreover and Dennett's Dragon Eray Ozkural \[[2](https://examachine.net/blog/why-the-simulation-argument-is-invalid/), [3](https://examachine.net/blog/simulation-argument-and-existential-ai-risk/)\] has incessantly hammered down the point that SH (Simulation Hyhpothesis) is a philosophical entry-drug for Creationism.  It is the _blue pill_.  
Why?  
Simple: (1) and (2) are very hard for us to know, at present, with high certainty.  Moreover "root reality" may have different laws of physics (nevermind computing theory).  We are invited te be handwavy in judging that they seem likely enough, and as Creationists tend to argue, we can't rule it out.  So hey, maybe we do live in a simulation!  We have VRs (Virtual Reality) now and can imagine them getting better, so it's an easy entry-point to belief in mysterious Creators.

My highly esteemed father, the illustrious Doctor Benjamin Goertzel \[[4](http://multiverseaccordingtoben.blogspot.com/2019/07/the-simulation-hypothesis-is-mostly.html)\] points out that we know that we do not know (1) and (2) to high certainty.  Worse yet, if (3) is true and we are in a Simulation, then we cannot know (1) and (2) to high certainty almost by definition. 

Now those are some sad red-flags befitting a trichotomy: if we manage to dodge getting gobbled up by Creationism, we're stuck in a circular argument that we are ignorant.

As Ben mentions, one good feature of SH is to get us thinking about ways in which our bubble of the [Observable Universe](https://en.wikipedia.org/wiki/Observable_universe) might be _but a drop in the greater sea of Reality_.   

However it doesn't help us beyond that.

---

For those who want to probe deeper, my suggestion is to keep the SH question grounded.

Assume we inhabit _root reality_.  
Now what can we say about (1) and (2)?  

First of all, we'll have to wait and see to be __absolutely sure__.

Second, any answer will have to deal with the [Bekenstein Bound](https://arxiv.org/abs/1504.03303): an upper limit to the amount of information in a finite region of space with finite energy.  Namely, the capability of posthuman civilizations to create high-fidelity simulations does not negate the physical limits on what can be computed.  
Eray Ozkural has investigated the physical limits of intelligence in a similar manner: [Ultimate Intelligence Part II: Physical Measure and Complexity of Intelligence](https://arxiv.org/abs/1504.03303). 

What does this mean?

Strictly speaking, running Sims won't gain you anything: you won't suddenly have room quintillions more sapient beings than before.  
Well, not exactly. Virtual Reality will allow, say, human-like Minds to live on a space station or a planet that doesn't support organic life.  So there may well be a good argument that eventually most sapient life forms will run on some form of computer, or we'll get femtotech working and merge into the very fabric of spacetime 😈.

Now what about Ancestor Sims? -- or what I think are more interesting: Alternate Universe Sims.  
Literally emulating the planet itself, assuming the same laws of physics, will take a colossal amount of energy (on the order of magnitude of the planet being emulated?).   
Another option in computer science is to emulate the API (advanced programming interface -- the functionality).  It's not immediately clear how to port this to Ancestor Sims.  

Next there is virtualization [see \[5\]](https://www.floobydust.com/virtualization/lawton_1999.txt).  I think the basic idea is, in computers, to default to the current hardware everywhere possible instead of actual emulation.  Where needed, real emulotion can be done.  That is, if the emulation takes as much energy as the planet itself, why bother actually simulating all the laws of physics over again? Just default to the laws of physics where possible.  But then the Sim ceases to be a simulation per se.  The simulation becomes a "special legal zone" of the _root universe_. And the situation is more akin to a [Virtual Machine](https://en.wikipedia.org/wiki/Virtual_machine) than a full-blown Simulation.

![Now just imagine these are laws of physics instead of hardware]({{"/assets/images/virtualization.png"}})

Well that's pretty cool, actually.  But it says little about why the _root universe_ would be more full of VMs isolating Ancestor Sims than dealing with the lives of the Progenitor Entities.

One answer is that there are more Sentience Entities in Sims is that they are so much simpler than the Progenitors!  
There are trillions of microbes in the human body \[[6](https://www.nih.gov/news-events/news-releases/nih-human-microbiome-project-defines-normal-bacterial-makeup-body)\].  
And likewise perhaps one Progenitor consumes more energy than an entire Ancestor Sim!  

Aha, skeptics!  How do you answer to that?! 

Another idea is that we don't need to actually simulate _everything_.  We can render reality in the simulation on an as-needed basis, maintaining consistency. This leads to the fear that as we approach Technological Singularity and consume exponentially more resources to simulate, we will be shut-off by the Progenitors \[citation needed\].  This delays the above problems rather than solving them as we progressively evolve to greater and greater complexity.  Will we be uplifted to _root reality_ at a certain stage, as postulated by New Ageists?  
Perhaps we can go to the next level and suppose we actually find a simpler way to program our ancestors than natural evolution did the first time.  Thus we can re-do history more efficiently. w00t!  
Will it be more efficient to run human minds on computers?  At present it seems far, far, far less efficient.  But is it only a matter of time?  
I've wondered if we can run human minds with fewer abstraction layers between 'Zar' and the [Standard Model](https://en.wikipedia.org/wiki/Standard_Model) than the current biological implementation. Roughly speaking: 
> Particles -> Atoms -> Molecules -> Proteins -> Cells -> Organs/Tissues -> Sub-Sapient Mammalian Consciousness -> Human Sapience (Zar) -> ... ?   
Then again, modern computers have many abstraction layers as well.

But if the Progenitors are so smart and can refactor the substrates of their own existence to run Ancestor (or Alternate Universe) Sims, then why don't they just refactor themselves to exist with less energy (in a more 'sustainable' manner)? 😏🤔

Anyway, coming back to the "humans are like microbes to the Posthuman Progenitors" question:  
> Are simpler organisms more likely to be in a simulation?  
Perhaps. Although as a generic living organism, you are more likely to be simple, right? 😛😛😛


The next questions regard Simulation Nesting:

![A Sim in a Sim in a Sim in a Sim in a . . .]({{"/assets/images/homunculus.png"}})

Will we put Simulations in Simulations ad infinitum to create essentially infinite Sim People relative to _root reality_ people?  
Short answer: no.  
The physical limits render this question moot.

It is cool to think about.  Start with a [PC Building Simulator](https://www.pcworld.com/article/3184375/meet-pc-building-simulator-a-diy-teaching-tool-that-could-be-the-novices-best-friend.html) that lets you install your favorite OS (operating system) on your simulated PC, and then install the PC Building Simulator on there.  Wheeeeee :) :) :)

As far as I can tell, there will not be many computational benefits to nesting VMs or Sims more than one level deep.  
The argument for digital hosting of one level is that we can run VRs or digital/uploaded minds in space and other ecosystems where biological life is not suitable.  
Is there some weird reason why further nesting allows life to fit into many curious niches that can't be done in _root reality_ and _digital reality_ alone?

So it seems there is no "explosion in people" due to running Ancestor Simulations.  
The same number of people could be hosted in _digital reality_ on the _root level_.  
We, based on our current understanding, do not get some increase in "universe size" by running successive simulations.

We can't really conclude Bostrom's corrolary either: "We will never run ancestor simulations".  Maybe we will. The SH is moot so long as we don't flood the universe with Ancestor Sims like grey goo replicators (but as the popularizer of the [Paperclip Maximizer](https://en.wikipedia.org/wiki/Instrumental_convergence#Paperclip_maximizer), perhaps he'd like this).

---

### Related ideas

Wheeler's _law without law_ idea proposes that we should look for theories of fundamental physics out of which the known physical laws can emerge.  He took inspiration from the "the boundary of the boundary is null", or "the law of the law is null (no law)".  Could our observable universe be then a sort af Virtual Universe on this fundamental base reality? And there are different "laws of physics" on top of the _root_ "lawless law" of the universe "elsewhere"?  In this sense you could say we are in a "simulation", but it's quite different from how Bostrom meant it. See [Precedence and freedom in quantum physics](https://arxiv.org/abs/1205.3707) for some 21st century work in this direction.

Another thought experiment is: if we identify a planetary life system, will we want to virtualize their physics if we can? Isolate them from the full _root reality_ for their own good, or to keep the rest of us safe as they sandbox?  Might we want to run different low-level physical "operating systems" to live within once we reach posthuman status?  In which case most of us will live in some sort of virtual environment.  
Interesting to ponder, but too messy to draw any nice conclusions as in a false-trichotomy. 

Your brain is a world/self simulator.  The phenomenal world you live in is an internal API for your brain by your brain.  To the best of our knowledge, the world doesn't look like it does to us.  We see a small segment of the light spectrum, for example.  So we all live in a simulator already, our own personal simulation.  
-- Unlike in the SH, our simulations are "one level deep" and interact with each other via "reality".

Love, 
    Zar








