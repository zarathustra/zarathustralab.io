---
layout: post
comments: true
title: "The blog posts continue at GardenofMinds.Art"
date:   2020-09-23 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- blog
- WordPress
---
---

I have moved my blog (back) to WordPress, which you can find here: [GardenOfMinds.art](https://gardenofminds.art/).

The principle of making pages with [Jekyll](https://jekyllrb.com/) seemed quite cool yet I found myself not investing enough attention in gaining the adequate skills.  Maintenance and extension of the site seems easier on WordPress for now.  However, to be honest, finding a good theme for WordPress also proved challenging (and I'm not particularly done).  Also, porting in a bunch of markdown ".md" files to WordPress was surprisingly painful, even though many of them were *exported* from WordPress. 🤦‍♂️ . 

I wonder when/if the time will come for figuring out how to make the website "just right" (in the spirit of satisfiction).

I like the idea of leaving this blog post up.  Some of the posts written for this blog appera to be more aesthetically pleasing than the WordPress-imported versions!

Wherever you enjoy this content, you have my gratitude 😊. 

