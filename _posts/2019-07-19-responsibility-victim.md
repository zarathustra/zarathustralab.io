---
layout: post
comments: true
title: Personal Responsibility and Victim Blaming 
date:   2019-07-19 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- personal development
- self help
- spirituality
- responsibility
- victim blaming
- balance
- compassion
---

In "personal development" communities and other advice I've seen a trend of telling people they should be responsible for, well, just about everything in their life.  
Moreover, don't inflict your problems on the rest of us.

Does such "advice" become __victim blaming__ at some point?  

### What is responsibility?

Typically "responsibility" has to do with duty or being accountable for something (and thus to blame if it fucks up).  
Thus it's natural that "personal responsibility" becomes "personal blame".  
And in seeking to positively encourage people to step up and take stewardship of their lives, we inadvertently end up blaming them for their negative experiences as we blame rape victims for not dressing properly.    
The argumentation is eerily similar: by accepting responsibility for your negative experiences, you'll think about what you can do better next time. For example, don't dress so skimpily and always be with a few friends when going out late at night!

It's true in a sense.  
And is a counter to [Learned Helplessness](https://en.wikipedia.org/wiki/Learned_helplessness) where you feel powerless to do anything about your negative experiences.  
It's even [optimistic](https://blogs.scientificamerican.com/cross-check/the-infinite-optimism-of-physicist-david-deutsch/) in a sense: I'm saying that you __can__ do something about your problems. Yes, you. You can do something. 

Which brings us straddling a fine line of reassuring you that it's not your fault and reminding you that you could have done something to prevent your negative experience.

Krishnamurti's “It is no measure of health to be well-adjusted to a profoundly sick society.”  
Sure, you can learn to cope but maybe you're not responsible for the shit a sick society gives you.

## Responsibility :- Response-ability

The best deconstruction and generalization qua simplification of "responsibility" I've seen came from [@streondj](https://twitter.com/streondj). Which, of course, is a simple reduction to the etymology of the word, to the roots. Gotta love 'em roots (linguistic, food, and [music](https://www.youtube.com/watch?v=kEGryvfh1w4))!

All the particular definitions are specializations or instantiations of "the ability to respond".

Ideally the one held accountable for something important or with a duty to deal with the matter is someone who is "capable of responding".  
Conversely, if you are able to respond and don't, letting some shit go down, we may blame you. __Why didn't you do anything?__

To dig deeper, in societies entrenched in authority, permission, and ownership, there may be very few people who are "able to respond" to most matters. For example, my daughter needs care to live and almost everyone is forbidden by law from doing anything without my permission. Someone needs to respond to her needs and there are only a handful of us who "are able to" (legally): thus we have a __duty__ to provide said care. (And if we neglect this duty, someone will violate the default permissions and give permissions to someone else who offers to provide said care. Cheers.)   
Duty is apparently a logical consequence of legal permissions.

Going back to the above dilemma of who to consider responsible, both the rapist and rapee may be response-able. And there are complaints on both sides.  
* Can the rapist really change its behavior given an opportunity?  
* Can we really ask someone to not leave their house to avoid any possible harm?

Our general answer is that **Plan A** is that people shouldn't be assholes, but there are still some assholes so be careful as a **Plan B**.  
But ultimately, some bad shit may happen to you that you couldn't __reasonably__ do anything about. 


Next up I'll examine an error of Individualist philosophy:

### Who's responsible for my happiness?

![It's your own damn responsibility to be happy, m'kay.]({{"/assets/images/willsmithyboi.jpg"}})

This passage stems from a misunderstanding of the **duty**-responsibility and individuality. 

There's an ideal of absolute independence where I isolate my emotional state from all externals and remain blissful no matter what happens. Stab me with a knife? No worries. I don't feel external pain. This often comes up in discussions of Buddhism.  
In this case I can say that I am "individually responsible for my happiness (emotional state)", i.e., only I am capable of affecting my happiness.  
Once you bite this bullet, the immediate retort to any negative experience is "well, why did you do that to yourself?"  
![If you get hurt playing the knife game, well, don't complain to me!]({{"/assets/images/knife.gif"}})

Do you want this absolute independence?  
Do you think it's possible? And if so, do you want to strive for it as the 'solution' to your problems?  
* Just find emotional independence, and then you can interact with others without fear of getting hurt!  
  (Note, if you read [The Finders](https://www.goodreads.com/review/show/2797259221) some people seem to __mostly__ attain this, with some exceptions for the most extreme of emotions. They can also dissolve this illusory separation into "me" and "you" though....)

Back to normie life where my 'happiness' is correlated to my shit, to my external environment. Actually, by interacting with me, you can even guide me to internal states I'd never imagined!  
This normie life where __I'm not alone__. 

(Or, ok, if you generalize "my happiness" to "take care of my shit" then it applies anyway.)

1. Am I actually the only person capable of helping myself? ... No.  
2. Are there things I can do with your help but not alone? ... Yes.

Suppose there's some deviation in my happiness at home. Who's most likely to be able to help?  
1. I'm nearby, so I'm response-able.
2. My flatmate is nearby, so she can probably help too. She's response-able.
3. My e-friend in Cambodia is online, and may be able to help. They may be response-able.

We're all response-able.  

It's not a matter of "I'm responsible for my happiness" or "My wife's responsible for my happiness."  
We both are responsible.

The lens of Individuality tries to assign sole responsibility to one party (as we try to assign sole responsibility to either the __asshole__ or the __sucker__).

This is similar to the principle of "duty stems from limited permissions".  

The duty of family to help each other also stems from proximity, plain and simply, not just from social and legal permissions.

Moreover, even if I spilled the milk, anyone near by is response-able.  
I've sensed confusion culturally between the "it's not my problem" perspective and the "well you could have helped" perspective.  
Personally, I held the former stronger.



Geez. There's another nasty meme hidden in this passage to disentangle.  
There's __perfectionism__ hidden in here: the idea that we should both "be happy" before coming together. 

What does this say to someone who is presently unhappy? 
* Get your shit together first. Then meet people.

This blends well with blaming individuals (__you__) for your own unhappiness.  

Not only is it your (and only your) problem you're unhappy, but only happy people should socialize to share their joy!

* Maybe you are a better shade of __unhappy__ when with me than alone?  
* Maybe we can find some way to deal with your shit together that neither of us could alone: we are response-able where both you and I are not! 

... Ok, and to be fair, yes, it is **selfish** and **entitled** to expect your wife to be soley responsible for your happiness.  
Exactly as Will Smith says.


### The System

So it appears assigning sole response-ability is generally false (and only true if we, by custom, forbid anyone else from acting).

[Assignment of credit/blame](http://www.bcp.psych.ualberta.ca/~mike/Pearl_Street/Dictionary/contents/C/creditassign.html) is a difficult problem, and humans tend to try and grant __all__ credit to a single entity.

Searching around to see if anyone else was rambling on about "self-help" and "responsibility" found me one cute article: [Self-help in the ‘age of responsibility’ denies unequal realities](https://theconversation.com/self-help-in-the-age-of-responsibility-denies-unequal-realities-101782).  
Basically: how can you say it's all my responsibility to succeed when the playing field is clearly worse for me?  
![Here to save the day by telling you it's already saved.]({{"/assets/images/captobvious.gif"}})

Who else can shoulder the responsibility? **The System**, that's who.

Or maybe you should stop whining about the system and actually do something for yourself. 

As always: it's a bit of both.

It can be hard to sympathize with someone who cuts his hand playing Knife. I mean, it's their responsibility and they should have known better.  
Especially not twice in a year! C'mon. 

When you go hardcore down a "personal responsibiltiy" philosophy, you'll find ways to blame poor people or unhappy people for their own predicament.  
At the end of the day "we can't help you unless you help yourself".  
Which has some truthiness, yet can easily gloss over __cooperation__.

![Cats are like weather: we can't change them yet.]({{"/assets/images/accept_change.jpg"}})

Uneven playing fields may be cast off by saying that one should accept what one can't change (as fighting the laws of physics is a route to insanity and perpetual misery).  
This again leads to sole personal responsibility because I can't (by law, at least) force others to help me or force the system to change.  
The only guaranteed help I can get is my own. Thus I am to pull myself up by my bootstraps on my own.

And this is how a memeplex of "empowering personal responsibility" masks subtle victim blaming.

When in reality The System and the playing fields can be altered.  
And people have response-ability for me (voluntarily).

Sometimes shitty things happen, and there was no reasonable way to avoid it.

You're not alone and you don't have to nurture yourself alone. 

Let's step out of this memeplex :)

P.S., hopefully you're already out of it and not even in social bubbles where it exists 🥰🥺

