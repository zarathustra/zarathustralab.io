---
layout: post
comments: true
title: Meaningless Meaning and Plural Purpose
date:   2020-05-24 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- rationality
- Subjective meets Objective
- Meaning of Life
- Dennett
- Strange Inversion
---

### What is the Meaning of Life?

The answer to this question pays homage to [Darwin and Dennett](https://www.pnas.org/content/106/Supplement_1/10061) along the lines of:  
> “To make a perfect and beautiful machine, it is not requisite to know how to make it.” 

Shall we say, "To live a beautiful life, it is not requisite to know what it means"?


Back in December, I read Dennett's [From Bacteria to Bach and Back: The Evolution of Minds](https://www.goodreads.com/review/show/3091870597) in which he absolutely adores the concept of "__designerless design__", which leads to agent-free teleology.  

The idea is that evolutionary niches provide the "specs" for life-forms to adapt themselves into fitting.  As the might [Giant Tube Worms](https://en.wikipedia.org/wiki/Riftia_pachyptila) near a hydrothermal vent thrive and reproduce more than their competitors, they prove themselves "better designed" for this ecosystem.  With them, life becomes increasingly designed without any agentful designer.

If there is room for further "design improvements", the mighty progeny will likely further design their species.  
I suppose this stops when they fully match the specifications set by their niche in the ecosystem?

The cool part is that while Giant Tube Worms fill their niche, they are part of the environment that provides "specs" and stimulation for other life-forms!   
They're not intentionally designing the hood life.  
They simply take up space and some bacteria.

Humans of course intentionally breed animals to facilitate certain "adaptive designs", such as [foxes](https://en.wikipedia.org/wiki/Domesticated_red_fox) and [dogs](https://en.wikipedia.org/wiki/Dog_breeding) (which ironically runs into inbreeding issues...).   
Yet Humans also shift the evolutionary niches of animals unintentionally, much as Giant Tube Worms can: apparently many mammals are [becoming nocturnal](https://www.nationalgeographic.com/science/2018/09/news-humans-making-mammals-nocturnal-behavior-ecology/) to run into Humans less.  This includes: bears, lions, tigers, wolves, foxes, deer, elk, pigs, tapirs, giraffes, rabbits, beavers, agoutis, porcupines, elephants, possums, and armadillos! 

And in Soviet Russia, words design humans.

Anyway, Dennett describes the concept quite colorfully. 

We can sensibly talk about design without intentional designers (in response to environmental stimuli.  This is similar to how we can explain the __reason__ water falls when poured out of a cup: it's due to gravity.  -- Oh, okay, the reason is also that I dumped the cup upsidedown! 😛

---

Then in January, Prague's Philosophy Group had a discusion on "The Meaning of Life", and it struck me the same errors are made here! 

*Absolutists* and *Objectivists* seek one grand Meaning to rule them all.  

+ Perhaps the meaning is to love one another and gain entry to Heaven.   
+ Perhaps the meaning is to survive, and everything else is subsidiary.
+ Perhaps the meaning is to have fun, the [Ludic Principle](https://thebaffler.com/salvos/whats-the-point-if-we-cant-have-fun).
    - Oh I suppose that's standard hedonism?
+ Perhaps the meaning is to be the Universe understanding herself.
+ Perhaps the meaning is to do what my superiors say, ultimately grounded in the Emperor and God.

Often one will rhetorically argue their answer is correct because "this is how the universe works".   
For example, "all beings strive to survive and grow, and this was the driving principle of evolution, so clearly our purpose is to further cosmic evolution!"

*Nihilists* point out that such arguments run afoul of the [is-ought problem](https://en.wikipedia.org/wiki/Is%E2%80%93ought_problem#Overview):
> "The is–ought problem, as articulated by the Scottish philosopher and historian David Hume, states that many writers make claims about what ought to be, based on statements about what is.

Basically it's a type error: it's unclear how to relate factual statetems, such as, "there is a cup on my head", and prescriptive statements, such as, "there should not be a cup on my head!"

The common trope of defying fate should also make this clear enough 🙄😜

The classic next step is to throw my hands in the air and give up: there is no meaning of life!  
Which is ever so close to, "there is no meaning in life!"

#### Subjective Meaning

Eventually Nihilists pull themselves up by their bootstraps and embrace a *Subjective Reality* perspective on meaning.  If Objective analysis fails, then we are fundamentally free to determine our own meaning in life.  

If you value love, survival, pleasure, curiosity, God, or obedience, that's all perfectly fine!  

Sure we can objectively analyze how you came to value [painting butts](https://www.maxim.com/entertainment/paul-roustan-butt-paintings-2017-6), but this study doesn't change the fundamentally subjective nature of your meaning in life, your calling: artistic butt paintings.

So now we are Nihilists in the technical sense of rejecting the absolute, and we're relatively free to find meaning wherever we desire :)

---

What do we find if we delve deeper into how we make sense of the world?

Consider the [image segmentation](https://medium.com/@arthur_ouaknine/review-of-deep-learning-algorithms-for-image-semantic-segmentation-509a600f7b57) below:

![Baa Baa Black Sheep, do you know where you are?]({{"/assets/images/sheep_segmentation.jpeg"}})

To make sense of the world, we draw many distinctions and identify objects.   
If I find woolen sweaters meaningful, then identifying sheep also becomes meaningful.

When blinded by absolutism we obsess over top-level goals, such as survival, as *the* meaning.  Everything that helps me survive is _meaningful_ but not *the* meaning.  But now that we've given up on identifying the absolute reason, we can simply consider everything instrumentally meaningful.

A classic _means_ and _ends_ analysis will distinguish that which is _intrinsically meaningful_, such as the cozy woolen sweater, and that which is _instrumentally meaningful_ because it helps me attain the woolen sweater, such as money or wool.  

Actually, a classic error is to lose oneself in the _journey_ and forget about the original goal.  Thus one ends up seeking money for its own sake, even though they originally sought to build a cotton-candy palace with their riches!  Aww, shucks!

As a superb life hack, developing a appreciation for the joy of being alive means that nearly every moment becomes subjectively meaningful.



Anyway, the cool realization with instrumental meaning is that objective analysis **works**. 

Consider this door's design: What are the most meaningful parts of the door?

![Why oh why is this door handle so damn meaningful?]({{"/assets/images/meaningful_door.jpg"}})

+ The door itself that functions as a barrier to entry.
+ The handle that lets the door be opened.
+ ...

The handle is quite meaningful, instrumentally.  Without it, the door might as well be the same as the wall.  

Look at how even the cute British cat knows to reach for the handle! 

The door hadle is instrumentally meaningful for any subjectively meaningful goal that relies on the door: safety, security, peace, warmth, privacy, isolation... -- okay, those sound ab it dreary. 

Can we perhaps say the door handle is generally meaningful, integrating over all subjective meanings that it may aid?  

Probably.  

The door together with the walls, roof, and floor make for _encapsulation_, which is quite a general, meaningful principle as well.

Now we've gotten to "meaning of a word" sense of the word "meaning".  Curiously, this is intimately linked to the "meaning of life" sense of the word!  

To be a prick, allow me to say, 
> We have words for that which is either intrinsically or instrumentally meaningful.

We Subjectively view the world through a lens of meaning: 
+ Objects are clearly separated to stand out
+ Everything is named for communication
+ Instrumental uses are known

Those who subjectively value knowledge, find this process instrinsically meaningful.  
And the rest do it for instrumental value and out of their cultural upbringing.  And to an extent we can't help it during our cognitive development?

As a curious aside, it appears important for wellbeing to be able to experience the world non-symbolically as well!  
Some people try to re-learn to see the world "as it is" rather than through a "lens of subjective meaning".  
However, these people can still effectively navigate the world, which indicates their brains keep up the work automatically.


#### Objective Meaning Rediscovered: Meet the Plurality

Now comes the plateau where all the ingredients can be mixed together: designerless design, subjective meaning, instrumental meaning, and "meaning of life" stories.

1. Intrinsic meaning and values ground _meaning in life_.  
2. Instrumental meaning has a life of its own, and can be viewed objectively.   
3. As with designerless design, instrumental meaning in the environment constrains what I can do.  
4. Many "meaning of life" stories are made up of instrumentally meaningful parts. 

Actually, as the 'door handle' case demonstrates, we can say it has _objectively intrinsic meaning_ whereas 'eating ice cream' has _subjectively intrinsic meaning_.

When viewed Subjectively, Objectively intrinsic meaning can seem _meaningless_.    
Any depressed person can tell you, 
> Sure, door handles are very useful, but what's the point of opening the door to the ice cream parlor?

In reality, objectively meaningful paths abound!

Survival is clearly both objectively meaningful, and to most of us, subjectively meaningful.

[Love and Marriage](https://www.youtube.com/watch?v=BRDBvKGc1fE) is clearly a common meaningful journey to embark on.

Scientific discover, research, and development offer many paths for objective meaning (and subjective meaning for the curios).

Is everything equally objectively meaningful? -- Not really.  I'd rate the scarf below the door handle.

Some paths do make more sense.  
Some paths do work better.

And the objectively meaningful paths are evolving and adapting with us over time.

**Purpose** is when objective and subjective meaning are aligned.

For example, I love sweet, sensual please and I'm leaving my apartment via doors and stairs, on sidewalks and trams, and finally using language and money to enjoy some cookie dough ice cream.  

Raw sex and AI development both satisfy raw subjective pleasure with the objective creation of the next generation ;-) 

Thus "meaning of life" stories can be meaningful, even if not absolutely true ~ 

For those who don't want to look to the far future, a common anti-story is to _live in the moment_ as a Markovian agent.

---

There's a plurality of purposes available to choose from a sea of ever-evolving meaningless meaning.

The nature of the game is discovering objectively meaningful trajectories that align with my subjective values.










