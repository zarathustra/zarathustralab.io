---
layout: post
comments: true
title: Reading Bukowski and others
date:   2018-06-30 12:00:00 
subclass: 'post  tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- life
- poetry
- bukowski
- reading
- dark
---

Reading some poems for my daughter Ilmeja. 

One of them made her disturbed.   

<audio src="/assets/poem-read/bukowski1.ogg" controls preload></audio>
<audio src="/assets/poem-read/bukowski2.ogg" controls preload></audio>
<audio src="/assets/poem-read/bukowski3.ogg" controls preload></audio>
<audio src="/assets/poem-read/bukowski4.ogg" controls preload> </audio>


<audio src="/assets/poem-read/rumi1.ogg" controls preload></audio>
<audio src="/assets/poem-read/rumi2.ogg" controls preload></audio>
<audio src="/assets/poem-read/rumi3.ogg" controls preload></audio>
<audio src="/assets/poem-read/rumi4.ogg" controls preload>Like This</audio>


<audio src="/assets/poem-read/zar1.ogg" controls preload></audio>


<audio src="/assets/poem-read/pigiurmu.ogg" controls preload></audio>
<audio src="/assets/poem-read/nisargadatta.ogg" controls preload></audio>



<audio src="/assets/poem-read/bukowski5.ogg" controls preload></audio>
<audio src="/assets/poem-read/bukowski6.ogg" controls preload> </audio>





