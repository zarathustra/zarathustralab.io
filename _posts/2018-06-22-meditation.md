---
layout: post
comments: true
title: Meditation
date:   2018-06-22 
subclass: 'post tag-test tag-content'
categories: 'zar'
navigation: True
logo: 'assets/images/ghost.png'
tags:
- zar
- meditation
- spirituality
- energy
- focus
---

### Why do you meditate? 

I've done a fair bit of meditation. 

And I don't have a good, clear-cut answer.


I'm not far into some religious school or cult tradition that says I must meditate. (Although [my mom](aranya.org) is.) And one of the first I remember intentionally doing is a gold standard: following the breath. Precisely counting breath up to 10 and down again. I got impatient to do some other one and she said no: do this more. Doing yoga together (as she's also a yogini and yoga teacher), I focused on the physical and exercise aspects -- not the meditative. The meditative seem extraneous and boring. What need have I to sit there? I can sit there with my Gameboy. 

To make matters worse, for most of my life I looked at those Eastern Mystics, those Enlightened Ones, and thought that they basically did nothing. What kind of mental plague of ideas leads one to just do nothing -- and be perfectly satisfied and happy doing it. To talk of this "self-death" or "Nirvana" or "Enlightenment" as something desirable, something that rids one of suffering, when people who become are boring and don't do jack-shit. Or there's the Zen paradox (a la [Alan Watts](https://www.goodreads.com/review/show/2412719677)]: nothing changes yet everything's different. You and the world are the same, yet now it's all good. -- Except that's obviously not the case: now you say this; before you didn't. (One reason I preferred Smullyan's [Tao](https://www.goodreads.com/review/show/2332429390): he flirts with the ineffable all while avoiding obviously false or illogical statements!)  

Is the foregoing of desire and interesting activity worth the reward of cessation of suffering?
To my delight, [Sri Nisargadatta Maharj](https://www.goodreads.com/review/show/2255822742) clearly told someone who came to him asking for peace that "diving into life's experiences, ups and downs, is a perfectly valid choice. But if you want peace without the downsides, then kill yo self (figuratively)". 

Watts tried very hard to make the point that the Eastern Mystics speak in the operative tongue and are tricksters. Taking what they say literally or the surface-intent as the total intent is, so he argues, obviously mistaken. If you do, then they intend you to trip and fall, and see what they are really getting at. Darn polemics.

... However if this is the case then one intent in the **Four Noble Truths** is to show you what truly being free of suffering in life entails. Sarcastic and full of dry humor: "So, you really want to be free of suffering, fully and completely, huh? Well, it comes at a small price. Here, this is the cause of suffering, and this is the path to overcome it. Nice, huh?"
![FOUR NOBLE TRUTHS]({{"/assets/images/nobletruths.png"}})

Then something snaps in you, somewhere along the way, and you embrace some suffering, some sadness, in life. You find some acceptance for pain in life, and become free as if dreaming you are a butterfly. Maybe you let go of your stigma and can respond to "How are you?" with "Miserable, today".

I didn't realize this then. I just didn't quite like that, but was very curious about different mind-states and cool experiences. Seems they can give you quite fine-tuned self-control too!
Thinking about meditation rationally, following one's breath was obviously good focus training (and breathing being conscious and unconscious may be a good place to focus for expanding conscious control -- or vice versa). And while those Nirvanic states seemed undesirable, they had funky cosmological claims about reality that one will "naturally realize" provided one just looks (which, err, could take decades or lifetimes of careful, consistent practice).

There are other positive benefits like [stress and anxiety reduction](https://boingboing.net/2018/06/17/tune-in-turn-on-slack-off.html), turning off habits deemed negative such as some types of mental chatter. 

Flow state is quite nice. Maybe meditation can help one enter a flow state of life -- not just in certain activities? That'd be pretty cool. I wanted to [free my ass](/reflections-on-2017-year-of-free-ass). Maybe meditation can help do this? That deadlock feeling holding me back somehow, where I just can't do what I want to for some reason or another . . . :< grr :<. I could do with better focus when doing things too. Concentration training may be worth it; although I didn't do it much.

Pick-up Artist material often talks about how "inner game" is important, not just doing a lot of stuff. Changing how one thinks and feels about the world can at times matter more than just talking to more women. Of course, Action -> Thought/Belief change and vice-versa. So there is the sex motivation to meditate as well! 

[My mom](http://www.aranya.org) has often stated that meditation can lead one to solving pretty much any problem one has in life. One does not really have to understand _how_ it does. My speculated means are that in meditation, in meditaitng on one's problem, relevant emotional triggers will come up. One's subconscious will be mobilized to process these, one's desires, and one's fears. Over time one will reconcile the conflict leading to a problem and one's attitudes/beliefs/behavior will shift. One will start solving one's problem. Or maybe she meant this in the [Yogi Gupta](https://www.goodreads.com/review/show/2357718933) style of thought/yogic powers: at the end of the day one's thoughts determine one's actions. Change your thougths and your actions change. A problem is really a problem with your thoughts at some level. So fix it at that level. And then there's the law-of-attraction type ideas: "thoughts are living entities" and once you have positvie thoughts for what you want, they go out into the universe and work for you, to manifest your desire. Why try to come up with some real-world solution when you can just meditate on it?

Ben showing me Jeffrey Martin's article discussing ["Persistent Non-Symbolic Experience"](http://nonsymbolic.org/PNSE-Article.pdf), of which he saw 4+ varieties in people he interviewed and monitored around the globe. The claim that this could be done in 18 weeks or so of 1hr+/day in meditation etc piqued my interest. It felt somewhat doable. 

Speaking of desirability they [report](http://finderscourse.com/awakening.php):

1. High well-being
2. Reduced mental chatter (is this desirable?)
3. Reduced negative emotionlity
4. Greater love and compassion (and I've felt annoyed at this seeming fixation on compassion as ultimate value/virtue)
5. Freedom from self-critical thoughts
6. Increased present focus
7. Increased mental capacity
8. ++ Decision making.
9. ++ connectedness
10. Ongoing flow b\*tches!

More-or-less the usual bag of positive things one can achieve via meditating. Very desirable. (Are we being duped? Hook-line-and-sinkered?)

Another [meditaiton progam](https://zenhabits.net/the-44/) I did states the following goals/benefits:

1. Overcome uncertainty, anxiety, fear, discomfort and procrastination
2. Find Peace, mindful openness, gratitude and joy
3. Transform your difficulties and pain into growth and beauty
4. Overcome any struggle you're having right now.

Zen. As my mom said: "overcome any struggle". 

This is quite a claim. Can it really be that good. This is starting to sound like a panacea.
Or meditation is a rather versatile family of practices that can help one with many positive psychology type goals, depending on how one uses them.


### Mind-States

Some of the "[locations](http://finderscourse.com/awakening.php)":

1. Fundamental peace that things are ok. So less fear, anxiety and worry. (Sounds like what corporate meditators may want -- except that desperate workers are malleable.)
2. Well-being++. Negative emotions are sparse. Lovely "nondual" oneness and little negative self-chatter. (Ok, this sounds like a more productive way of being.)
3. Emotional experience --> love/gratitude/compassion. Next to no negativity. Sense of divine unity. From happy to extremely happy. Infinite Patience. (-- Wait, hold on. Do I even want no emotional experience?)
4. No more sense of divine unity. Freedom from approval, emotions, and sense of agency. Ability to lock-in to feelings, values, or one's self, yet also be aware beyond it. And shit is good. (O.o seems a bit creepy in that undesirable way. But one can lock-in, so...?)

Note that with the semi-progression one goes from generally desired, positive traits in 1/2 to something closer to the "wtf, y u wan da?" territory of the Eastern Mystics.
([This reddit](https://www.reddit.com/r/streamentry/comments/62ev8b/community_the_finders_course_techniques_and/) contains a nice description of the meditation practices they use if you're curious.)

Unsurprisingly, there is parallel to the 4 (or 9 here) [Jhannas](https://dhammawiki.com/index.php?title=9_Jhanas):

1. Pleasant Sensations
2. Joy
3. Contentment
4. Utter Peacefulness
5. Infinity of Space
6. Infinity of Consciousness
7. No-thingness
8. Neither Perception nor Non-Perception
9. Cessation (Samadhi?)

(And a friend points one to this [cuter portrayal](http://thequestforever.com/the-ten-worlds/).)

Somewhat similar and the reports on video for lecations 3/4 contained some bits of the higher Jhannas.

### Do We Even Want That?

Fortunately, as if pre-emptively responding to our creepiness-feelings, Jhannas are not seen as permanent states of being. Through practice under guidance of some guru one learns to enter Jhanas as one pleases -- and then they are optional. One can make of them what one will.

(I note Buddhist lore has also responded to this [recent quandary](https://eprints.soton.ac.uk/420273/) that "meditation does not quiet the ego, but boosts one's self-enhancement (sense of greatness)". They say: "One is not supposed to talk too much about jhanic experiences since it is virtually impossible to discuss your jhanic attainment or any powers from the jhanas without inflating one’s ego." Or as was said in Spiderman: "with great power comes great responsibility". How does one do something pretty awesome, something that benefits from not thinking too highly of yourself, without thinking you're pretty cool for doing this awesome thing? -- Well, they do embrace paradox.)

This [blog post](https://meditationstuff.wordpress.com/2014/09/29/deeply-valuable-experiences-meditation-zombies-and-planning-preview-2700-words/) discusses some of the creepiness pretty well. The locations seem to correspond to a progressive shutting off of different brain/cognitive functions from conscious experience. In meditaiton one "quiets one's ego" and trains oneself out of negative chatter, emotions. Then others. At the end, one has shut off most of the cognitive systems normally linked with conscious experience. One seems to maintain the ability to hook into them again. It's as if the poor saps willing to pay 2500$ for it are getting progressively zombified. They are happy as if "everything is as it should be", and continue living the shells of their prior lives, but are ultimately in that place the Eastern Mystics I dreaded are. The Mystics got there in monastic settings, and stayed like that. These people got there in the midst of everyday life and stay like that.

The author claims to have experienced "no-self" for 20min, and disliked it. Was glad it lasted only 20 minutes. On the other hand, the author claims that meditation has helped him beyond the experience of "rational planning that one can't do". That experience I've had of thinking ([sex](/sex)) to exhaustion but not acting "is not in his universe".

The author also claims that some who have meditated a lot and claim "classic enlightenment" avoid these odd negatives, such as "no-emotion". (And pushes Acceptance and Commitment Therapy as helpful in addition to just meditation.) 

This fits the idea that these locations/Jhannas are valuable states to taste, and maybe even to dip into or draw from, but that one may not want to inhabit permanently. (Should one then focus on mastery and control over one's state of being more than whether one is presistetnly or ongoing in a state of "non-symbolic experience"? And somehow getting stuck... ouch? Or as value is subjective, perhaps no-emotion staes are very desirable for some and not for me?)

Which brings me to a friend Logan's gloss of "meditation".
> Meditation :- mind administration

Aside on Buddhist Mentality
--------------------------- 

It's also worth noting that under the worldview that there is [spirit-realm between lives](https://www.goodreads.com/review/show/2326037659) and we (somewhat) consensually/voluntarily choose to incarnate on Earth in easy/hard life-paths, the life-choices of Eastern Mystics make more sense. Especially as the 9th and 10th steps on the [Noble Path for Buddhism](https://en.wikipedia.org/wiki/Pre-sectarian_Buddhism#The_Noble_Eightfold_Path) are: 

* 9."pubbenivasanussati-nana: he recollects his many former existences in samsara"
* 10."sattanam cutupapata-nana: he observes the death and rebirth of beings according to their karmas" 

This was also conveyed quite clearly in the book "[The Jewel Tree of Tibet: The Enlightenment Engine of Tibetan Buddhism](https://www.goodreads.com/review/show/2022455768)".

The Golden Rule is pretty obvious: respect consent. 

We all know it's pretty frustrating when somenoe tries to help you with something you don't want help with. Or when there's a nosy friend and you'd like to do your thing. Maybe you want to learn how to bake a cake from scratch by experiment! Cuz it's fun or whatever. And this friend keeps trying to show you how to bake the best cake ever! If they succeed, you can no longer get that cool experience of learning to bake better and better cakes -- and if they fail, they've just annoyed you. Even though they were kind and "just trying to help". Or what if your mom "cleans your room for you", also displacing some things, causing you trouble. 

Consent must be gotten to interfere with the lives of others, whether with "good" or "selfish" intentions. (No, we can't just demonize "selfish" intentions and then be happy with "positive" intentions. Consent comes first.)

So then what do you do when you "break the fourth wall"? You realize that you're in a work of fiction: is it ethical to tell others that they are also in the work of fiction? First, they'll just think you're crazy. Second, if it's a work of fiction they consented to be in (forgetting they are in it, to make it more realistic), then you're being a pretty big dick. 

Some people may, however, want to break the fourth wall too. But most? Most want to live their life here on Earth. 

You, having realized the nature of reality and gotten bored of "wordly desires", don't have so much interest in forcefully shaping the world "for great good". (Or maybe you are but realize the limited scope of one humans ability and decide the world needs more simple, peaceful, loving hubs.) If you go in the [Yogi Gupta](https://www.goodreads.com/review/show/2357718933) direction, then via clear, unmuddled thought one can lovingly influence the world from your Himalayan mountain retreat. The awakened ones that see a strong need to take a powerful worldly role will be, well, rare. There are other ways to act, and you don't want to spoil the fun play for others. 

Is someone suffering? Well, you'd love to help them, but maybe they're seeking some developmental experience at the moment. Even if they're frustrated, failure is part of trial-and-erro,r the essence of learning. 

To help the few that want to learn what you have, you simply have to sit, wait, and provide means for them to contact you. Hey, maybe you even want to be annoying and confusing to test their mettle: better not spoil them unless they really want it. As Watts said, you'll be a bitch thinking for them with that tough love. Like a compassionate tough mom. 

... Then the confounded Eastern Mystics only make sense if you also take up their cosmology/epistemology. 

Without it, they're senseless and boring.

With it, there's some wry sense to it.

Mental Dexterity, Choice, and Growth
------------------------------------

Wowzers. All that for why meditate. 

It's confusing. Meditation is jumbled up with Buddhism (and then there are oh so many types of that). Ok, there's Taoist meditation too. Prayer could be considered a sort of meditation?

Then many meditation practices will be biased towards, say, specific goals of (whichever flavor of) Buddhism. You'll want to free yourself of negative-thougts that hold you back and make you anxious and stressful in your daily life (or your courtships), and end up doing a practice intended to help spring you out of the narrative, to awaken you to the fact your awareness/beingness/existence spans more than this life on Earth. Hmm. This is how we end up with headlines like: "[Employees who practice mindfulness meditation are less motivated, having realized the futility of their jobs"](https://boingboing.net/2018/06/17/tune-in-turn-on-slack-off.html). There are many instrumental benefits from such practices, yet they work at a more foundational level and bring about other effects too. (Ok, maybe this isn't such a bad one in this case >:D.)

Can we just get what we want and not other things? Maybe. 

Hard to have perfect/total control over one's development and changes throughout life.

> Meditation :- mind administration

That's where this gloss of meditation comes in. 

One wants to administrate one's mental experience. 

"Hey, I've got all this anxiety. I want to tune that down. Maybe throw in some equanimnity in the face of difficulty and some flow-focus when doing work. That sense of Oneness with the Cosmos sounds cool too. Maybe I want that."

Yeah, man. It's all good. And I want the wherewithal to retract some mind-state shift I don't like. 

Do I like having an identity? Or do I want to experience life like [no-one](https://www.goodreads.com/book/show/439332.Being_No_One) awareness with some masks to fit on if (situationally) desired?

### Personal Experience and Meditation's Practical Functionality

It's not hard to see that the standard initial meditation of breath-following is helpful: if you can't even follow your breath over whatever thoughts, feelings, hungers, mosquitoes come up, what makes you think you can change your state-of-being, evaluate it, decide it's undesirable, and shift out? You'll just get carried away, stuck in some weird location/jhanna. Maybe its pros are better than the cons relative to your previous depressed state, but still, dude. Duuuude. 

One often hears that during meditation one shouldn't "think". I feel very strongly that this is misguided, but caring advice. 
Leo Babuta of Zen Habits put it better in his program on "[Turning Uncertainty & Discomfort into Mindful Openness](https://zenhabits.net/the-44/)": "It's ok to have thoughts. Everyone does. What's important is to move your focus back to your breath, and let the the thought pass". 
Some thought time is fine. Just recall that now you are focusing on your breath, not solving some physics equations (that will take more than a few seconds). If the thought takes but a few seconds, hey, maybe you can even conclude the thought before getting back to your breath!

(Whereas if you follow the "no thinking" route, you may cause yourself unnecessary traction. This also ties into how focusing on positives works better than negatives: "Don't think of the pink elephant" and you think of it; it's easier to replace bad habits than to just "not do them anymore"; et cetera.)

Some meditation practices have more clear function than others. 

Initially inspired by Jeffrey Martin's Finders Course methodology in Hong Kong summer 2015, I tried meditating for an hour a day for a couple weeks. The claim is that when meditating for an hour there's a curious qualitative shift around 45 minutes in. I did find this to be true. In part it seems to happen because my internal clock is not that accurate. Around then I start getting extremely strong doubts and desires to "check to make sure I actually set the timer". (Hey, at least this is training in self-trust and persisting in the face of strong self-doubt.) Jeffrey said there were four main categories of eeffective meditation methods: somatic-awareness, cognitive-awareness, cognitive-contents, and symbolic-repitition.  

I tried **mental noting** a few times: whenever you have a thought or observation or feeling you note it with a non-specific label. It could be "thought" or a bit more specific, but nothing you'd have to think about applying. Quite interesting. Obviously useful for mind-administration to hone the habit of observing what's going on in your mind (without getting too caught up in it).

I tried some **mantras**. I tried a Sanskrit one (but don't remember which, not Om Mani Padme Hum). I tried "la illa ha il allah". I visualized as chanting, whatever came to mind. "la illa ha il allah" was very, very trippy, weird shit (as agnostic atheist at that point). Perhaps even more perverse was chanting "fuck you" for an hour straight. Such a weird mind-state. Try it. I was thinking it might give me more of a "fuck you" attitude to life -- something I felt I could do with a bit more of, being on the meek/shy side. It's less clear to me how this helps beyond being a level up of breath-following. The meaning? The sound seems it can put you in a strange state too. Vibrations through your body (in that brain entrainment vibe? :D)

I tried conscious awareness, being aware of my being aware. Was pretty weird, but I didn't get much from it the one time.

I tried **Vipassana-style body scanning** a few times. Was good. Again, strengthening my ability for controlled attention: focusing on sensations in my crown, and then slowly moving a ring of physical sensation awareness around my body. I did it by the water near Sha-Tin, so there were mosquitoes. I was lucky and got to experience a mosquito landing, sticking its sucuker in, sucking, and departing. Later I got to experience (mostly) ignoring another as it wasn't in my area of focus. I've later found that "paying attention to the experience" can help with doing and [not-minding](https://www.youtube.com/watch?v=TvQViPBAvPk) things that are painful (ok, maybe Lawrence of Arabia was a masochist). Eat a hot pepper and focus intensely on the sensation itself and the sensation is, oddly, less painful. Go in the sea in Copenhagen when it's 11C and focus intensely on the sensation of the cold water on your skin, and it, curiously, doesn't feel so painful. Quite a useful life-skill, actually. I can see how this could help with "almost anything" you want to tackle in life. A sort of "life-hack".

My sister reported quite profound experiences with "**Who am I?**" meditation. Whenever you have a thought or something you question "Who am I who has this thought?" This is sort of in the [Advaita Vedanta](https://en.wikipedia.org/wiki/Advaita_Vedanta) tradition.

My dad seems to be a proponent of the simple "**no thought**" style. Is it Zen or Taoist? Maybe both? You sit there and empty your mind. That's it. I have never tried it. 

. . . An hour a day is a lot. I didn't keep it up. 

I didn't do it much at all until October 15, 2017. A few times I tried a long hour meditation again in Copenhagen. Or maybe at some Tantra gathering some meditation. It, like exercise, was in the category of things that would be good/desirable to do but feel like they take too much investment. (Ok, I did Orgasmic Meditation once. Quite cool.) 

Then I wanted to overcome anxiety and free my ass for good, at a deep foundational level. [Become like Luffy](/radical-honesty-or-acting-like-luffy).
I highly recommend the [Zen Habits program](https://zenhabits.net/the-44/). Good stuff. The practices, 11 in total, are 5-15 minutes, done for 4 days each. (And I apologize for spoiling it, as the suspense at what would come next is kinda cool. Muahahahhaa. I do recommend reading his description and watching his video instead of just trying to follow my summary.)

1. Breath Meditation. Nice warm-up.
2. Body Scan. Hi my good friend.
3. Curiosity & Welcoming. This is where things get interesting. Pain, itch, or pleasant, we welcome the sensation. This is taking the intense body sensing a step further and actually welcoming that cold water, not just letting it be as it is.
4. Dropping From Stories to the Body. Now, a bit as in mental noting, we notice our thought streams as stories and drop into body-experiences. This is like teaching ourselves that we can up and exit thought-habits at will.
5. Train in Uncertainty. Maybe this is also a Vipassana practice. It's like what I did with body-sensations but with situational or psychological uncertainty. Recall situations that make you feel uncomfortable or uncertain and this time welcome them. Focus on how they make you feel physically (using those skills we just honed). At least you can welcome the sensations and sit with them. Hi friend. I note this practice seems done in the Orgasmic Meditaiton and Radical Honesty communities as well: one gets into the body and notices how things troubling one makes one feel (physically or emotionally). Helps one ground things and not get stuck in some 'rational' mental loop. Also helps one resist the temptation to just run from an uncomfortable, uncertain situtaion (or avoid water that's a bit chilly). This is, by the way, good done in the field, not just in one's meditation room.
6. Resting in Open Awareness. This is also a favorite. One tries to tap into as many sensory dimensions as one can. One takes in sound, touch, sight, smell in one direction. One tries to take it in without interpreting it, just being aware. Then another direction. After the four directions, one focuses internally. Then one welcomes all the sensations at once (even behind the head to the degree possible). One rests in this open awareness. I felt quite a strange sensation sitting there as all these senses, my sensorium, blended together. Internal and external senses seemed part of a continuous ocean of sense. I'm just there, this bubble in an ocean of awareness. Multi-modal bubble of awarenes. Quite cool. What do I learn from this? How to toggle my interpretations of experiences?
7. Loving Kindness. Metta. A standard. I had underestimated it in the past. [They](https://www.youtube.com/watch?v=r83sdnV2eLU) do say that self-love is important for this personal-development thing, positive psychology thing, etc. I'm after. Somehow quite impactful to sit there actively loving yourself, your friends, flatmates, lover, family, strangers, dogs . . ..
8. Tonglen. This also tries to teach us not to "reject" or "push awya" difficulties. One visualizes taking in anger, sadness, or pain (red, blue, or black) smoke and then sending it out purified into the world. Seems a bit hacky, a bit like saying affirmations into the mirror. Or one is trying on a sort of resolve: "I can make something good of this." One also practices doing this for others :).
9. Tender Heart. This is a sort of strange practice. We take one time we were hurt, emotionally, and look for the tender sensitivity underneath the hurt. To see that underneath our pain is a desire to be loved, happy, at peace. I'm not sure how much this one affected me. Perhaps the intent is to try and replace negative self-talk about our difficulties with a compassionate understanding for our sweet desire underlying them?
10. Bodhisattva Path. Ok, come on, what if we don't want to go the Bodhisattva route?! There are two sides of this one. 
    * Wishing happiness for others. Meditating on what we can do for them. Do I see some dirty dishes? Maybe I can just be kind and wash them for the guy. (Will this overpower my annoyance at there being dirty dishes when I have some of my own to wash?) A lot of my social difficulties are an odd mix of self-protection and fear-of-wronging-others. With a strengthened focus of helping others mixed in, will this become clearer? My mom did think so. This did promote some kind actions of a type I don't usually have. My kindness usually leans toward "not being mean" than towards "active aid" (similar to how I prefer "not making a mess" to "cleaning up"?). Quite weird and kinda nice.
    * Wishing happines for yourself. Not just for yourself, but for the diverse parts of yourself. Some thought? Some worry? Welcome them like a good host. A good host will let all guests into his house. Treat them to some drink or food of the kind they desire. Find them a place to sit. Maybe introduce them to someone interesting or that they'd like. A good host will also keep a guest from causing too much trouble, but find some way without kicking them out. So one welcomes all the diverse thoughts one has. I welcomed my mind observing the thoughts I had. I welcomed my mind welcoming. I experienced one time a chaotic warm spiral of welcoming everything that comes up, yet also not getting too caught-up in any one thing. 
11. Accept Everything. Here we go: back to sitting in the mountains because one accepts everything? Always comes back to this paradoxical theme. Is meditation for personal development a sort of Quixotic practice (especially if motivated to help me with [sex](/sex)? As lust is totally one of the "five hindrances" or "baser desires"? Grr, moralists)? Basically: just accept what you experience. Sit there and accept everything you experience. Do I worry if I'm doing it right? Or wonder how to do it? I accept that worry. I accept that wonder. Does my knee feel a bit awkward in lotus position? I accept that awkwardness. Do I feel an itch? I accept that and accept my hand moving to scratch it! Aha! :D. I accept my acceptance. Sometimes I think this scratching an itch is a metaphor for how one accepts the world and oneself as one is at the same time as acting in the world. Actually, this meditation was quite stellar. Maybe better after the build-up of the previous 10. I experienced another chaotic spiral of accepting everything, recursively up and onward -- AND THEN IT STOPPED. And there was just still, thoughtless silence. Zip. In accepting everything, I ended up in that state Bendad likes: no thought. I find it pleasantly ironic.

The program definitely changed me for good. 

Did it succeed in its mission? To some degree, but not really. 

Of course all Leo Babuta can hope with the program is to get me started and give me the tools to continue working. 

The general methodology does not seem too different from [Acceptance and Commitment Therapy](https://en.wikipedia.org/wiki/Acceptance_and_commitment_therapy) actually, or other psychology methods. Accept and learn to be with the state of discomfort in your life-struggles. That's the first step: no longer running away. See your struggles in a tender, compassionate light (not negative). Focus on what you can do for yourself and others. And staying present on the cusp your desire will sing its siren's song in your ears. Eventually you will act. What else can solve all your worries? (If anything can.)

I can see how these practices help one gain the basic mind-administration skills to start dabbling with different states of mind too.

I kept doing 5-15 minute meditations, often from this selection, sometimes an Om Mani Padme Om, for a lot of 2018 after this. 

I tried some brainwave entrainment YouTube videos as well. The idea is that spamming you with certain frequencies can help your brain get into different states. Like those really deep meditative ones, or ones in which past-life recall may be easier. I also tried brain.fm's meditation track some ("Alpha wave"). Brain.fm's was quite effective at getting one into a state different than just breathing. Quite interesting. Gongs and bells may serve a similar function.

I was initiated to Reiki 1. I also tried meditations of reikiing my chakras (and occasionally some Dru Yoga that seemed to stimulate similarly). That ASMR type crown-of-head tingling started appearing, which is pretty nifty. It can at times appear when reading certain texts or doing some activities too. 

Make of it what you will, but I came to "sense energy" in that sense of things. Say walking around Prague, from trees, in/around my body, in some girl sitting next to me on the bus, etc. I started hearing my heartbeat when meditating too. This is perhaps a topic for another day/post. Mind-training meditation does seem to help one with keen sensation. 

One other meditation practice I tried a lot (15+ times) this year is "**Psychic Development**" of [Yogi Gupta](https://www.goodreads.com/review/show/2357718933) taught to me by [my mom](http://www.aranya.org) (hey, she finally taught me something beyond counting my breath!). It's a series of 8 meditations, actually. 

1. The [Mantra for Purification](https://www.youtube.com/watch?v=lbEQovnsal0) (asking Vishnu the purifier to purify you inside and outside)
2. The Mantra for Unification ("Hum Sa So Hum; So Hum Hum Sa" -- [I am That](https://www.goodreads.com/review/show/2255822742) in Sanskrit)
3. Purifying Pranayama (Pranayama are breathing practices
4. Stimulator Pranayama (humming, so a sort of brainwave entrainment?)
5. Nadi Vibrator Pranayama (stimulating psychic senses by blocking one's normal ones?)
6. Closed Circuit Pranyama (practice mentally directing energy through one's body? Stimulating one's whole body energetically? Some body-scan benefits too?)
7. Psychic Curent Concentration Pranayama (a particular sort of Chakra meditation, starting with base to crown and back with breath)
8. And then, at the end, one does that positive, creative visualization for bringing what one wants into this world!  

I've never learned such a long mantra before:
> Om apavitra-pavitro-va 

>  sarva-vastam gato api-va 

>  Yah smaret pundarik aksham                                                               

>  sah bhaya bhyantara suchih.                                                                          

>  Sri vishnuh sri vishnuh sri vishnuh

And in Sanskrit: 
> ॐ अपवित्रः पवित्रो वा सर्वावस्थां गतोऽपि वा ।

> यः स्मरेत्पुण्डरीकाक्षं स बाह्याभ्यन्तरः शुचिः ॥

Quite interesting learning by heart. It can really put one into a weird tranced out state. Knowing the intent of it can help, I feel, for having some of that effect.

I can't necessarily vouch for the effectiveness of the creative thougth visualization. However as I [noted earlier](/notes-on-steve-pavlinathought-action-visualization-honesty-consent-and-intimacy): the law of attraction can have purely physicalist/materialist means of _working_. Perhaps this procedure primes my mind for properly focusing on my desire.

Anyway, the practice does leave my body feeling vitalized in an interesting, loose way. 

### Concluding Remarks

Recalling those Jhannas/Locations, I have experienced a fair bit of the 1st and 2nd. Some of 3rd and 4th. Not that persistently, but I have some ability to adopt them more than less. 
Also of interest is that I unlocked my expression and feeling of **anger**. I had basically repressed this for much of my life (though my parents tell me I had it enough as a kid). It feels strange and good to express it, although now I'm in that common place of learning ways of living anger that aren't too hurtful to others (along with desiring not to hurt them when angry -- egads!).

It's indeed not all positvie though. I'm personally glad to have any traumatic past memories brought up (as I'd gladly work through them), but [some people](https://www.yogajournal.com/meditation/the-dark-side-of-meditation-how-to-avoid-getting-stuck-with-pain-from-the-past) seem to have this to their surprise or against their wishes. 

The motivation things are weird too. Realizing my best hypothesis is eternal-spirit-nature, which happened through a combination of thought, reading reports, meditation, discussion, experiences, my motivations become shaky. A lot of my desires in life are in part rationally-motivated. And I want to live indefinitely. But if in a powerful sense I already will live as long as I want, then what? My desires to help intelligentify the world and make smart AgI life-forms, too, is in part motivated by the fact that that will accelerate life-extension research/technology! Had the strong feeling that the legs of the table I'm standing on were taken out of under me. That damn big worldview shift. 

. . . Yet the desires remain, albeit not bolstered in the same way they were. I still wish to stay with this body so long as desired (aka indefinitely -- LONGEVITY ENERGY), and the others too. I want to go through this cool Singularity roller-coaster a leading, surfing participator. Nor do I understand the subtleties of spirithood. I'm not yet going the route of those Eastern Mountain Mystics.

It seems to some large degree one can treat meditation practices as mind-administration. One can pick and choose, allow one's mind to subconsciously adapt to one's preferences, the states of mind/being one wants. I can take or leave as I please. I don't have to take in the desired-states of some religion in an all-or-nothing manner :). 

However, it also seems quite hard to promise one won't run into curious (potentially disturbing) surprises along the way. It's a sort of mental adventure and quest, a sort of self-creation. What adventure is it if one has guarantees all the way?

I suppose I'll keep meditating/exercising/administrating my mind as I play in this life. 




