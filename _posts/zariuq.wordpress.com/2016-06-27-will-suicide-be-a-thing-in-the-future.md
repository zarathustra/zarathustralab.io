---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-06-27 15:45:02+00:00
layout: post
link: https://zariuq.wordpress.com/2016/06/27/will-suicide-be-a-thing-in-the-future/
slug: will-suicide-be-a-thing-in-the-future
title: Will suicide be a thing in the future?
wordpress_id: 933
tags:
- blog
- zariuq.wordpress
- future
- futurology
- Hamish
- mindplex
- Monkey Balls
- suicide
- Zar
---

#### Warning: telescopic futurology, worthy of a Hamish, follows.


Over ten thousand people commit suicide every year now. Even after involuntary death is largely obsoleted, will people continue to commit suicide? What about after people no longer need to work to have reasonably luxurious lifestyles? What about after we cure depression, addiction, and gain more control over our moods and qualitative experiences (such as, to use a mundane example, making spinach taste better than ice cream)? What about after we can be uploaded with backups (say via a neural lace)? Will people still choose to voluntarily cease their lives?

One must then ask why people now choose to stop living: generally, suicide is a means of escape. Some form of pain (loneliness, rejection, abuse, guilt, depression, helplessness, embarrassment, confusion, etc.) is unbearable, and rather than deal with life under an unending barrage, cessation of life seems preferable. (The proverbial solution to all problems.) Who would choose the pain? (I hope I would.)

Frankly, all of these are endemic of poor mind set-ups. As are unfortunate individuals with sub-par average degrees of happiness (whereas people are usually moderately happy on average -- the hedonic set-point thing). Clearly some human minds are not susceptible to these severe problems. We will develop the ability to help anyone into such a state.

When we do, suicide for modern reasons will cease to be a thing. There will be other means to escape. When dealing with life becomes unbearable, there will be routes to make life bearable again.

Are there any other, new, reasons to cease living?
People will just get bored of living. They will have done and exhausted everything relatively new and have little interest in continually exercising their intellect. As you would get bored playing Tic Tac Toe ad infinitum.
This a common theme, featured in _Schismatrix_ where there's a stupid hive-mind species with a super-intelligent part that hibernates for millennia on end as no need for intelligence surfaces. Or in _Down and Out in the Magic Kingdo_m where people often 'deadhead' until some pre-defined point or something they deem interesting happens.
Of course, the answer is hinted at here: boredom is no reason to die when you can just put yourself on hold.

Whether such severe susceptibility to boredom is appreciable mental variety or a mental defect is yet another point to debate :x.

There are other ways to, more-or-less, cease living as your individual self without dying per se: such as merging with a mindplex or undergoing a fast, discontinuous mental architecture change.
