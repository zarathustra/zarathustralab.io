---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-08-25 18:18:10+00:00
layout: post
link: https://zariuq.wordpress.com/2015/08/25/things-social-people-seem-to-do-differently-from-me/
slug: things-social-people-seem-to-do-differently-from-me
title: Things social people seem to do differently from me
wordpress_id: 391
tags:
- blog
- zariuq.wordpress
- anxiety
- bubu-bumpkins
- donkey dick
- Monkey Brains
- psychology
- shy
- social
- socialite
- socializing
- Zar
---


	
  * They value socialising as an activity in and of itself.

	
    * Whereas I tend to think about socialising in terms of some purpose :p




	
  * They expect and plan to do things with others, as a matter of curse.

	
    * Whereas I expect to do things alone, and kinda fear getting stuck with someone =.=;




	
  * They don't hesistate to ask what you're doing and to come along.

	
    * Whereas I do.





I think those few things are the jist of it :o
And ironically, I did all those things when working as a teacher >:3
