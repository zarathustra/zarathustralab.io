---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-05-26 13:58:11+00:00
layout: post
link: https://zariuq.wordpress.com/2016/05/26/cute-4-dimensional-analysis-of-suicide/
slug: cute-4-dimensional-analysis-of-suicide
title: Cute 4-Dimensional Analysis of Suicide
wordpress_id: 839
tags:
- blog
- zariuq.wordpress
- assisted suicide
- liberty
- life extension
- suicide
- Zar
---

What does self-change say about suicide?

That is, what does the fact that you are not, now, the same person that you are 20 years ago or from now, say about suicide?
If be both value self-autonomy (liberty) and disallowing murder, what should our take on suicide be?

The standard argument is that one should have the right to kill oneself even if not other people. The argument against is that people shouldn't be allowed to make choices not in their own interest, especially not out of a moment's desperation. (Maybe if it's premeditated long enough in advance...? Or if one is sick . . .? :P)

Well, if you are 20, your 40 year old self is basically a different person. In committing suicide now you are essentially preventing ver existence. You are preventing the existence of a person expected, statistically speaking, to exist. This is pretty similar to murder.

Is there some guarantee your 40 year old self will want to die too? Most likely, no, there isn't. You are killing your future self, a different, albeit highly related, person.



In what sense than can one be said to be killing oneself, and not taking anyone along for the ride? On the deathbed, basically.

Maybe sometime in the future we'll develop ways to figure out if our future selves will also want to commit suicide, so that we will be allowed to not live indefinitely if we want to.
