---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-05-05 13:17:34+00:00
layout: post
link: https://zariuq.wordpress.com/2015/05/05/musings-on-the-nature-of-digital-life-continued/
slug: musings-on-the-nature-of-digital-life-continued
title: Musings on the nature of digital life (continued)
wordpress_id: 379
---

<blockquote>

> 
> 
	
>   1. Order
> 
	
>   2. Reproduction
> 
	
>   3. Growth (development)
> 
	
>   4. Metabolism (energy use)
> 
	
>   5. Realtime Adaption (Responsiveness)
> 
	
>   6. Evolutionary Adaption
> 

</blockquote>


The biting issues I repeatedly run into with digital life and my goal to make it is: when you get down to it, life is pretty trivial. Trivial and boring. It's a paradigm and only as interesting as what it does. ,

The other issue is that the current digital environment is just not conducive to the paradigm. And as I noted last time, the real world is becoming less and less conducive to the paradigm as well (hence mass extinctions).

But let's say what interesting things we can =]

Order is trivial. >_>

And the more intelligent the life-form, the more AI-like, the less it needs reproduction.
Metabolism basically has to do with the energy flow, and in substrate independent life, the metabolism is distinct from the life-form (e.g., an uploaded person would be alive and no longer really intertwined with their metabolism, the computers they're running on). Digital life isn't necessary substrate independent, but it's closer.

Reproduction and Evolutionary Adaption aren't really necessary. However, they point at various useful constraints. Constraints that say interesting things about the other properties.
EA demands efficient energy use (metabolism minimization), as those that do so will flourish better. EA also demands quick reaction time, again for the obvious survival benefits. Quick reaction time means that memory is needed (in one form or another, unless insanely simple). And energy minimization (yay laziness!) means that the memory has to be compressed.
The more a life-species depends on EA, the more important reproduction is. Further, the reproduction is minimal to provide the next generation with more flexibility to adapt to new environmental features.

Growth derives from these requirements. The life-species must have a compressed memory it fills up anew each generation (though survival also demands they don't start tabula rasa :p).

Realtime Adaption basically requires learning, and the degree of RtA correlates with intelligence. RtA and memory also lead to Growth, as adaptations to new phenomena require additions to the memory. The stronger, and more general, RtA is, the less useful Reproduction and EA are. Further, if a life-form is flexible or adaptable enough, then memory doesn't really need to be thrown out during reproduction. (Does it?)

The difference between digital life and AI?

Life doesn't have to be intelligent xDD
AI just needs Adaption and Growth :P

Hmm, I suppose the interesting thing is in combining them: AI with autopoietic structural integrity and autonomy. But structure and autonomy are only meaningful with respect to doing something...
