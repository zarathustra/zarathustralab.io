---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2017-12-02 16:53:33+00:00
layout: post
link: https://zariuq.wordpress.com/2017/12/02/on-abuse/
slug: on-abuse
title: On Abuse
wordpress_id: 1397
tags:
- blog
- zariuq.wordpress
- abuse
- life
- musings
- Personal Development
---

Lately I read a bunch of Wikipedia articles on abuse, a few focusing on trauma. I have in various ways been traumatized in my past.

I am also aware that it is likely impossible to discuss this topic, abuse, without triggering or offending someone. Someone may feel I speak too lightly of abuse, but veer in the other direction and I take it too seriously. Do I let evil sadists off the hook or do I vilify mere humans?

What is abuse though? Legal definitions, as they come with serious ramifications and punishments, must be more strict than pure philosophical ones.

In some regard, the laxness and openness of Wikipedia is helpful here. One can see a more full picture of the sociocultural understanding of the concept, rather than any particular group's controlled view. (Ok, this isn't perfect. Wikipedia editors can be quite opinionated [citation needed].)

Abuse seems to include anything that can cause someone to be traumatized, to become emotionally/spiritually repressed, for a long period of time. Dealing with issues of causality, some assert that abuse must be intentional: accidentally kicking someone in the nuts is not abuse, even if the victim remains jumpity whenever there are kicking motions nearby.

What does this effect on one's being mean?

I like to envision a person as some sort of flying sphagetti monster growing out and expanding its noodly appendages in directions of interest, exploring the Cosmos and intermingling with other flying sphagetti monsters. For the following analogy, I'll compare to plants growing in some (pattern)space.

An interesting note that has often confused me is the difference, both moral and practical, between abuse and discipline. Is it ever really ok to control the growth of another living organism, at least beyond simply asserting your own [boundaries](https://markmanson.net/boundaries) (a la Mark Manson)? Spanking and other physical punishments seem to be being realized for what they are, nonetheless there seems to be an emotional primalcy in how one is actually affected by what happens.

If there is a clear, firm rule, a clear environmental punishment, then we grow around this like a pillar. And as in any garden full of coexisting lifeforms, such boundaries will exist.

Reasons seem important: with reason and understanding, the mind can find the exact nature of the boundaries.

Without these, a mind may learn restriction and discipline as the way of the world. Arbitrary barriers are placed, and then that is what the organism does too. The organism places retrictions on others for whatever the organism desires, and sees to it they are met with cold, strict precision. One can get a strange pathology (-- or simply way of being --) of discipline for discipline's sake. Culture for culture's sake. Tradition for tradition's sake.

Clarity seems important. Without clarity, punishment is hard to understand.

Take for example the following concept:


<blockquote>A COEX System (Systems of Condensed Experience) is a concept coined by Grof (1976) to describe the way the human brain organizes its experience. A COEX is basically a set of related experiences organized around a powerful emotional center. [[2](https://spiritwiki.lightningpath.org/index.php/COEX_Systems)]</blockquote>


Minds seek concise understanding of the world. With clarity and punishment, you are a careful gardener and the bonsai can grow beautifully. Without clarity and harsh (emotionally at least) punishment, the mind knows not to expand and explore in this general direction but not exactly what to avoid. The mind may develop some condensed representation of this threat. The mind can become plagued with anxiety as it has a strongly charged COEX but doesn't know how wide the barrier spreads -- worse yet, the punisher may have been unreliable. Sometimes far out, sometimes close to center.

And then that horrible tragic trauma forms: a whole region of experience-space ends up blocked for the poor mind left in fear of an ambiguous punishment lurking in the dark unknown.

With clarity, some areas of experience-space are blocked yet the mind can grow around them and explore the general regions anyway.

Boundaries between concepts are generically fuzzy in reality though. Is the difference between abuse and discipline always clear-cut?

With this view, one can see why what exactly defines abuse is hard to pinpoint. The nature is subjective.

If an entity gets brutalized yet accepts such brutality as simply how the world is, the entity won't wilt away from this part of experience-space the same. The entity may approach it with a willingness to fight or sneakiness. A caveat is the entity may, especially in the human case, be more likely to dish out brutality as well. But then this is how the world is, so Bob's my uncle.

What if one regards wild, confusing, non-regular punishments from a specific agent or two as spcific to these two agents and not general features of the world. Children -- and maybe humans in general -- seem to have a tendency toward this generalization that the whole world may be like this. Moreover, in forming a COEX, this simplification is an easy one to make -- even if harmful in the long run. Simple short term strategies often are. Viewing transgressions as specific to the perpetrators, one then gains a conceptual clarity in mapping out experience-space despite the lack of clarity in the violent acts: one may wilt around the perpetrator yet not in general ;).

Combine the above two perspectives: the violence is particular to some agents and that's just how they are. Now one has fair conceptual clarity and choice to make around the violent entity: be cautious and not fully alive near them or risk and be ready for aggression.

This isn't all good, but the mental state can be clear and trauma-free.

Abuse is in the eye of the beholder. (Which seems to have been used in a [paper title](https://www.ncbi.nlm.nih.gov/pubmed/8310277).)

The way one emotionally/psychologically takes a situation can easily matter more than what actually happens. Alas, most humans are not enlightened to the point they can burn themselves with equanimity as monks. Worse yet, most humans can have composure beaten out of them. Thus with persistence, most humans can be abused -- and this is where legal measures against abuse come in.

Hmm, on the matter of discipline and blocking regions of experience-space, there's the issue of, for example, a parent teaching a child not to have sex before marriage (or any other cultural rule for its own sake). The child learns this rule of the world in a clear way and grows around it. If the topic is sufficently demonized, the child may experience an odd pain/pity seeing many people do bad things. The child can grow up and live a lovely, beautiful, enriching and fulfilled life :). Yet, the child will also completely miss a whole region of experience-space compatible with a freedom-respecting society as a consequence of this arbitrary rule benig taught. The end-result is quite similar to a traumatized individual, except without the negativity. Yet this option is, to some degree, available: choose and accept avoidance to the point it's a non-issue.

What type of transgression is an 'excessive' limitation on a mind's experience-space exploration, even if not accompanied by trauma? Even if internally welcomed as part of the world?

- - -

Rounding off, various statistics seem to indicate that mild abuse is fairly rampant. Perhaps 30% of couples will face (mutual) mild abuse if they last long enough. Children can easily find some way to form some broad fearful COEX. Or perhaps they'll be bullied? After all, we're still more raw animal than we like to admit.

Can one at least avoid abusing others?
Perhaps not. Perhaps standing up for one's self with strong boundaries will be sufficient to wilt someone else. One can strive to not do so.

My stance here is to focus on recovery. Focus on clarification, understanding, reconsolidation of previous-learned maps/laws of experience-space.

Accept one will run into trouble and learn/master the process of overcoming :)


