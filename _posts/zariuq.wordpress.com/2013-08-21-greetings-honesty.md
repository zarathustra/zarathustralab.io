---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2013-08-21 15:00:53+00:00
layout: post
link: https://zariuq.wordpress.com/2013/08/21/greetings-honesty/
slug: greetings-honesty
title: Greetings - Honesty
wordpress_id: 3
tags:
- blog
- zariuq.wordpress
- Attraction Institute
- honesty
- just do it
- lying
- Models
- putting self out there
- real
---

I make interesting observations and ideas all the time.  All I do with them is to share them with some friends (and to think through them myself).  This is okay.  I doubt I have much _real_ need for a blog.  However . . ..

I like honesty.  I think honesty is usually the better choice when relating with the world and other living beings.

Don't get me wrong.  Sometimes lying is the better choice.  These are the exceptions though, the serious exceptions.  Lying can also be really fun.

It's everything in-between where I think honesty is better.  If we are honest, we can more easily gauge the state of affairs and act accordingly.  If we're not honest, we will be less clear and have more trouble dealing with matters.  Heck, matters will be made more complicated and contradictions will remain subdued but not removed.  Similar to medicine that does nothing but hide the symptoms.  Thus, yeah, I like honesty.

Oh gosh, can one make an argument for honesty most of the time based on Occam's Razor?  Intuitionally, the cases when honesty seems a worse choice, the other choices are 'simpler' (whatever that means).  Interesting.

Now, now, what does honesty have to do with starting a blog?

There are many things I want to do that I don't do.  I even think I need to, ought to, and should do many of these things.  They often require significant energy, dedication, and perhaps courage to push through fear though.  I compromise and go for something I want to  do and enjoy doing a lot that is easy to do.  Of course, if all I really want is 'happiness', this will get me plenty of some types of happiness.  Nevertheless, I leave myself unsatisfied; I end up not doing many things I want to.

There's a proverb that says, "actions speak louder than words."  Actions speak.

In not doing things that I want to, I am saying to myself that I don't want to.

I'm not being honest.  I'm also complicating things.  In not being honest to myself, I create all sorts of interesting problems.  If I don't do something, do I really want it?  And then I can spend a bunch of energy wondering what I really want.  Maybe I want to want to do it without exactly wanting to.  I can analyzing things based on conflicting desires.  This one may be an accurate method of analysis.  My desire to do A and my desire to minimize energy expenditure (or risk).  This is just like with honesty.  People often hold back on their honesty to protect peoples' feelings or to make things smoother.  Sometimes it works.  If it's trivial enough, maybe _it really isn't worth expending energy on even though we want it a bit_; sometimes small white lies may be worth it (though one can argue they have the potential to complicate things later down the line).  Just as these matters can be dealt with in one go when we choose honesty, honestly accepting that I want to do it even if some energy expenditure or risk is involved works wonders.

That was long.  It's similar to, "where there's a will, there's a way."  In choosing to be honest to myself, I find the way to do what I want rather than shying away because the path isn't smooth enough.  Rather than wasting energy on thought/word games, the real communication or work will take place.

If I stick to honesty, I'll do what I want.  Waiting for the spark that gives me enough energy to overcome my drive to minimize energy expenditure to honestly speak with my actions will stop.  Because I will be being dishonest while waiting.

This is where the blog comes in.  I'm scared of publicizing my views and opinions.  What if they're not the best?  What if they are worded poorly?  What if I change my mind?  I'm thinking dishonestly.  Rather than simply expressing my current thoughts and views (before I become a different Zar), I'm over-complicating matters.  I'm overly-cautiously monitoring what I say lest it somehow cause trouble.  Again, the same reason people are annoyingly dishonest.  Repression is just passive dishonesty.  This is why I'll start a blog (and just because I enjoy rambling): to honestly put myself out there.  More importantly, and less shakily, I want to put my views out there, so not doing so is dishonest.

Honesty also comes in elsewhere in my life.

I have spent ages (years) wanting to put more energy into finally learning to go out and find women whom I like and get along with, without really doing so.  I slowly and surely became less and less shy _in general_, but only did more than that slow, passive improvement during short blips in energy/motivation.  (Eeck, I hope this isn't just one of those.)  In not going out to find them, I am being very dishonest.  I am, through my actions, telling myself that I don't care.  I tell myself so forcefully and consistently that I smoothly transition from an active blip into other interests as if I don't want to look for them.  This is all good and fine, _except I do care.  I do want to._  I am also being dishonest to every attractive or interesting woman I see. While I may show some signs of interest, in not acting on it, I force myself to also show disinterest._
_

I also want to rule the world, lead it in a good direction (by my standards) and to create new life-forms, some of the future, some maybe not.  This task easily overwhelms me into dishonestly not working on it, or at least just busybodying over attaining miscellaneous knowledge that may help broaden my general understanding of the universe.  Hey, that is one thing I want to do.  Or I could be honest and do what I can.

In conclusion, many of my standstills seem to have to do with not being honest.  I'm not going to get much pizza hovering around the pizzeria not honest enough to go and ask for it, now am I?

Since I have always liked honesty and felt we're better off just being frank most of the time, this realization is quite shocking.  How un-honest I've been.  I have to thank Leigh Louey-Gung ([AI](http://www.attractioninstitute.com/)) and Mark Manson ([Models](http://markmanson.net/books/models)) for showing this to me.

Let's see how liking honesty and being aware of this helps one be more honest.  ;)

What to expect from this blog, from my ramblings?  Good question.  You'll have to wait and see what interesting ideas I run into next.
