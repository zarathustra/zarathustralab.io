---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-01-11 14:35:00+00:00
layout: post
link: https://zariuq.wordpress.com/2014/01/11/what-dhammapada-verses-apply-to-me/
slug: what-dhammapada-verses-apply-to-me
title: What Dhammapada Verses Apply to Me
wordpress_id: 137
tags:
- blog
- zariuq.wordpress
- Buddha
- Buddhism
- Dhammapada
- distraction
- essence
- evil
- flower
- fool
- hedonism
- heedful
- live
- lust
- Narada
- poison
- sage
- savior
- self
- world-upholder
---

I've read a bit about Buddhism with varying degrees of indirectness, but never read much that was 'said by the Buddha'.

The Dhammapada seems a good place to start. I'm reading the Venerable Narada’s rendition. While it is rather self-centered and somewhat contradictory to the contents, it seemed interesting to compile a list of verses that seem to apply to me (hey, we're advised to come back to these rather than just reading through them). And what better place to do that than my blog?

So, here goes the long list:

7. Whoever lives contemplating pleasant things, with senses unrestrained, in food immoderate, indolent, inactive, him verily Mara overthrows, as the wind a weak tree.

11. In the unessential they imagine the essential, in the essential they see the unessential -- they who entertain wrong thoughts never realize the essence.

13. Even as rain penetrates an ill-thatched house, so does lust penetrate an undeveloped mind.

19. Though much he recites the Sacred Texts, but acts not accordingly, that heedless man is like a cowherd who counts others' kine. He ha no share in the fruits of the Holy Life.

21. Heedfulness is the path to the deathless, heedlessness is the path to death. The heedful do not die; the heedless are like unto the dead.

33. The flickering, fickle mind, difficult to guard, difficult to control -- the wise person straightens it as a fletcher straightens an arrow.

35. The mind is hard to check, swift, flits wherever it listeth: to control it is good. A controlled mind is conducive to happiness.

38. He whose mind is not steadfast, he who knows not the true doctrine, he whose confidence wavers -- the wisdom of such a one will never be perfect.

48. The man who gathers flowers [of sensual pleasure], whose mind is distracted, and who is insatiate in desires, the Destroyer brings under his sway.

49. As a bee without harming the flower, its colour or scent, flies away, collecting only the honey, even so should the sage wander in the village.

51. As a flower that is lovely and beautiful but is scentless, even so fruitless is the well-spoken word of one who does not practise it.

61. If, as the disciple fares along, he meets no companion who is better or equal, let him firmly pursue his solitary career. There is no fellowship with the foolish.

63. The fool who  knows that he is a fool is for that very reason a wise man; the fool who thinks that he is wise is called a fool indeed.

64. Though a fool, through all his life, associates with a wise man, he no more understands the Dhamma than a spoon the flavour of soup.

66. Fools of little wit move about with the very self as their own foe, doing evil deeds the fruit of which is bitter.

78. Associate not with evil friends, associate not with mean men; associate with good friends, associate with noble men.

81. As a solid rock is not shaken by the wind, even so the wise are not ruffled by praise or blame.

100. Better than a thousand utterances, comprising useless words, is one single beneficial word, by hearing which one is pacified.

123. Just as a merchant, with a small escort and great wealth, avoids a perilous route, just as one desiring to live avoids poison, even so should one shun evil things.

141. Not wandering naked, nor matter locks, nor filth, nor fasting, nor lying on the ground, nor dust, nor ashes, nor striving squatting on the heels, can purify a mortal who has not overcome doubts.

160. Oneself, indeed, is one's savior, for what other savior would there be? With oneself well controlled one obtains a savior difficult to find.

167. Do not serve mean ends, Do not live in heedlessness. Do not embrace false views. Do not be a world-upholder.

168. Be not heedless in standing [at people's doors for alms]. Observe [this] practice scrupulously. He who observes this practice lives happily both in this world and in the next.

207. Truly he who moves in company with fools grieves for a long time. Association with the foolish is ever painful as with a foe. Happy is association with the wise, even like meeting with kinsfolk.

Hey, I think he's got me pretty figured out in this next one:

209. Applying oneself to that which should be avoided, not applying oneself to that which should be pursued, and giving up the quest, one who goes after pleasures envies them who exert themselves

223. Conquer anger by love. Conquer evil by good. Conquer the stingy by giving. Conquer the liar by truth.

239. By degrees, little by little, from time to time, a wise person should remove his own impurities, as a smith removes [the dross] of silver.

241. Non-recitation is the rust of incantations; non-exertion is the rust of homes; sloth is the taint of beauty; carelessness is the flaw of a watcher.

274. This is the only Way. There is none other for the purity of vision. Do you follow this path. This is the bewilderment of Mara.

276. Striving should be done by yourselves; the Tathagatas are only teachers. The meditative ones, who enter the way, are delivered from the bonds of Mara.

280. The inactive idler who strives not when he should strive, woh, though young and strong, is slothful, with [good] thoughts depressed, does not by wisdom realize the Path.

290. If by giving up a lesser happiness, one may behold a greater one, let the wise man give up the lesser happiness in consideration of the greater happiness.

302. Difficult is renunciation, difficult is it to delight therein. Difficult and painful is household life. Painful is association with those who are incompatible. Ill befalls a wayfarer [in samsara]. Therefore be not a wayfarer, be not a pursuer of ill.

316. Beings who are ashamed of what is not shameful, and are not ashamed of what is shameful, embrace wrong views and go to a woeful state.

320. As an elephant in the battlefield withstands the arrows shot from a bow, even so will I endure abuse; verily most people are undisciplined.

329-330. If you do not get a prudent companion who [is fit] to live with you, who behaves well and is wise, then like a king who leaves a conquered kingdom, you should live alone as an elephant does in the elephant forest. Better it is to lve alone. There is no fellowship with the ignorant.Let one live alone doing no evil, care-free, like an elephant in the elephant forest.

Another one about me. Yay monkeys ;).

334. The craving of the person addicted to careless living grows like a creeper. He jumps from life to life like a fruit-loving monkey in the forest.

410. He who has no longings, pertaining to this world or to the next, who is desireless and emancipated -- him I call a brahmana.

 
