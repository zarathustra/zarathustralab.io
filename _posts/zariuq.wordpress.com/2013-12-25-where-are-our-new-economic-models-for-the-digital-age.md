---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2013-12-25 05:04:46+00:00
layout: post
link: https://zariuq.wordpress.com/2013/12/25/where-are-our-new-economic-models-for-the-digital-age/
slug: where-are-our-new-economic-models-for-the-digital-age
title: Where are our new economic models for the digital age?
wordpress_id: 64
tags:
- blog
- zariuq.wordpress
- ads
- amazon
- anime
- brevity
- digital age
- e-books
- economics
- manga
- microdonation
- micropayment
- music
- new economics
- piracy
- pirates
- scanlator
- shame
- teamwork
---

I like manga, music, and anime. I've enjoyed them on many of their various mediums.

Manga scanlators are sometimes surprisingly good. They translate and edit manga in a few days with great quality. Sometimes their versions may even be better than the official versions that come out way later. So I've wondered why the publishers and good scanlators don't try to work together. It's such a waste to re-do the work they already did so well.

The old business models don't work so easily with the new medium, but ignoring and shaming such good work in such a good medium because we don't know how to deal with it is truly a shame.

This got me thinking. In their time, the old mediums were the most efficient and best we had. Many people want to read the stories or hear the music. We can broadcast music on the radio to everyone, but then we hardly get any choice as to what or when we hear it. So the next option is to create many CDs (records or tapes) and to print many volumes of manga. The best way to distribute many printed volumes of manga is to have some central publishing companies do most of the work*. With physical music mediums it's the same. A friend could copy a few for you, but that's not at all efficient on a large scale*. So, the old way of doing things was the best we had.

But technology changed. The way the world works changed. That's no longer the best way of doing things.

For example, imagine we could all replay music in our heads. We can replay music so well that CDs are pointless. Once we hear a good version, we can replay it. These are humans that for some reason have incredibly good musical memory. All we'll need is radio and places to go to hear songs. We'd have music stores, but they'd be places with lots of CDs and headphones/speakers.

Now, what if we had superior vocal cords that could actually play the music we remembered (like the Puppeteers in Ringworld)? We often wouldn't even need to go to those music stores. The business model would be completely different. The economics of music/manga/anime/whatever distribution should clearly depend on the underlying reality.

So how did the underlying reality change? Digital data is 'infinitely' duplicable. It's easily searchable too. In this dawning digital age, what is the most efficient way to get music and manga to people? Why, we just need to make sure we have one copy of each song or chapter in somewhere accessible by search engines. And I guess to spread out the bandwidth burden, we'll want to have multiple sites around the globe.

If something like this is most efficient in our current world, then the economics of music/manga/anime should revolve around these hosting sites. But they don't officially. Officially they cling to pre-digital-age methodology and fight the new mediums. They fight the people and groups who step up and do things intelligently in their stead. For there is a second 'economy' of music/manga/anime that does things efficiently in accordance with the underlying technology. But they don't try to work together. It's quite bewildering.

I understand a large part of the problem is not knowing how to give the creators the credit they deserve with the new mediums. Of course, we no longer need separate physical things for every person, so there is no way to expect to profit directly from every 'copy'. Attempts at doing so will ultimately be awkward and overcomplicated. It's good that they at least try something though. I'm glad they're really trying with e-books (and music a bit). Although, still, buying e-books from Amazon can be awkward and it feels more like renting than buying. Even so, we can have better debates on the issue thanks to their attempt.

But, back to the super-human example: if we could replay and remember music on our own, there would be no rational way to restrict sharing of music. There would be no real way to say that if you want a song, you have to hear it in the music store or radio, not from a friend. It's the same idea.

Digital age distribution is fundamentally different, so we'll need fundamentally different ways to credit the creators, editors, etc.

Two approaches one thinks of off the top of one's head are ads and micropayments. I don't see many manga or anime hosting sites asking for donations anymore, but they all have ads on the side. I'll take this as a sign that they are getting enough (if not more) from ads. I don't buy things from ads and I haven't bought things from commercials, but some people seem to buy from both. So it kinda works out. If they make more than enough, some of this could theoretically go to the musicians or manga artists and editors :).

Also, it's not as if everyone who reads manga online or listens to music on youtube is strongly against paying the artists for their works. But when the currently free mediums are (far) more efficient and you don't have to invest in something you don't like, there is little temptation to. Nor can we ask people to wait a decade as we work things out. An easy way to make micro-payments -- or donations -- may help with this.

Coupling ads with microdonations might get us somewhere. Are there any other natural ways to credit artists in the new digital environment?



Footnotes:

1. Ack. I've got to work at brevity.

2. Whoa, such pro-level scanlating may be a sign that we are approaching a gift-based economy. Cool ;D.

3. I've ignored the issue of bootlegging completely :3. But, damn, what if there was no 'proper' competition for bootleggers?
