---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-12-31 20:02:36+00:00
layout: post
link: https://zariuq.wordpress.com/2016/12/31/new-years-resolutions-again/
slug: new-years-resolutions-again
title: New Year's Resolutions... again
wordpress_id: 1326
categories:
- New Year's
tags:
- blog
- zariuq.wordpress
- courage
- free
- New Year's Resolution
- noporn
- novel
- Steve Pavlina
- Zar
---

## 2016





	
  * I got better at asserting my will -- hard to characterize how much though.

	
  * Detailing plans specifically without actually working through them is surprisingly difficult, however I gained some good clarity on how to proceed, and will be doing some work on my Grand Goals early 2017!

	
  * Vegetarian is often not as bad as it seems, and there are often many good options. Lack of options is one of the worst parts though, not to mention meat is just good, as bread is (from when I was Paleo). **I did make an exception for vacations.
All in all, I'll go back to eating meat and supporting in-vitro (cultured) meat. The main takeaway is to not bother with meh-meat.

	
  * The [selfie a day](https://www.instagram.com/zariuq/) worked pretty well.
My psyche has been altered by this simple ritual. I've gotten a bit better at _the angle._ And I've become a bit more photogenic, comfortable with how I look, and conscious of it :o.




## 2017


Now it's time for this year's.
The themes may be openness (putting oneself out there), being true to one's values, and getting shit out of the way.



	
  * **Write Novel**. 40k+ words. Complete and utter bullshit fiction is OK.

	
  * **No porn** -- err, at least once a week.
It's hotly debated: what will the effects be?
I know one: I'll get way better at imagining naked ladies :p

	
  * **Live my philosophy**.
Yes. It's time.
O Captain Courage possess me please!

	
  * **Free my ass**.
Yes. It's time.
I no longer accept delay in a cramped lifestyle inside my anxiety bubble.
I pop it.
What does this mean?
While there are many manifestations, they basically boil down to:
>> No more avoiding action because of fear*.
Which often involves putting on a show for others.


The last two are the biggies and tightly interlinked.
They will, I predict, involve much cheesiness and courage.
In freedom I am biased, as I [wrote earlier](https://zariuq.wordpress.com/2016/04/30/radical-honesty-or-acting-like-luffy/), to openness, directness, and honesty. And as I wrote there, playfulness often negates this bias >:p

I experience the largest attention, energy and time drains chained by anxiety in social situations: connecting with lovers, comrades, intimate friends, social and group organization, entrepreneurship, etc. However, I also experience some in fear-based inhibitions to working on cool projects. I suspect there will be various unexpected ripples.

As I [wrote about just now](https://zariuq.wordpress.com/2016/12/31/notes-on-steve-pavlinathought-action-visualization-honesty-consent-and-intimacy/), in addition to **courage**, I cohere my thoughts, beliefs and philosophy. I test the detailed visualization of how Free Zar acts. I accept my feelings of fear, and the potential consequences of my feared actions.

... time to go enjoy the fireworks ;-D





*With the usual exception of serious / life-death situations.
