---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-07-06 04:05:49+00:00
layout: post
link: https://zariuq.wordpress.com/2014/07/06/first-person-triumph-why-im-no-longer-a-czarist/
slug: first-person-triumph-why-im-no-longer-a-czarist
title: 'First Person Triumph: Why I''m No Longer A CZARist.'
wordpress_id: 328
---

A long tirade went here. It mysteriously disappeared. I'll sum it up.

The first basic tenant of [CZARism](http://jagatsahaya.weebly.com/) is: "Everyone believes that they should act for the maximum benefit of all present and future sentient beings at all times."

I no longer see why I should value other sentient beings in general.  There may be cases where I value a course of action disharmonious to many other sentient beings. Such as, uhh, my choice to eat meat.

Survival of the fittest versus cherishing all sentient beings? Hmm, hard choice...
