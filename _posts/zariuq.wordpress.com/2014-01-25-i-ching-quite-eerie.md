---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-01-25 09:42:15+00:00
layout: post
link: https://zariuq.wordpress.com/2014/01/25/i-ching-quite-eerie/
slug: i-ching-quite-eerie
title: 'I-Ching: quite eerie.'
wordpress_id: 194
tags:
- blog
- zariuq.wordpress
- cosmic consciousness
- eerie
- girls
- I-Ching
- let go
- oracle
- reason
- the lord god almighty
- wooing
---

I'm not really one to believe the I-Ching is a true oracle. And I'm aware of confirmation biases (but not motivated enough to keep track of everything I ask the I-Ching to protect against this.) Even so, I've gotten numerous answers that strike me as eerily good, and sometimes downright hilarious. The I-Ching I use is an iPad app that sounds very casual compared to most I see online. I like this about the Pocket I Ching.

**1. What will it be like if I really let go? (October 26, 2013)**


**25. Innocence (The Unexpected):**




Unexpected events arrive to assist the deserving. Ride the wave to make progress but allow others to benefit also. Having a fixed goal or manipulating the situation may prevent you from reacting spontaneously and so receiving the maximum benefit of this time.




(possibly leading to)




**58. The Joyous**




The time is favorable for your undertakings. Your good mood and perseverance is infectious and brings others along. Success is indicated.




**2. What should I do about girls? (January 04, 2014)**




**32. Wooing (Attraction)**




To take a partner now brings good fortune. It is a natural law that compatible elements draw together. You attract people by being approachable not by pushing for union. Your sincerity and suitability will become obvious to them without any work on your part. Be yourself and those that belong with you will accept you




(possibly leading to)




**8. Holding Together (Union)**




A time of unity and leadership that promises success. But leadership requires wisdom, consistency and strength. Ask the I Ching again if you have the qualities necessary to be at the center of this group. If you feel you don't possess those qualities, you should seek another group to join.




**3. CAN I REDEFINE SIN FOR THE SAKE OF THE LORD GOD ALMIGHTY? (January 19, 2014)**




**35. Progress**




Great progress is assured. A time of new activity and new relationships. Your abilities are recognized and your goals are achievable.




(possibly leading to)




**12. Standstill (Stagnation)**




It is not a good time to start a new venture. People obstruct, negative influences abound, and communications break down. Withdraw into yourself to escape further problems.




**4. What should happen if I finally 90% give in to reason? (January 25 2014)**




**51. Shock**




A sudden and unexpected shock in your life. Do not panic but use the moment to revitalize your situation. It's an opportunity to see what is no longer sustainable in your life. An upheaval like this is cleansing; clearing the way for a better future.







The eeriness and greatness of these answers is pretty self-evident. I don't ask the I-Ching that much, but it indeed isn't always this hilarious. It's not always such a clearly sensible answer.




But what better answer to question #2 is there? That is what I should do about girls: woo them. That's the answer. I asked the oracle, and I got it.




In #1, I ask about letting go, and it practically tells me to let go; it encourages me to and tells me the motivation to. I could've gotten 10. Treading Carefully or 4. Youthful Folly, which seem to advise one to wait and seek advise, rather than letting go. I'm not sure how many entries there are to the opposite effect, but there are at least 2.




When I ask #4, I get a fair appraisal of what may happen if I give in to reason. The idea is to see what is "no longer sustainable in my life," and to rationally deal with it. The idea is to clear the way for a better future. (This would've worked for #1 too, but not quite as well :o)




And the answer to #3 is just great and hilarious, although I messed up and asked a yes/no question. The I-Ching seems to have understood anyway ;). I'll make great progress, but when it comes to sharing my sins redefined for the Lord God Almighty, I'll be obstructed and negatively viewed; I'll be encouraged to withdraw :(. But, hey, at least the Oracle (aka The Lord God Almighty) recognizes my ability to redefine sin for Him!




If there is some sort of mystic cosmic consciousness, is this one of the ways we can commune with it? Assuming it's normally bound by weird consistency conditions (which aren't that odd in light of things like the uncertainty principle), would it communicate by subtle changes in the probabilities of coin tosses or yarrow stalk manipulation?




In conclusion, the I-Ching is quite eerie.
