---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2013-12-25 03:27:00+00:00
layout: post
link: https://zariuq.wordpress.com/2013/12/25/long-time-no-see/
slug: long-time-no-see
title: Long Time No See.
wordpress_id: 52
tags:
- blog
- zariuq.wordpress
- codify
- enervate
- forcing
- habit
- ideas
- let go
- post
- slack
---

Putting myself out there and keeping up a blog is harder than I'd thought. Perhaps because I don't really make progress in line with my earlier posts. But the idea isn't just to journal my progress, is it? It's for anything.

If I have ideas, post them. If I have insights, post them. If I want to tell a little story, heck, post it. If I want to curse someone out, why not post it. -- That's the general idea.

 

Forcing myself to take the actions I need to for my goals doesn't seem to work that well. Perhaps it should come naturally, but I've spent a lot of time codifying non-useful habits into my system and making a big deal of the goals seems to enervate me.

So, I'm trying the let go approach. I let go and don't take them too seriously, and perhaps I'll build up to naturally working toward the goals. I hope it works, but I can't hope it works as that is not letting go. . . . . . ..
