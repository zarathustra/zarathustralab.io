---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-05-15 12:27:07+00:00
layout: post
link: https://zariuq.wordpress.com/2016/05/15/the-language-experiment-update/
slug: the-language-experiment-update
title: The language experiment update . . .
wordpress_id: 754
tags:
- blog
- zariuq.wordpress
- Chinesepod
- chineseskill
- duolingo
- fluent in 3 months
- lang-8
- language experiment
- SpanishLearning
- Zar
---

Okay, my minimalistic experiment with studying languages consistently but in bite-sized chunks of time continues.

Currently, I've worked through [ChineseSkill](https://play.google.com/store/apps/details?id=com.chineseskill&hl=en), up to Intermediate on [ChinesePod](https://chinesepod.com/), and am now learning about 10 characters a day and choosing a sentence of study a day (preferably with a new funny twist or new grammar point). It seems that it's hard for me to remember the TONES without associating them with the character very explicitly. Remembering tones on a character-by-charcater basis seems most effective so far. I'm also remembering the character written in the tone's color (1-light blue, 2-pink, 3-dark green, 4-red), which seems to help a bit. Alas, at this pace, it'd take a year to know enough characters :'(.

I wonder how far I could take what I've learned if I spent more time trying to have basic text conversations online (and maybe 10% of that in offline), but that'd be a larger time-investment. It could lead to burning out :p

In Danish, which I started less than a year ago, I'm almost done with [Duolingo](https://www.duolingo.com/). Sometimes I understand surprisingly much when Danes are speaking; othertimes, almost nothing at all. I'm becoming able to string basic sentences together a bit. Being in Denmark is almost cheating.

I finished Spanish Duolingo a little while ago. Unlike the people who rushed through it quickly, I don't really feel capable to go off speaking Spanish in the forums. Maybe a bit of trying would work. Now I'm using [SpanishListening](http://www.spanishlistening.org/) until I saturate its usefulness. I did cheat with Spanish a bit too. I watched 124 episodes of Dragonball, which I understood a lot of :o.



For a few weeks I tried writing 3 sentences a day in one of these languages. That was surprisingly hard! The creative practice of it is definitely good, but it takes a lot of energy compared to doing something easier to streamline. The one sentence selection a day sometimes ends up with half-creating the sentence, for Mandarin. I had them checked on [Lang-8](http://lang-8.com/), which is pretty great (except that you'd have to pay to get more than 2 languages).

The [Language Cafe](http://studenterhuset.com/en/events/category/language-cafe/) at the Studenterhuset in Copenhagen has been helpful. (They haven't gotten it working quite as well as the Coffee Chat every Monday at UMD:CP yet, but they just started =]) This goes a bit over from 10 minutes a day, but the live conversation and listening practice is great.


### 




### Big Mistakes I've Made (And Am Still Making)


The biggest mistake I've made is not having enough variety. I can clearly notice that when I repeat one form of study too much, I start learning how to do that practice as much as I am learning how to do the thing I'm practicing. The particular contents I'm learning stand out less. [Read more about interleaving training](http://www.scientificamerican.com/article/the-interleaving-effect-mixing-it-up-boosts-learning/).

The eternal tug between moving forward and reviewing is exaggerated. In a single day, I have to choose between only reviewing and only learning something new. I generally choose to learn something new, likely a faulty estimation of the opportunity costs. Note, however, that the reviewing features on Duolingo seem poor when you only have 10 minutes a day.

Not enough creative practice.

Not enough interactive / interpersonal practice.


#### 




#### Remedies?


I could try different approaches on different days. Perhaps I could at least choose to do something different, perhaps fun, on the weekends. Easier may be to choose a different mode of practice anew each month, though this is more a remedy to over-acclimating than a way to tie in interleaving.

As for the other three big mistakes, I must accept the cold, hard facts and rid myself of the cognitive bias that I get more if I keep moving forward nonstop. Finishing Duolingo and getting through most of the basic grammar doesn't, it seems, equate to possessing mastery over it. Reading a grammar book is a good step, but it doesn't suffice either.
_Counterintuitively_, taking the time to review could speed up progress. The attention invested in reviewing could even facilitate learning new parts later.

This ties into creative practice. Creatively using what you have learned so far basically satisfies all of the above. It reviews what you know, it lays new foundations and makes your knowledge more thorough, it is a different kind of practice (interleaving), etc. Moreover, is it still creative practice if you repeat it too much?

Likewise, interpersonal practice satisfies many of the above. Moreover, you can gain (intense) emotional associations to ways of using the language, that will enhance your memory ;-).



What comes to mind is: "You get what you practice."

--- --- ---

Sometimes it seems like 30min a day for one language would be a much more effective paradigm. However, the time needed to become satisfactorily proficient in one language times three may not actually prove more effective. Even though it's easier to fit in multiple kinds of practice in 30 minutes.

Also, looking at [Fluent In 3 Months](http://www.fluentin3months.com/), and my own experience all-out-cramming Korean for 2 weeks, I do sometimes feel this approach may be more emotionally satisfactory. Although when you do the numbers:



	
  1. 40 hours per week for 12 weeks: 480 hours

	
  2. 10 minutes a day for 8 years: 486 hours


Wow. So when looked at in terms of days spent, minimalistic study seems ineffective. However, it could take EIGHT WHOLE YEARS to match what he does in 3 months. In that time frame, yes, it'll probably work out fine. In my two weeks of study, I spent more time than I did in 2015 on Spanish (except for that damn Dragonball, hehe :p).

Hmm, looked at like this, it's hard to gauge the progress one has with a minimalistic approach. If I have 2.5 years to get where Mr. 3 Months does in 1 month, what do I even really have to look at? Bi-annual tests XD?

Still, a mix might be 'best'. An all-out week once a year? Devoting some extra time to read a grammar book? Perhaps, perhaps. Or I could go learn a programming language or dialect of math :p


.
.
.

And on the experiment goes :-3




























