---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-01-06 03:55:06+00:00
layout: post
link: https://zariuq.wordpress.com/2014/01/06/emotional-and-intellectual-conflict/
slug: emotional-and-intellectual-conflict
title: Emotional and Intellectual Conflict
wordpress_id: 133
tags:
- blog
- zariuq.wordpress
- conscious
- courage
- dillydally
- emotion
- emotional
- guts
- intellect
- intellectual
- intellectual focus
- intellectual system
- justification
- meditation
- moo
- sociopath
- system
- unconscious
- understanding
- will-power
- Zar
---

I have a deep mental understanding of what is to be done in many areas, but I don't do it. Reading an article about a sociopath from her point of view made this issue much more clear: we have two systems for dealing with decisions: an intellectual one and an emotional one. The distinction here was surely grasped at by Freud and his id, ego and super-ego, and surely by many others as well.

The emotional system is mostly 'unconscious', whatever that means. It helps us quickly react to others and many situations; these situations often aren't even worth intellectual focus. The intellectual one is mostly 'conscious' and deals with less regular or more complex situations. In humans, the emotional system typically takes precedence if one does not exert strong will-power.

So, even though I've gained some very good intellectual understandings of what I gotta do, my emotional system is tuned up against it. In some cases it just doesn't support what I gotta do and does support other things that use up time instead, which partially explains bursts of will-powered energy and their degeneration back into 'the norm'. There's an emotional build-up when I don't do what I think I gotta do for long enough, but this quickly burns up once I start doing it for a bit. Heh. Are we not made by evolution for such long-term habits other than the ones we gained while growing up? And when my emotional system actually tells me to avoid something, my energy will burn out even more quickly. Egads!

Now we can throw in our ability to make logical justifications based on our emotions, and our intellectual certainty will slowly chip away as we do nothing. We'll fizzle down.

So, the next question is how we synch the emotional system with the intellectual one. I haven't really succeeded personally, so this is a higher level of speculation. Making me, oh gosh, a talker rather than a doer. Well, I knew that. I'm a philosopher. Means we talk a lot, though we can do too.



	
  * One way is through the environment. Environments and people around you can greatly influence your emotions in subtle ways. They can keep encouraging you to do some things you enjoy or motivate you not to do something else.

	
  * Another way is through sheer will-power, to have enduring courage and determination to push through the emotional resistance as you do what you gotta do.

	
  * Meditation may cool down the emotional system and lesson its impact – well, and that of the intellectual one. The intellectual system can go haywire too. It can go wild with speculations and unending tangents. I'm prone to this kind of mental knotting too.

	
  * Some people just feel far less from their emotional system, such as sociopaths. Maybe some are the opposite of this. One will then be free of a lot of emotional-intellect conflicts, but one may then miss out on numerous unconscious messages we communicate to each other on the emotional level. So far, we have less choice here though.


Heh, writing this out, I think I have an even clearer idea of what to do. Alas, will I do it . . .?

So, what methods have you used? What ideas do you have for this?
