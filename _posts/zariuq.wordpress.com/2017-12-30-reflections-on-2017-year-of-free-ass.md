---
navigation: True
subclass: 'post tag-test tag-content'
cover: 'assets/images/cover3.jpg'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2017-12-30 20:36:22+00:00
layout: post
link: https://zariuq.wordpress.com/2017/12/30/reflections-on-2017-year-of-free-ass/
slug: reflections-on-2017-year-of-free-ass
title: 'Reflections on 2017: Year of Free Ass'
wordpress_id: 1401
categories:
- New Year's
tags:
- blog
- zariuq.wordpress
- Buddhism
- Free Ass
- Intuition
- life
- New Year's Resolution
- Personal Development
- philosophy
- Rationality
---

I had a kinda cool start to a novel and then plunged it into nonsense and, err, didn't finish it.

Did I watch more than 52 porn sessions? Might be pretty close, actually. But probably over. I noticed that I got better at some visualizations while not looking at porn, which had an effect of more vivid sexual dreams. I also note that with porn I tend to be quicker in the "wham bam thank you ma'am" style than otherwise. Hard to identify actual personal changes, however the sort of focus given to my mind seems different (on some sort of personal ideal or on some specific girl). Tempting to take the quick cum it is tho :p.

As to freeing my ass (not avoiding action in fear) and living my philosophy, I have ended up in quite interesting places :).

Where to start? What to say? As noted before, small steps are the heart of lasting change. (** However big change throwing many small steps at one does seem plausible too.) Even this year, I generally progressed with small steps. Small "hi"'s or asking-out-on-dates. Encouraging myself to say X whenever/however/wherever rather than caring about doing it in a good way. For example, even if I'd rather express interest in a girl in person, doing it in person is good enough compared to waiting for the next chance in person and finding the right opportunity then, which then makes that meeting more stressful/confusing, and spirals me into oblivion! Somehow ended up with more experience than I'd really gotten in my life before this year -- and yet not that much experience :0!

Another usual experience: finding it difficult to tell if one is shying away in fear or legitimately picky. Yet one doesn't want to cop-out and accept everything either. An interesting experience there is this admittance of being in fear and not acting in fear. Somehow knowing you will act later, eventually.

There's the problem of radical honesty: not always being completely honest is sometimes effective (especially in the short term). Moreover, there are numerous ways to be honest, to express oneself honestly. Shame can lead one to express oneself honestly in a way that is somehow less effective, that misses some crucial oomph and assertiveness. On this topic, I've settled for "communication trumps honesty". Focus on what one wants to communicate, not on how true it is. And duh, if you communicate something others won't view as true, well, be aware of how it won't correspond to some fact they can observe (and if this observation is in your control) :D. ("Lie well, my dears, m'kay?").

There's the devil of hindsight: noticing opportunities one had but somehow avoided taking.

Another nice feature of gaining more experience is, say, the glow one can see around girls in kissy mode. Is it just how I experience noticing their pupils dilating? Perhaps.



I've actually ended up focusing on a sort of Free Ass Mode as State of Consciousness that doesn't quite correspond with what I set out to become. It's close, and in some ways more general :).

Part of what leads me here is a sort of positive-intention personal dystopic vision of "living one's philosophy". Of course, living one's philosophy, one will take action regardless of fear. By definition (of my not-fully-defined personal philosophy). In some ways the world I inhabit (referencing that "my mind is my prison" meme) has been quite close to this already.

I don't like "shoulds" for this reason too. Firsts because using should generally shoves the metric and reason under the rug and leaves one only with a command/rule. A "life of shoulds" is then a life lived like a puppet, training oneself to take right action as one should. Uninvestigated, one dances to the strings of society, being a good boy.

So then what does living one's philosophy look like in this perspective? One becomes one's own puppeteer! One looks under the rug of should and takes the lead. One (rationally?) investigates life and decides how one wants to act. Then one takes said actions. Rinse and repeat. This is life. This is the wet dream of self-discipline. One does exactly what one wants to. Perfect.

-- Except, err, look at the emotionality here.

Does this feel "free" as in freedom, as in free ass. As in a free donkey dwarf?

Nope.

The feeling is one of control, of bondage.

One is then fully bound.

When torn between one's rational analysis and one's 'fear' one is more free, as one hasn't fully given way to rationality yet. This dystopia is a control-freak's attempt at freedom without actually letting go. It looks like freedom, yet subtly isn't.



So then, what does acting freely look like?
Does one impulsively do what one feels in the moment? Yes, more like that.
How does one corroborate fear and a desire to act? [Err, that one is simple: act in fear!]

Ok, cool. But can one guarantee one's impulse in the moment will actually be to act in fear rather than feel the fear and desire and not act? To guarantee that is again to enpuppet oneself.
Can one live one's philosophy without enpuppeting?

In the framework I set out in, of mustering courage to act according to one's strings, perhaps not.

This is how I ended up somewhere not quite expected :).



In the past I mareveled at how I was a ball of contradictions. This year I've been moving towards coherence/unification of self (parts) instead.

One exists and acts at many different levels of detail and timescales. One lives in the moment -- yet one lives on the timescale of minutes to hours when navigating anywhere. Having a child with awareness is living on the timescale of years. Going to college is too. Investing in your Roth IRA a timescale of decades. One can take an action to benefit only oneself -- or an action that will potentially affect millions. Giving a man a fish has a quite local effect (unless by keeping them alive, you then influence all they influence throughout life). Teaching a man to fish and teach others to fish influences the rest of his life and perhaps the lives of many others.

In each moment one is aware of one's designs for multiple timescales and multiple levels of abstraction.

This brings me to the question of coherence of one's desires in multiple timescales. One can also say multiple levels of abstraction.

Being one's own puppeteer is a polarization where one prioritizes a certain level of abstraction (that of beliefs/plans -- a domain of rationality) over others.

Impulsive in-the-momentness is a polarization where one prioritezes a certain levle of abstraction (that of immediate desires/drives -- a domain of intuition) over others.

What if one wants coherence of philosophy and intuition?

This is tricky, and I think priority be given to intuition -- most of the time. Tricky as guarantees imply self-imprisonment.

To let go of the reigns, self-trust seems paramount. Trust that, even if not perfectly or immediately as if guaranteed, I will freely align with my philosophy in action. Trust that I will align hard enough so as not to be stuck behind any wall that needs serious forced effort to break through (as I was prior to this year).

This self-trust and being-in-momentness, diving into flowy focus and passion, point to acceptance that sometimes philosophizing about my own intentions/desires/designs for life is a high-priority desired activity :). Sure, I felt I do it too much and like taking action more..., but I do desire it nonetheless.

This style in some ways matches how I cooked. For the most part, I didn't taste while cooking. Just at the end. I nonetheless honed my sense for how long to cook things, in what ways, etc. Lots of observation in the act and intuition. Some thoughts about things to try next time, even while mostly freeballing it. When I got new ingredients, I did look up some recipes. I read them to get the basic idea (so as to see how it's generally cooked in an edible and yummy way), and then I went and did something like it.

Likewise, living one's philosophy can become an educated, thoughtful guidance rather than strict puppeteering. Intuition is aligned with and calibrated with philosophy as one acts, not necessarily in perfect alignment at any moment, yet mostly there :).



And that's more-or-less the answer to 2017.

What about 2018's New Year's Resolution?

I won't make one. Not that I need take my resolute word seriously as a Jew. Bahahaha :D >:P ~ ~ ~

Presently a New Year's Intention sounds better :). However, I want clear intentions to be a part of my daily mode of existence ;). Re-focusing or finding clarity to intentions, well, as needed :).

My New Year's Resolutions have been awesome :D. And with love I pack them away :) <3
