---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2017-12-02 17:20:27+00:00
layout: post
link: https://zariuq.wordpress.com/2017/12/02/garden-of-minds/
slug: garden-of-minds
title: Garden of Minds
wordpress_id: 1399
tags:
- blog
- zariuq.wordpress
- abolitionism
- Animal rights
- Compassion
- life
- mind
- mindplex
- pan-species
- politics
- society
- suffering
- utopia
---

I've often discussed pan-species society.

I want to uplift other animals. However, I don't want to force this on them. I want to include other animals into society as much as possible. I think we can to a fair degree gauge animal consent and, perhaps painstakingly, educate them on possibilities. VR may help with this -- though the use of that's ethical status and respect for voluntarism is debatable. Use of AI systems to both better understand animals and better communicate will likely be fruitful too: http://www.csc.kth.se/cvap/EquestrianML/.

One motivation for thinking about a pan-species society is thinking about post-human society. Once we can uplift ourselves, create fully-functional and conscious mind, etc, we can potentially have a society with greater mental functional diversity than we do now. Frankly, we could have a society with more functional diversity than a current pan-species society would [_insert algorithmic information theory calculations_]. We don't necessarily have to, though. Moreover, various forms of mindplexes or higher level brains (like the global brain) also complicate matters.

I do want to do what I can to bias our future towards inclusivity, though I'm not sure how many minds will choose to stay, err, 'stupid'.

What would such a pan-species society look like?

It's not that easy, right?

Population is exponential. We can't just let deer and dog fuck like rabbits and fill the Earth until even advanced tech resources are strained (never-mind difficulties in ecosystem management). Yet killing them when they fuck too much is abhorable and antithetical to [Compassion](https://zariuq.wordpress.com/2016/10/14/mythological-memeplectic-morality/). Abortion is also far from side-effect free. Advanced birth control, I guess. Currently our birth control technology for humans even is in a premature stage. I wonder what psychological effects this will have on them.

And if you think this sounds hard, wait until you hear about the [Wild Animal Suffering](https://en.wikipedia.org/wiki/Wild_animal_suffering) problem and the [Abolitionist Project](https://www.abolitionist.com/) to _end all pain_, including [fixing predator species](https://io9.gizmodo.com/the-radical-plan-to-eliminate-earths-predatory-species-1613342963). Yet these are only simple conclusions of **valuing Compassion**.

Society with interdependent entities will involve restraint, whether this comes from within the individual or without. Without carefully engineering the members of the society, a la Pearce's predator fixing, external restraint must be present.

I like viewing such a society as a **Garden of Minds**.

This emphasizes the subtle interplay of fostering creative mind growth and control for over-all growth and beauty. from colocation of minds to allowing minds to go crazy without monopolizing.




