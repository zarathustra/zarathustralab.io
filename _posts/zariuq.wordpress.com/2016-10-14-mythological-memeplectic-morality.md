---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-10-14 13:58:55+00:00
layout: post
link: https://zariuq.wordpress.com/2016/10/14/mythological-memeplectic-morality/
slug: mythological-memeplectic-morality
title: Mythological Memeplectic Morality
wordpress_id: 1003
tags:
- blog
- zariuq.wordpress
- Compassion
- Creativity
- Ethic
- Greed
- Love
- Meme
- Memeplex
- Moral
- Mythos
- philosophy
- value
- Zar
---

First, I have come to realize that I was wrong about [Dictator Bob's Valuism](https://zariuq.wordpress.com/2016/02/06/442/). I still hold that it provides a clear framework (obviously consequentialist and utilitarian):


<blockquote>E[satisfaction(values vs, world state ws) | policy p]</blockquote>


However I was likely wrong to call it moral relativism: the whole process from an understanding of the world and one's values (preferences) to an ethical policy is fundamentally amoral.

What is amoral ethics or morality then?
The recognition of a need for standards for behavior on personal and group levels.
The desire to isolate whim and intuition to the degree possible (in value determination).

Standard understanding of morality can not just be written off as poor philosophy though. Morality is not merely passive prescription, but an enforcing arm of a societal puppeteer (as Baudrillard duly points out in [Simulacra and Simulation](https://en.wikipedia.org/wiki/Simulacra_and_Simulation)).

Social pressure directs more of human choice than we want to recognize. We will choose not to pursue our own desires at the fear of censure (even if we _rationally_ know this unlikely, or entirely due to archaic standards we don't even hold ourselves). Every time a moral tenet is proclaimed, the pressure to obey increases.

Expounding moral theory corroborating your values can form a bulwark of one's policy realizing one's values in reality. As suggested in [Model Zero](http://www.heljakka.com/model-zero/) we're advised to evaluate models on more than _factual accuracy alone_, moral models seem chosen more for forcing power than for truthful accuracy.

Once expounded and birthed, can one just stop? Alas,

![memeplex](https://zariuq.files.wordpress.com/2016/10/memeplex.jpg)

Enmeshed in moral philosophy there live powerful memeplexes. We live in fear of challenging these mighty beasts; and, in fact, we will be punished for crossing these memetic warlords, for they are writ into law. These memeplexes of morality are even ostensibly _good_ for us (and some arguably good for the propagation of a society of intelligent beings in general); they also infiltrate many a mind at such a depth that their inadequacy is inconceivable.

Memeplexes of morality are mythical entities existing not as inherent truths despite popular belief, yet exist on a meta-plane possessing immense power.

They bear more relation to the [Global Brain](https://en.wikipedia.org/wiki/Global_brain) than to traditional _living_ entities.

One of these mythical beasts is Compassion, which I have [struggled against previously](https://zariuq.wordpress.com/2015/12/24/too-long-ramblings-on-compassion-vs-power-ethics/). As the [Examachine](https://twitter.com/examachine) often points out, cooperation is a strong strategy. [Tit for tat](https://en.wikipedia.org/wiki/Tit_for_tat) proves a successful, albeit imperfect, strategy. Cooperation is related to Compassion. [Golden Rule ](https://en.wikipedia.org/wiki/Golden_Rule)dynamics also propel Compassion onward to the future.

If you dislike Compassion, rest assured, you can still avoid her. I wonder for how long as we approach the panopticon society though?
To what degree will Compassion expand her influences beyond the apex of intelligent life? (After humans cease to be this apex?)

What other mythical beasts do we live amongst?
Love -- distinct from Compassion.
Creativity, which I may cast as a combination of Growth and Choice (two of [Ben's proposed Cosmit Values](http://ieet.org/index.php/IEET/print/3838)).
Greed?  -- Natural gravity-esque accumulation.


What kind of moral theory and insights can be reached from the Mythological Memeplex perspective?

Haha, I don't find Moral Memeplex Myths that easily, nor do they necessarily have clear bodily membranes. Any suggestions as to others or revisions of these?






