---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-04-30 15:42:13+00:00
layout: post
link: https://zariuq.wordpress.com/2016/04/30/radical-honesty-or-acting-like-luffy/
slug: radical-honesty-or-acting-like-luffy
title: Radical Honesty (Or Acting Like Luffy)
wordpress_id: 676
tags:
- blog
- zariuq.wordpress
- honesty
- Luffy
- One Piece
- Radical Honesty
- Zar
- 正直一途
---

So, it's been a while since I posted on here. Is that a sign that I haven't been thinking? That I haven't thought anything 'worthy' of being publicized? (To my wide-readership of < 5.) Or just that I'm doing that thing where I revert to not putting myself out there for whatever reason. Again and again.

Well, I've always liked the idea of just being honest and frank. There's the Japanese term 「正直一途」that I also took quite a liking to at some point. For the most part, there does not seem to be much need to lie.

Of course, this does not include lying for **fun** or in cases that are very **serious**, which could result in losing thousands of dollars, a life, being jailed, etc. In these cases I wholeheartedly support lying without any hesitation. And coming up with a good, believable lie.

(Haha, is this why politicians lie all the time? When you're in that position, even small lies could save so much money. Every little lie becomes serious.)

The need to lie basically signifies some issue with the world. How often is lying the solution to that issue?  More often lying may be shoving the issue under the proverbial rug.

Furthermore, being honest is in some ways a more down-to-earth, empirical approach. You lay your cards (_personal facts_) on the table and figure out the best answer. Better yet, others can help.

Sometimes something one feels (like expressing) may end up completely withheld, never expressed. But if you don't even try, how can you possibly succeed? (Getting very, very lucky.) So there's a strong case for at least finding a way to express oneself.

Yet another main point is that what one fears may happen when one expresses oneself often doesn't. Thus one often needlessly complicates, uses mental energy, and avoids potentially good outcomes out of unfounded fear.

But aren't there some problems with the world better left unspoken? Like the fact that the person you're talking to is incredibly ugly? Or you see someone across the street with the ugliest shirt imaginable and feel a strong urge to run over and tell them?

Well, if you _really_ feel like saying that, perhaps it's best you try and see how it turns out. Perhaps you'll like the outcome. Perhaps your experience of the outcome will lead to not feeling like doing that so much in the future.

It's not clear this is any different from the fears that may not quite pan out.

Moreover, I saw the quote, "[if you're going to be candid, be beautifully ](http://drprem.com/love/truth-practice-radical-honesty-life-love-truth)[candid.](http://drprem.com/love/truth-practice-radical-honesty-life-love-truth)"
I think it has a very good point. There is some core information you want to express. Most likely, you don't want to express an exact string of words. Thus there will be more and less beautiful ways to express what you want.

I did read the book [Radical Honesty](https://www.goodreads.com/review/show/1616768484). As you can see, the book is full of new agey bull shit, but has its redeeming qualities. The author does stress psychological benefits of radical honesty. The author also stresses expressing resentments (e.g., "I resent you for fucking my bunny").

Does this only apply to words? No, I think it could be called Radical Expression or Honest Expression instead. If you hear music and feel like moving to the groove, not doing so is somewhat dishonest.

However, say what I do of the benefits, I suspect 80-95% honest is preferable. Just from a practical perspective. Pretty similar to my suspicion that only having 5-20% privacy is preferable (more at the start of the transition to a transparent society). Transparency is, in theory, just a more effective paradigm (and more encouraging of acceptance too). Privacy is just good for personal manipulations. (Yet some of these may just be needed, at least for self-protection before we come to acceptance.)

Nonetheless, I think a period of radical honesty may be the route to reach the 80-95% route. Because if one does anything else, there seems a strong chance one will refrain from being honest in some cases that do matter (thinking they matter more than they really do). Ideally, when not **fatal**, one may try honesty first and then learn based on the results. In an evidence-based manner :-D.

I guess my main motivations for this would be the few big areas where I think not expressing myself to the extent I currently do may be a liability: freely dancing when around people (I know), social arrangements, expressing sexuality / skinshipality, romance, expressing annoyance, expressing that I think someone is full of shit, and maybe some other things.

I am generally pretty honest (despite liking to lie) already. >_<
Though less so when one seriously considers withholding as _lying_.

Another way of thinking about radical honesty is to find people or characters one likes who are basically radically honest. I do know some people, and then there are characters like Luffy. They have other positive characteristics too ~

Although figuring out how those characteristics would manifest in oneself is not that easy :p

I think my going model will be to ease into Luffy-Like Modus Operandi and Radical Honesty (as I know I can't expect anything more than easing into it).


