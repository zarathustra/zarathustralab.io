---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-10-14 13:32:23+00:00
layout: post
link: https://zariuq.wordpress.com/2014/10/14/the-transhuman-continuity-of-self-problem-step-1-base-case/
slug: the-transhuman-continuity-of-self-problem-step-1-base-case
title: The Transhuman Continuity of Self Problem (Step 1 - Base Case)
wordpress_id: 332
---

What makes you the same person today and tomorrow? What makes you the same person you will be in 5 years? Or 5 years ago? Let alone when you were a tot! Why do you continually feel like yourself?

Furthermore, will you keep feeling like yourself as you transcend into transhumanity? Or are you likely to run into some insurmountable fork where you have to choose losing yourself or living forever. If you no longer feel yourself, in what sense can you claim to be the continuation of your old self? Rather than, say, a new being influenced by your old self. In this case, doesn't your old self die rather than living on in some new form.

I'll argue that we can probably find a way to live on into some new transhuman form, rather than just giving birth to H+ beings. Of course, we can't positively or certainly answer this question. Starting with my take on the normal continuity problem will help. What is that persistent 'self' you have now and 5 years from now? And we can't all agree on an answer to this yet either. The best response may be analogous to Diogenes the Cynic's response to Zeno's paradox: keep on living as you are. If your theory leads you to doubt the obvious, perhaps your theory needs to be redone. :-D

**The Normal Continuity Problem**

First, I'll be taking a relatively materialist position. If 'you' reside in some 'soul', we can still have an interesting discussion about what this means as you change yourself into a transhuman, but that's not the focus here. Borgs, group minds, mindplexes, extreme mind-sharing, and any other likely scenario that involves a complete revolution of the sense of self will also be put off for later. While there are many interesting intricacies (beyond my understanding), part of the point there _is_ going beyond the self. How do you preserve the self while going beyond the self -- it's almost a koan ;)

The first places to look are our obviously persistent physical bodies, habits and idiosyncrasies. These remain relatively stable and either one typically allows one to be identified without the others. Low and behold, the brain seems to be the most relevant to who we are. Lose an arm and you're still 'you'. Have a frontal lobotomy and some of your personality may go. However, personality can occasionally change after heart transplants, so there may be many subtle physiological effects going into who we are.

These are all important to our selves, but they do change over time. Is there some more essential manifestation of the self? Not necessarily. Buddhists have claimed that 'self is an illusion' for millenia. Doesn't mean that we don't have habits, quirks and characteristics, but that none of them are concrete or essential in and of themselves. Fortunately, this idea has been refined as in _Being No One (_by Thomas Metzinger). I can't claim to do it justice as I only read a few chapters of the book :p.

The idea, _as I understand it_, is that what I experience as myself is actually a self-model. We are a collection of cells (and (mental) processes) with a common purpose (some guy in The _Forever War_). If there's a common purpose, there ought to be a common representation of the executive unit. So we need a self-model. It happens that there are many efficiency and evolutionary restraints on this self-model (and some mental disorders can be viewed as these restraints being broken). For example, the self-model should be immersive and unaware of its modelness (probably good for faster reaction times).  Aaand, I don't remember enough details to say more, nor do I need to.

If there's nothing essential or absolute, and we're a bunch of processes/habits with a common purpose and self-model, why do we feel and seem the same over the passage of time? Because it's more efficient to! More evolutionarily robust! Because we have habitual ways of modeling ourselves! All true. And I'm not the same in 5 years, just close enough (and continuous, though perhaps I could fill in the blanks...). To what extent I was the same as a 3 year old is even harder (I only have a handful of memories from that age). Is there some reason changes are slow or at least internally regulated? I think there may be.

We have mental habits (processes/w.e. you want to call them), and surely meta-habits (habits about habits), and maybe even meta-meta-habits (how far does it go?). Surely these habits may form loops or be recursive too. I'd hypothesize that higher level (more meta) habits are less likely to change than lower ones. And the dynamic system is largely self-supporting. Small habits and idiosyncrasies can change here and there, and you'll still seem the same 5 years later. If some higher level habit changes, you'll be the same except that '_something feels different about you.' _

How we process information is another key to why we don't change much. I hypothesize we largely take in information in terms of what we already have. By the time we adopt some new habit, it's already been molded and cropped so as to fit into the general pattern of us. Heck, if the new action or information doesn't fit in to the rest, we'll experience unpleasant cognitive dissonance until we find a way to hack it in :D. That's a pretty strong process forcing us to maintain coherence of self.

Well, this was far longer than expected and all over the place. But the characterization of the self as a model integrated with a dynamic system of habits and meta-habits suffices to explain why we feel like the same continuous self (efficiency ftw) and why we don't change so quickly (slower and slower the deeper you go). This viewpoint also makes it easier to explore how our feeling of self and characteristic dynamics/habits can robustly and continuously change in the face of transhuman advances.

**The Transhuman Continuity Problem**

So, in what sense can we remain the same while transcending humanity? Can it be a journey of true radical self-improvement, in the literal sense? Or do we stop being ourselves somewhere along the way. If I compare H+ Zar in 35-75 years to the current Zar, the differences may be so extreme that one'd think there must just be a gap somewhere. I'd like to think not necessarily...

Let's start with mind-uploading (or equivalently transporters/teleporters). Mind uploading has lots of promise, but for now assume you're uploaded into an Earth-like VR world. The tech works perfectly. You wake up in your VR world (or the Enterprise), and you feel just as you did before being uploaded. The room is the same to smooth the transition. One small problem... you're a clone. The teleporter disintegrates the original, and you can choose what to do with the original when uploading. I'm fine with being a copy. Once you are the copy, you'll see no reason to care -- unless the original comes after you.

What if you didn't want to be a copy? What if you really wanted to be continuously uploaded? Perhaps the upload's mind could be linked with your external mind so that you can experience being in both worlds at will? Another approach, which I saw on a [blog post](http://theness.com/neurologicablog/index.php/the-continuity-problem/) that covers the issue well, is to create a digital extension of the mind that gradually does everything the meat-mind does. In a sense it's a copy, but in a sense you're gradually becoming digital instead. Uploading a copy and being happy with that may be easier and works, but you can still find workarounds if you try.

Now once you are uploaded, there are so many avenues for modification and improvement. I'll postulate that we can reach advanced H+ states through a (long) series of improvements, eventually altering (nearly) every aspect of ourselves, and then making those better again. In reality, AGI may outpace our improvement (egads, I hope it does). And this AGI may deign to jumpstart us up to speed very.. rapidly. I'll argue that if we can remain ourselves throughout the gradual, step-by-step journey, then we (err, or the AI) can find a way to more quickly and directly transhumanize us that also retains ourselfhood. And, of course, that if we are lucid for each individual improvement, then we will be for the whole journey to H+. This is where the continuity comes in. (Of course, never forgetting that what it means to 'remain oneself' is rather vague and we're always changing nonetheless.)

We'll do this by considering some basic first improvements or upgrades to the mind on the path to transhumanity. And the rest follows by induction (the everyday case is the base case). There's no way I could outline them all, but if you think you know which one would break the continuity, please throw it my way     \(^_^)/.

... And this took up too much space. I'll do the next, the interesting part in the next post.

(p.s., I feel someone can say what I did in one short paragraph, but I'm not good at being concise. At least writing this up helps get it off my mind =])
