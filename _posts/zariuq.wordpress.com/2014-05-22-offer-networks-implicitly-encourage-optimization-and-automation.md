---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-05-22 10:05:07+00:00
layout: post
link: https://zariuq.wordpress.com/2014/05/22/offer-networks-implicitly-encourage-optimization-and-automation/
slug: offer-networks-implicitly-encourage-optimization-and-automation
title: Offer Networks Implicitly Encourage Optimization and Automation
wordpress_id: 260
tags:
- blog
- zariuq.wordpress
- automation
- barter
- capitalism
- donkey kong
- money alternative
- Offer network
- optimization
---

Ack I don't update much. Anyway, I was thinking of automation lately. That and Offer Networks.

Offer Networks are a sort of alternative to money. They are a form of barter only possible in a highly connected and digitized world, such as the one we're entering. People register what services/goods they can offer and what services/goods they request. The hard technical part here is the back-end production system that matches up offers and requests on a global scale. Offers can have conditionals and get complex if desired, so we should be able to make exchanges that are difficult with money. They seem a good alternative to money once we have the technical wherewithal. (You can read more about them [here](http://hplusmagazine.com/2014/03/26/beyond-money/).)

 

One perk of capitalism is how it encourages efficiency: one always wants to do things more efficiently so that they can profit more! Some may misguidedly complain about the loss of jobs due to automation or globalization, but in some senses these are good. Ultimately, we want to automate pretty much anything we can, even if there be short term consequences. We may also want proceeds to go to those who need them the most, those who will work for the least. Paying too low is a shame, but we'd still rather the money go to them than those who don't need it as much.

The question we arrive at is how well do potential economic alternatives encourage automation. Do Offer Networks have this implicit drive for optimization built in? 

At the core, we want the system to have a drive for efficiency or optimization built in. One strong enough to overcome our resistance to change and the implementation curve. Once we realize this general principle at work, it's obvious that Offer Networks will support automation etc. at least as well (and hopefully with less exploitation). For efficiency and optimization will be built into the production system: if one offer can satisfy 8 requests, it'd be bad design to pair each of the 8 requesters with a separate offerer. You can extrapolate from there to a general principle for efficiency/optimization built in. Yay :D
