---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2013-08-31 02:10:43+00:00
layout: post
link: https://zariuq.wordpress.com/2013/08/31/putting-myself-out-there/
slug: putting-myself-out-there
title: Putting Myself Out There
wordpress_id: 37
tags:
- blog
- zariuq.wordpress
- blog
- diary
- japan
- life in japan
- public
- value
---

Am I putting myself out there if I don't actively try to share and publicize my blog?

Technically, I am.  If a post actually contains information someone finds valuable, they can find it.  Or I could show it to them.  Either way, it's more out there than private notebooks, diaries, or just telling one friend.

There is an easy approach: write about what you do in Japan and take pictures.  People are always curious about "life in Japan".  Oddly enough, these internal musings, this part of my life, interests me more.

And here Wordpress has the answer to my question: "If the writing is honest it cannot be separated from the man who wrote it." (Tennessee Williams_).  _

Amazing.  As if the universe is really guiding me around giving me what I seek.  Carp cakes! Are the yogis right?  And I'm elsewhere on the internets, so this ought to be too.  Okay, it will. When the time is right.  Mr. Cosmos will tell me when >:D.  (That's God in some other languages, but that term doesn't emphasize how like a cheesy superhero he is.)
