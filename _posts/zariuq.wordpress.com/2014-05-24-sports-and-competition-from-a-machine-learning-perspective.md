---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-05-24 06:25:40+00:00
layout: post
link: https://zariuq.wordpress.com/2014/05/24/sports-and-competition-from-a-machine-learning-perspective/
slug: sports-and-competition-from-a-machine-learning-perspective
title: Sports and Competition from a Machine Learning Perspective
wordpress_id: 288
tags:
- blog
- zariuq.wordpress
- competition
- expert
- Go
- machine learning
- ML
- pointless
- sports
---

I recently took Coursera's Machine Learning course. I find looking at various aspects of life from a ML perspective can offer nice insights.

One area that comes up a lot at my job is sports. As are many ML problems, sports are very narrow. You have your precise rules and goals, and a very large part of play is how well you can perform the basic movements involved. So you spend hours upon hours practicing your shoot in basketball or soccer, practice dribbling, hitting the ball where you want with just the right spin in tennis, etc. There's some cross-over, but being an expert in one sport doesn't make you one in another. This seems a lot like training a neural net to do some specialized task (_err, maybe it actually is_ :p).

This brings us to step two: what does this say about competitions?

Generally speaking, if you have enough examples and parameters, you'll be able to perform near perfectly well. More examples won't help much. Sports for humans are the sort of problem that can be gotten close to perfection, but not quite solved (like tic-tac-toe can). This is similar to the "10,000 hours to be an expert" idea. Practice enough and you'll pretty much get it.

Thus, to simplify things for example, we are competing to see who can best fit a curve (where, say, you get a new point every time you press a button). Jim Smith has an accuracy of 99.999997, but, ohhh, he lost to Sally Ferguson who had an outstanding 99.999999! (Strategy, teamwork, and high bias may prevent humans from getting this good though.)

This perspective adds some substance to my distaste for competition that involves (hundreds of) hours of practice to even be able to compete. (Which may be a bit odd as I've spent so much time learning to play Go :x)

 

What's more interesting, then?

  * How many examples do you need to get a good model? This makes Fluent in 3 Months such a nice idea.
  * Do like in Go and make sure you play someone with as much data as you. That way you can enjoy a good game without going wild ;)
  * What interesting games haven't been solved yet? If someone's spent the '10k' hours to be an expert, let's consider the game solved. Let's move on to one that hasn't been solved yet ;)

Oh yeah, playing just for fun works too xD.
