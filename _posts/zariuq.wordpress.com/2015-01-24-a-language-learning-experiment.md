---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-01-24 08:49:45+00:00
layout: post
link: https://zariuq.wordpress.com/2015/01/24/a-language-learning-experiment/
slug: a-language-learning-experiment
title: a language learning experiment
wordpress_id: 362
tags:
- blog
- zariuq.wordpress
- arabic
- bumpkin
- chinese
- chineseskill
- duolingo
- howtostudykorean
- kdrama
- korean
- language
- learning
- learning experiment
- Lord Crunchkin
- memrise
- spanish
---

i'm doing a language learning experiment: how well can i do on about 10 minutes per study a day? consistent practice seems more important than massive studying in brief spurts (at least for exams :p), so a small amount of consistent practice should work.

if this experiment works well, then i may've spent lots of energy on overstudying with rapidly diminishing marginal returns! impatience could be worth it anyway though... xD

i'm currently going for spanish, chinese, korean, and arabic. i started on 12 december 2014.

i'm using:

spanish: duolingo

chinese: chineseskill (an app)

arabic: http://www.madinaharabic.com/Arabic_Language_Course/Lessons/

korean: http://www.howtostudykorean.com/ + memrise

so far, duolingo is pretty awesome. it teaches me grammar through example and gives me pretty close to as varied practice as i can get in such a simple way. i've got to remember to 'review'/'strengthen my skills' and not just rush ahead though...

chineseskill is pretty good. it's basically similar to duolingo, but with a few fewer features. it works pretty nicely too. the main difference is the lack of a general review option.

at first i tried finding some simple method that contains everything for arabic and korean too, but the memrise courses with grammar just didn't cut it. they're a bit all over the place. they may be good for review once you know the stuff. may. that and learning to read arabic is/was pretty slow. i can mostly read it now though.

now i'm reading a section (of a lesson) on howtostudykorean a day in addition to two quick memrise sessions. my vocab acquisition lags behind the lessons, but progress seems better.

ahh, i do know a fair amount of korean intuitively from kdramas. and in 2013 i studied korean for 2 weeks. i wanted to see how much i could learn in 2 weeks. i got basic grammar and about 400 words down before going to korea for 21 days. being able to read and write korean was very useful and pleasant when there :-). alas, i feel my spanish/chinese has already surpassed my korean (just not on the intuitive level).

let's see how this goes! :-D
