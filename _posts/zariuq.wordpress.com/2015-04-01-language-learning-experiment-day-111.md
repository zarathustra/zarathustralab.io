---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-04-01 08:13:30+00:00
layout: post
link: https://zariuq.wordpress.com/2015/04/01/language-learning-experiment-day-111/
slug: language-learning-experiment-day-111
title: Language Learning Experiment (Day 111)
wordpress_id: 368
---

So the experiment continues.

I noticed significantly more progress in some than others. In decreasing order: Spanish, Korean, Chinese, Arabic.

Duolingo is better than ChineseSkill (sorry!), and more well-rounded than using a textbook-like site + flashcards (Memrise). Especially if short on time.

Having watched about 10 Kdramas (and 200 Korean dubbed One Piece episodes) definitely helps a lot. When I learn words, I often feel I already know them.

And of course having to learn a new alphabet with Arabic is slow. In addition, I found no good, brief method of study. So I ditched Arabic for Danish on March 21 ^_^ ;_;

While Duolingo is good, I find very many days get eaten up "strengthening skills" rather than progressing. I guess this is good, but progress becomes slow. I can't say I don't need the practice/review though. I guess this is to be expected on 10min/day.

Instead of spending too much time on review and making sure I've got the Chinese characters visually memorized (though recognition is no problem :p), I've decided to learn the English & Chinese names of the radicals as a part of the daily ritual. It seems like it'll be helpful.

Lastly, I feel that I ought to devote more time to creative practice. Otherwise I will fall for the classic error of learning only relative to my methods of study and practice, and be unable to rend up words on the spot...

Hmm...
