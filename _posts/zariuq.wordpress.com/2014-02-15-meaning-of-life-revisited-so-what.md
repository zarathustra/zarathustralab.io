---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-02-15 03:34:52+00:00
layout: post
link: https://zariuq.wordpress.com/2014/02/15/meaning-of-life-revisited-so-what/
slug: meaning-of-life-revisited-so-what
title: 'Meaning of Life Revisited: so what?'
wordpress_id: 199
tags:
- blog
- zariuq.wordpress
- life
- logic trap
- meaning
- Meaning of life
- oops
- so what
- values
---

Oops. A discussion about how logic doesn't get you anything by itself (beyond tautologies) has led me to some more realizations. Realizations about the interplay between values and justified-views.

This realization fills in the words I didn't have at the end of my construction of a [meaning of life](http://singularityhub.com/2012/03/22/3d-printer-cranks-out-exquisite-structures-smaller-than-dust-mites-and-sets-a-new-world-record/). I identified a broad progression life is a central part of. This is indeed a meaning of life, but I didn't know how to connect it to action. I didn't know how to verbalize why we should ally ourselves with this meaning of life, even though it will guide us so well if we do choose to. I had fallen for the logic trap myself.

My host of values implicitly approved of supporting this progression.

What if my values were more pro-entropy and the heat death? Maybe life will increase entropy faster and I'd still support it. What if my values were more non-conformist? I'd be repulsed at the idea that we should conform to this grand universal norm and fight it by putting an end to humanity (or would ignoring it work?). What if my values were more conservative? I may think we have no need for progressing further. We should instead find a good, fairly steady state to live out our time.

Heh. My _meaning of life_ is a thrifty one: living beings caught up in this progression will have a hard time not implicitly fulfilling it. So patterns propagate whether we care or not, but the _meaning of life_ will only have prescriptive value if it falls in the logical spectrum of your values. Otherwise it's just an observation about the world.

In conclusion, yay, there's a meaning of life. One catch: it doesn't necessarily mean anything to you.
