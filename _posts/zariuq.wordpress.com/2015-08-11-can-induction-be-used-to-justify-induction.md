---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-08-11 18:44:58+00:00
layout: post
link: https://zariuq.wordpress.com/2015/08/11/can-induction-be-used-to-justify-induction/
slug: can-induction-be-used-to-justify-induction
title: Can Induction be used to justify induction?
wordpress_id: 386
tags:
- blog
- zariuq.wordpress
- anti-philosophy
- goertzel
- induction
- monkey-butts
- philosophy
- principle
- Zar
---

Part of the standard "problem of induction" is that any observation-based justification of induction uses induction. Which is generally viewed as circular reasoning. That is, of course you can show A to be valid by assuming A. A -> A. If we use A in the justification of A, we'll necessarily succeed.

Is that the case with induction? Will using inductive reasoning necessarily validate induction (at least probabilistically speaknig)?

I think the answer is, surprisingly, no. Using inductive reasoning will, obviously, not lead one to necessarily concluding that induction is valid.

The first step to realizing this is that the question of whether induction is valid is actually a question about environments. (Unless you're talking about valid as a method for making predictions as compared to other methods we have... :p) If you're in a weird environment where anything that happens 500 times necessarily happens forever after, then induction will be valid in that environment (where N = 500), though you still have to assign levels of confidence if something happened 400 times... Hmm... xD   But we don't know if we're in an environment like that. So, not knowing the environment, you don't know if induction works or not. You don't know if there are regularities that can be statistically mined via induction or not, nor how much data you'll need to make OK judgments. Maybe. Maybe not. I'll call enviroments where induction works "induction-friendly."

So what happens if we try to use induction to see if an environment E is induction-friendly or not? What are the possibilities.



	
  1. **Case A**: You try induction in E and it works because E is induction-friendly.

	
  2. **Case B**: You try induction in E and it works because E just happens to look induction friendly "momentarily" (say a few decades to billions of years).

	
  3. **Case C**: You try induction in E and it doesn't work because E is not induction-friendly, too chaotic or whatnot.


These are the main possibilities. The problem ("feature"?) here is Case B: we have no way to tell distinguish between Case A and Case B.

However, what do these exprimental cases using induction say about whether we should continue using induction in E? In Case A, yeah, induction seems to be good (Induction -> Induction D'=). In case C, **induction in E is not inductively supported**, as you don't have repeated occurences of induction working. And in Case B, induction supports itself until some unknown poitn in the future.

So, obviously, using induction doesn't necessarily lead to inductively assigning a high probability to induction working in your environment. Using induction will, generally, only lead justify induction in an environment that is inductive-friendly for at least as long as you're doing your experiments. Therefore, using induction to justify induction doesn't actually lead to problems with circular reasoning.

Part of the problem, actually, is that we do seem to largely be in an induction-friendly environment (though we also have knowledge of chaotic or multicausal systems that are hard to inductively learn about :O). Therefore we think using induction will result in circularity because we only look at induction-friendly environments like our own.

As for Case B, it is a problem if you want absolute guarantees. However, if you don't, it becomes a **principle,** not a problem. And, again, the obvious answer to Case B is somewhat dull. There's no choice but to tentatively induct induction, aware that the environment may cease to be as it seems.

(And that's why I find the practical side of induction more interesting: given an environment E, to what degree of confidence can you reach inductive conclusios? What's the best way to do so in practice? Etc. Etc.)
