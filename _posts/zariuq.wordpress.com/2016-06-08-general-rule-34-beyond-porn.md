---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-06-08 09:27:17+00:00
layout: post
link: https://zariuq.wordpress.com/2016/06/08/general-rule-34-beyond-porn/
slug: general-rule-34-beyond-porn
title: 'General Rule 34: Beyond Porn'
wordpress_id: 893
tags:
- blog
- zariuq.wordpress
- goat man
- life
- philosophy
- pogo stick up mount everest
- porn
- Rule 34
- Zar
---

[Rule 34:](https://en.wikipedia.org/wiki/Rule_34_(Internet_meme)) "If it exists, there is porn of it – no exceptions".

Of course, as xkcd [points out](https://xkcd.com/305/), Rule 34 is a creative, or timeless, rule. There's almost a euphoric duty to producing porn of anything one can think that one cannot find porn of.

What if Rule 34 does not only apply to porn?


<blockquote>If it's possible, some idiot's doing it.</blockquote>


My favorite example is the [goat man](http://www.businessinsider.com/thomas-thwaites-goat-man-2016-5), who decided to take a break from being humans and live among the goats. (Now an esteemed author, of course.) Another example could be [roller man](https://www.youtube.com/watch?v=cNVslA7T2q8).

Why would this be true?
--> Because humans have a strong urge to be unique and creative.
And we have all sorts of different tastes. Mmm, mm, m.

Moreover, GR34 paints a cool portrait of humanity and life systems, ever prodding into the beyond in as many ways as we can.

By the way, I don't think anyone has pogo sticked up Everest yet.
