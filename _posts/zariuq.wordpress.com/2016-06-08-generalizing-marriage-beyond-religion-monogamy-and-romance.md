---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-06-08 08:26:24+00:00
layout: post
link: https://zariuq.wordpress.com/2016/06/08/generalizing-marriage-beyond-religion-monogamy-and-romance/
slug: generalizing-marriage-beyond-religion-monogamy-and-romance
title: 'Generalizing Marriage: beyond religion, monogamy, and romance'
wordpress_id: 865
tags:
- blog
- zariuq.wordpress
- antimonogamy
- civil union
- marriage
- polyamory
- polygamy
- Zar
---

What's left of marriage when you strip away the religion and monogamous ideals?

A commitment to live life with another;  to share property, finances, taxes, and other citizenship rights.

Sharing property, finances and taxes all go together.
The right to live somewhere your spouse does, on the other hand, reflects respect for the commitment.
The say your spouse has after you die or when sick, does not necessarily follow.

-- -- --
-- Do these have to come together?
-- Do these have to constitute a single parcel?
-- -- --

Not really.

Writing wills is standard practice, perhaps again after marriage. One may want to specify someone trusted to have primary say in an emergency anyway. Prenuptial agreements can make sharing of finances optional.

Maybe you want to commit to life together with a few best friends in addition to your lover? You want to set up special legal relationships with each one; they're all special in their own ways, right?

That's nothing like what we think of as a polygamous marriage. You don't have to always live with all of them or anything. Nor even what we think of as polyamorous relationships.
Approached in this way, and with the well-known frustration that is divorce, prenuptials should also be thought out; for this is the legal side, not something that must be done if you are caught up in romantic passion.

Of course, there are issues to generalizing marriage:
If you commit to live with 5 people, do they all get to move abroad with you?

The generalized, modular form of marriage is probably best called a **civil union**.








