---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-12-31 21:53:06+00:00
layout: post
link: https://zariuq.wordpress.com/2015/12/31/overly-ambitious-new-years-resolutions/
slug: overly-ambitious-new-years-resolutions
title: Overly Ambitious New Year's Resolutions
wordpress_id: 422
tags:
- blog
- zariuq.wordpress
- george bernard shaw
- grand goals
- hocus pocus
- New Year's Resolution
- NYR
- selfie
- vegetarian
- Zar
---

I usually try to actually fulfill my NYRs. I've managed to for the past few years, even with vague things like "get a gf" or "do things slowly and steadily" (slow and steady works wonders, btw :-D).



This time I'll be overly ambitious and silly.

I will,



	
  1. Assert my will (which ties into the whole theme of trying to put my thoughts out there more, etc.) And with the quote I always see in Ben's email, "The reasonable man adapts himself to the world: the unreasonable one persists in trying to adapt the world to himself. Therefore all progress depends on the unreasonable man." -- George Bernard Shaw

	
  2. Write/outline detailed plans/designs for my grand goals. Because, well, they're grand and hard to get started. So making things as concrete and detailed should be a good exercise. (Further, (1) is so vague (2) will need to be applied to it to come up with concrete steps to ingrain its practice in the fabric of my habit -- err, being.)

	
  3. Go vegetarian, except for one meaty meal a week. Because I want to stay used to eating meat in case I'm trapped on a desert island with you ;-). And because I'm only 80% convinced the vision of a future of creatively working with animals to enrich ourselves as much as possible in our own ways is just better, and that it's better to start voting for weaker beings in our group-manifest ethics sooner than later. (Gotta be ready to renege next year when I further my philosophy. And vegetarian, not vegan, because I'm half-assed and lazy :p)

	
  4. Take a [selfie a day](https://www.instagram.com/zariuq/). Because I'm too shy. (Yay narcissism~)




Well, I can probably do them all. Maybe I'm still aiming too low, but that's how to stick to one's word >:p

Happy new year world, let's make 2016 better than 2015 =]
