---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2013-09-23 07:43:50+00:00
layout: post
link: https://zariuq.wordpress.com/2013/09/23/writing-more-stories/
slug: writing-more-stories
title: Writing More Stories
wordpress_id: 43
tags:
- blog
- zariuq.wordpress
- getting started
- NaNoWriMo
- short stories
- stories
- writing
---

Writing is cool. I want to write more.

And yet when I start writing, I quickly decide it's not a good use of time time and give up.

I just thought of a method that may work to get me started writing again should I get the determination. Writing long epics appeals to my aesthetics more than short stories. But forcing myself to finish the story in under 3 pages would allow me to finish in one go. This could get me used to writing stories again. Definitely easier than NaNoWriMo.

Maybe I'll try it sometime.
