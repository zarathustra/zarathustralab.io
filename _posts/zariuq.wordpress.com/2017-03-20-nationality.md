---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2017-03-20 12:49:20+00:00
layout: post
link: https://zariuq.wordpress.com/2017/03/20/nationality/
slug: nationality
title: Nationality
wordpress_id: 1391
tags:
- blog
- zariuq.wordpress
- American
- Danish
- Japanese
- Nationality
- Nomad
- Zar
---

Apart from the passport, I decided to calculate my degree of nationality using a simple heuristic:


<blockquote>% nationality X = (# years spent in X) / (# years lived)</blockquote>



