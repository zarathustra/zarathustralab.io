---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-06-21 14:51:32+00:00
layout: post
link: https://zariuq.wordpress.com/2015/06/21/renaming-the-months/
slug: renaming-the-months
title: Renaming the months
wordpress_id: 383
---

I propose we start counting months by number. Some already are (September, October, November, and December). And the rest are mosty god or emperor names. Do we need to keep those and have names unrelated to the number? Not really . . .

Japanese and Chinese don't have that either. Just 1月、2月、3月。。。 (Although apparently the Chinese used to have particular names and stopped using them xD.)

So we can start too. Latin names sound cooler, so let's just count with those. We use some of them in English anyway.

I suggest with little but enough confidence:



	
  1. unatilis

	
  2. bitilis

	
  3. tritilis

	
  4. quatilis

	
  5. quintilis

	
  6. sextilis

	
  7. september

	
  8. october

	
  9. november

	
  10. december

	
  11. undecimber

	
  12. duodecimber


(If you know how to more properly deal with latin numbers... feel free to let me know.)

Anyhoo, I have half a year to figure that out :p



Or I could just do Onemon, Twomon, Threemon, Fourmon, etc.
It's easier to get started.
