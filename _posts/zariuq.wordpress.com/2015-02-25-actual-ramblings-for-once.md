---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-02-25 06:08:12+00:00
layout: post
link: https://zariuq.wordpress.com/2015/02/25/actual-ramblings-for-once/
slug: actual-ramblings-for-once
title: Actual Ramblings For Once
wordpress_id: 364
---

1:
If you can’t understand without an explanation,
You can’t understand with one*.
Said the Old Fart.
For sir, talking is but pointing,
And the blind cannot be pointed.

Should I show you a Whulpshodslof?
When you see it, you shall see.
See that you cannot,
cannot,
cannot.
See that you cannot understand.
It’s there all right.
Right in front of you.
Its glmoshmes and frigtans,
Bright as a sunflower in sunlight.
But know not what they are, you don’t.
Not flippers, leaves, stones or bones.
Where does glmoshes start and – what was it? – end?

2:
Flesh out my world.
Grow, grow, beyond expanse.
Become so grand O world of mine,
That I lose track of what you are.
Change so fluidly that interesting things happen,
Maintain coherence that you convince.
Stay, stay, go, stay and go.
Shake to the left and to the up.
Up, up, a fucked up world.
Can we, dear, love anything else?
Imagine!
Go on! Imagine it.

Who can imagine a sane world?
an interesting world?
a world without problems?
a dead world.
One that is real.
Built from scratch. A one, two, three.
Not I.
Such a world isn’t real.
Life. Is it strife?
So go on world o fine, world of mine.
Grow on, contradict thyself.
Cohere and appear,
Just don’t make sense.
It’s taboo.
Don’t disappear characters without reason,
Even reasons without ground.
Use what you’ve got.
Persist your patterns and increase your entropy.
Seems evil, but evil’s a rule.
Be strong.
Compel well.
Coopt reality.
It’s all Mentalese within.
Your cat, Frank,
And your brat, Skank,
within are the same as imaginary Bob.
Bob with his pet saber in tow.
O I’m habituated, that I am.
This write, one Sir Reality,
Gets a special pass, that he does.
So grow strong, will you, my world dear.
Be special, that you may surpass.

3:
Up and beyond,
The time has come.
Let’s go! Let’s go!
Be wary of infinity,
for finite we are.

Bit by bit,
One and one,
Zero and one.

Let’s make it up,
Make it round,
Polly, polly, pound.

A room of balls,
A space of malls,
Zoom in, zoom out.
A pace of nil.

One is the Earth.
Seven billion earths.
Utterly separate?
Perhaps; it’s debated.
Nonetheless mutual.

Kept alive against the fray,
By semaphore galore.
We call them words.
We call them senses.

Is there always only one reality?*

So they say.
Yeahno, nay.
Nay we say.
Make it my way.
I control the sway.

It’s all Mentalese,
as far as you can see.
Believe strongly,
And so it will be.

One final proof exists.
One strong anchor:
Bleed and you shall die.

One proof found lacking,
For where you die
Is but where we share.

You are a pattern,
and patterns persist.
So on you march,
in words apart.

4:
Draw, draw, draw with words.
“A picture is worth a thousand words,”
But that’s nothing.
A small picture, my friend,
Is worth ninety thousand pixels.
How’s that for size.
Say, does that make a word 90?

That’s wrong.
A word is closer to infinity.
Abstraction is rich, small yet grand.
Pictures may not compress,
but can they grow?
Ho ho, Escher’s can!
They can do both.
“Cow.”
O the profundity.
“Cow.”
O you reader, “cow.”
How many cows can you imagine?
How many cows can you visualize?
Immensely numerous cow variety.
Hordes, hordes around the earth.
Blue cows, fat and thin.
Green cows, happy and mad.
O how many cows there are.
“Cow.”

5:
And then there was a bunny.
The cutest bunny you ever did see.
With villainy in its eyes,
And sweets in its teeth.

Lord bunny smoked a lollipop,
And thumped on rock candy.
Lord bunny, you see,
Just conquered Candy Land.

He was not lucky,
But strong.
The other competitors,
drew cards.
How silly could they be?

Lord bunny knew better.
With no hesitation,
He jumped on Candy Land,
Sending everyone helter skelter.

The war was over.
There he sat triumphant,
lollipop in mouth.
This ends Lord Bunnys story.
On it shall live in his heart.

6:
Another bubble, another bunny.
Another bunny, another funny.
Another funny, another day.
Another day, another way.
Another way, another story.
Day by day.

Come up hither,
Firm on your bubble,
And blow it away.
Blow many bubbles.

This is the bubble-hopper.
He shimmers on many,
And stays in none.
He travels by bubble blown,
And in bubble he doth live.

Enemies? He has a few.
Pokers, you’d think.
A poked bubble pops.
Pops. Rejoice.
A poked bubble pops into bubbles more.

Ahh, what’s that?
It’s getting hot.
… Fare thee well, my friend.

* Two phrases are stolen from 1Q84
