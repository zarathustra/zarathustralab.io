---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2013-08-31 01:26:50+00:00
layout: post
link: https://zariuq.wordpress.com/2013/08/31/3-goals-women-world-domination-and-life-creation/
slug: 3-goals-women-world-domination-and-life-creation
title: '3 Goals: women, world domination, and life creation.'
wordpress_id: 6
tags:
- blog
- zariuq.wordpress
- AGI
- AL
- goals
- life creation
- purpose
- Singularity
- women
- world domination
---

After a bunch of rambling I seem to have 3 main goals.  I'd love to combine them into one, but that seems difficult without getting even more general.

**Goals:**



	
  1. Get better with women.  (And offline friends.)

	
  2. Better organize people via expansive e-infrastructure.

	
  3. Make new life-forms, leading us into the Singularity.


**Comments:**

So, it was hard making that first one sound good.  I'm tempted to throw in many things about enjoying my current life as much as I can while pursuing the other two, but I'm doing a fair job of that except with regard to women.  So they don't get mentioned up there.

2 comes from World Domination.  What would I do if I ruled the world?  Apart from the expanded 1 and 3, this is what I'd do.  "Fake it till you make it."

3 boils down AI.  That's what digital life forms are.  AI.  Uploading humans into new, more flexible forms of existence may work too; this counts as new life-forms. By all means, correct me if I'm wrong, but the upgrading humans approach seems to be a very incremental, step-by-step process.  One that depends largely on scientific and technological advances.  That just doesn't feel right.  It feels harder to aim for.

I'd really love to combine 2 and 3 into one.  Advancing 2 a lot without 3 will be really good, and may set the stage for a more positive Singularity.  However, somewhere down the line a sort of flexible intelligence will have to be introduced to the equation to get there. It's possible a use of extensive narrow AI in 2 could morph into a global brain and get us there.  Possible.  But, again, doesn't seem like something one can aim for.

And there you have it: 3 goals.



I find it interesting how many more ideas I have on this topic as soon as I actually decide to act on my desires.  I've spent ages pondering purposes and grand goals. And then I decide, YARGH, I'm acting on them.  Next thing I know my head is full of related ideas and I quickly gain more clarity than I'd had before.  Of course, no supreme certainty or anything.

So, if you're seeking a 'purpose' to get behind, perhaps you have to decide to work towards that purpose whatever it may be.  The chicken does come before the egg.  :O
