---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-12-26 11:18:53+00:00
layout: post
link: https://zariuq.wordpress.com/2016/12/26/basic-exercise/
slug: basic-exercise
title: Basic Exercise
wordpress_id: 1114
tags:
- blog
- zariuq.wordpress
- exercise
- lazy
- plank
- quantified donkey
- Zar
---

I'm sure y'all have seen the 7-minute workout routines.
Staying fit is necessarily for health; walking a lot isn't quite sufficient.

Yet I'm too lazy for those 7-minute routines.

Sadly, an implication of [this study](https://www.sciencedaily.com/releases/2016/07/160712094259.htm) would be that exertion to fatigue, to the point of failure is precisely what's necessary, in some form or another.

The plus side? It's really easy to physically exert oneself almost anywhere: the only bottleneck is pain-avoidance. (And I hear that gets better . . ..)

So just bike faster, power walk harder, run to the bus, take a short break to hold yourself up on your seat, push a (sturdy) wall, or whatever. :P
Meanwhile, inspired by my daily language shit (that I've lost energy for) and seeing Ben's more thorough morning routine, I figured I could, at least, do the plank daily.

You can see my progress [here](https://docs.google.com/spreadsheets/d/1bN3GLInLNG_7om5450KqLLT6NuB-Bl65XkIHAs31I7A/edit?usp=sharing).
![exercise](https://zariuq.files.wordpress.com/2016/12/exercise.png)

I'm nearing the two minute mark ;-D.
When I hit it, I'll have to consider mixing other stuff in more.

I feel my physique slowly changing. Benefits seem better when I get some other kind of exercise (like running to the bus, dancing, or biking). Lately I've been doing a small downward dog afterwards as a counterbalance stretch.
