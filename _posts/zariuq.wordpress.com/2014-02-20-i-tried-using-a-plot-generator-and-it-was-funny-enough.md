---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2014-02-20 21:04:43+00:00
layout: post
link: https://zariuq.wordpress.com/2014/02/20/i-tried-using-a-plot-generator-and-it-was-funny-enough/
slug: i-tried-using-a-plot-generator-and-it-was-funny-enough
title: I tried using a plot generator and it was funny enough
wordpress_id: 256
post_format:
- Aside
tags:
- blog
- zariuq.wordpress
- funny
- generator
- plot
- plot generator
- queer romance
- romance
---

# There's something about Brad
- a queer romance




## by Qono Vuor






Don Hank is a puny, pale and splendid police officer from the city. His life is going nowhere until he meets Brad Jones, a curvy, slim man with a passion for booze.

Don takes an instant disliking to Brad and the deranged and creepy ways he learnt during his years in England.

However, when a zombie tries to squash Don, Brad springs to the rescue. Don begins to notices that Brad is actually rather virtuous at heart.

But, the pressures of Brad's job as a actor leave him blind to Don's affections and Don takes up hiking to try an distract himself.

Finally, when arrogant computer programmer, Suzanne Ferguson, threatens to come between them, Brad has to act fast. But will they ever find the queer love that they deserve?

(From http://www.plot-generator.org.uk/)


