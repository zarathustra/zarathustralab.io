---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-09-02 07:54:31+00:00
layout: post
link: https://zariuq.wordpress.com/2015/09/02/whats-a-good-way-of-thinking-of-expenses/
slug: whats-a-good-way-of-thinking-of-expenses
title: What's a good way of thinking of expenses?
wordpress_id: 398
tags:
- blog
- zariuq.wordpress
- everyday economics
- expenses
- mass distribution
- monkey bladders
- question
- scale
- Zar
---

For example, a pair of 24$ pants that last 4 months might be .75$ per day of use (if it's durable and lasts 2 years, then it's down to .125$ per day of use :D). Choose wisely, and you're paying less than a dollar a day on clothes.

A 1000$ laptop that lasts 3 years? That's about .9$ per day for your laptop xD.

(With the small problem that you have to make the investment/gamble all at once, etc :P)

Taking the reverse view, food is damn expensive (trumping cheap rent D'=). Just 5$ a day on food is already 5475$ for 3 years, 15$ a day would be 15425$. 'Small' differences in how much you spend make a fairly significant difference in the long run (depending on how poor you are :P)

Which just reminds me why it's 'so easy' to get rich with economies of scale: a .01$ profit on 200 million sales already gives you 2 million. Differences customers won't even care about, are a matter of millions in revenue (u_u).

So clearly time and scale of use have to be taken into account, not just the raw $$ amount. What else needs to be taken into account when thinking about expenses?
