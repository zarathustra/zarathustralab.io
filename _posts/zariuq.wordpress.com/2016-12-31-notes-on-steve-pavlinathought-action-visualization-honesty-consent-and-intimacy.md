---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2016-12-31 18:18:23+00:00
layout: post
link: https://zariuq.wordpress.com/2016/12/31/notes-on-steve-pavlinathought-action-visualization-honesty-consent-and-intimacy/
slug: notes-on-steve-pavlinathought-action-visualization-honesty-consent-and-intimacy
title: Notes on Steve Pavlina:Thought-Action, Visualization, Honesty, Consent, and
  Intimacy
wordpress_id: 1136
tags:
- blog
- zariuq.wordpress
- Action
- Approach Anxiety
- Law of Attraction
- life
- Personal Development
- Relationships
- Responsibility
- Steve Pavlina
- Thought
- Visualization
- Zar
---

In planning for my New Year's Resolution of freeing my ass this year, I sought mindset/perspective changes that would make it more easy than "just grab your balls and do it".

I found my way to the blog of Steve Pavlina (who claims more fame than he has). He details many points of view and insight that do make taking free action more normal ('easy').

Note, to prepare you for much cheesiness, reviewing the sort of philosophy presented in [Model Zero](https://www.goodreads.com/review/show/1425721299?b). That is, consider models of self and reality by their use and effects, not just by their accuracy.

[~~)](http://www.stevepavlina.com/blog/2005/02/thought-vs-action/) We all know that Action leads to motivation. But what relation do Thoughts and Beliefs have with Action?

I have often griped that my actions do not correspond to my philosophy: I believe X, Y, and Z to constitute RA (Right Action) wrt (with respect to) my Values, yet my Action differs.

I love to break it to myself, but this perceived Belief-Action divide is false!
Take any example where I don't take (Zar defined) RA, and one can find Thoughtplexes and Beliefplexes contrary to the RA.
For example: longer periods of focus without multitasked chatting online is better, and I Desire a different ratio of online to offline socializing. Yet I have Belief-based compulsions to respond to people quickly; the FOMO (feel of missing out) on what's said in various chatrooms; the urge to comment on, narrate, and discuss various thoughts that come up in the process of what I do, etc.
That is, whoa, my Thoughts have not even reached congruence despite my Philosophically-oriented Thoughts having reached conclusion. There is no Thought-Action divide, just muddled, contradictory thoughts.

What can be done about this?

[~~)](http://www.stevepavlina.com/blog/2006/08/creative-observation/) Take 100% responsibility for your thoughts and beliefs.

Also an idea Mark Manson basically brought up in [his book](https://www.goodreads.com/review/show/1764789662) and various blog posts: take complete responsibility for every single little action you take, not just the 'big actions'. They're more important than you suspect.


<blockquote>belief x thought --> action
action --> belief x thought</blockquote>


Just doing it, with courage, and exposing oneself to RA can nudge one's beliefs and thoughts into congruency.
Or one can through conscious effort overpower incongruent thoughts and beliefs, or find compromises in them.
This can also be claimed to be an ancient Buddhist principle, and of no surprise, Meditation also helps with this, in addition to helping one not take various incongruent thoughts as _seriously_.
Are there other ways of changing one's beliefs? Likely. I'll test out the Lefkoe method.

Ex 1) Fear often exemplifies both desiring and rejecting X.
Desire motivates me to seek X.
Fear motivates me to avoid X.

Incongruent thoughts pre-action.

Can one simply eliminate fear? Not easily.
One needs courage.
To aid courage, one can also [embrace the fear](https://www.youtube.com/watch?v=ivKpzESQNwc) and accept both the feeling of fear as a part of the path to one's desired outcomes, and the feared consequences as acceptable outcomes.

[~~)](http://www.stevepavlina.com/blog/2009/11/how-to-visualize-your-new-reality/) Complete Visualization
An interesting meditation-esque method proposed, which I haven't field-tested yet.

Do you desire to be or do something that you don't currently? What could life be like when you achieve some goal?
Sit down for 20+ min to visualize your daily life when you live in the way you want to. Fill in the details to make it realistic. Include the pros of, say, pursuing your desires without shame or living by your personal philosophy; include also the cons: will your day not include much TV watching? Ohnoes!
Will all your current friends be there? Will you have new ones? Will you have a different sort of lover? What exactly are they like?

Take the imagination a step further and see what your desired life feels like. What are your base emotional states? How often are you excited? In Flow? Worried?

Now you can go about your day much more aware of the differences in your current lifestyle and a lifestyle fulfilling your desires.

Ex 2) Own your preferences. Forge yourself to be [shameless, fearless and guiltless](http://www.stevepavlina.com/blog/2012/10/shameless-fearless-guiltless/).
And trust humanity to not let you be truly evil? >:p

As a non-religious person, if a theist moralist despises my actions or desires, I'll just shrug. Fuck them. In some countries or social environments, not publicly posting my views is not only wiser, but safer. Yet I feel no shame or guilt about them. A devout religious person may feel strong shame at his desire to masturbate, yet I plain and simply feel none.

On the other hand, I experience the infamous [Approach](http://www.stevepavlina.com/blog/2012/01/approach-anxiety/) [Anxiety](http://www.stevepavlina.com/blog/2013/10/you-dont-need-to-overcome-approach-anxiety/), which encompasses, for me, basically any sexuoromantic expressions of desire/intent to women, the objects thereof. I believe expressions of intent are okay and the only way; I also held many beliefs discouraging open expression -- many of which are actually false :o!

Thus own your desires and embrace them.

[~~)](http://www.stevepavlina.com/blog/2010/02/broadcast-your-desires/) Step Two: broadcast your desires
Now that you've accepted your own desires, as _strange_ and _perverted_ as some moralists may perceive them, speak up. Show up. Let those around you and the world know what you desire. How can people help you or those you desire to do things with connect with you if you don't even let them know.?

~~) Rejection --> Incompatibility.
Related to the lovely "Fuck Yes or No" principle.
If you could know who and what is compatible before the fact, perfect, you'd rarely ever have rejection. Alas, we don't, and have to use our noodly appendages to feel out.

[~~)](http://www.stevepavlina.com/blog/2005/09/trial-and-error-ego-and-awareness/) Trial and Error
One often doesn't know exactly what will work -- or even what one wants.
One just has a vague sense of what direction one wants to explore in.
Thus try shit out and experience for yourself what results. Good? Bad?
Will you run afoul unpleasant experiences? Likely.
Unexpected delights as well.

What about respecting other people?

[~~)](http://www.stevepavlina.com/blog/2009/01/conscious-sexuality/) Consent is a Golden Rule
As ye all know, I'm philosophically amoral, as my Values are not really 'morally' grounded.
So if you value respecting others, I'd say **consent** is the primary principle.

As long as those involved consent (and you won't get caught if it's illegal), try whatever you're curious about!
An interesting catch-clause to consent as a basic principle: you can ask for anything.

[~~)](http://www.stevepavlina.com/blog/2006/09/making-decisions-that-stick/) "What should I do?" --> "What do I want to experience?"
I've been in favor of ridding 'should' from my language for a while, as it's an ill-founded concept.
'Should' needs to be grounded in some metric, and can often be replaced with "better" or "want". This can be a good swap as well.

[~~)](http://www.stevepavlina.com/blog/2007/07/the-abundance-mindset/) "Scarcity" and "Abundance"
While these are cheesy concepts, there are some interesting insights lurking behind.

Basically, one with a scarcity mindset/vibe will take any and all perceived opportunities much more seriously. These could be the only option they have!
One will be quite likely to experience anxiety and strong fear of failure.

Yet with an abundance mindset/vibe, one will be aware that there are sufficient options out there in the world. All one has to do is look out and find them.

[~~)](http://www.stevepavlina.com/blog/2012/03/how-to-create-intimacy/) Intimate Relationships
Getting away from the idea that all of one's friends and lovers must be fully compatible and satisfy all of each other's needs, one can think of different types of intimacy: **physical**, **mental, emotional, and spiritual/supportive. **
I appreciate good compatibility in any intimacy dimension, however 3-D and 4-D relationships are those more fulfilling ones.

Also advised making explicit the type of relationship you seek with others.
For example, fuck time-wasting and hesitation: just ask friends / girls what their physical boundaries are, so that you can get cuddly without any doubt or second-guessing!

[~~)](http://www.stevepavlina.com/blog/2011/06/inspired-relationships/) Your relationship with Life
Relationship Anarchy is pretty cool: eschew labeled relationships and simply relate to people as you both see fit.

This idea kinda goes another step: take what you want out of life and what you want to give life, and pullback beyond the individuals you specifically connect with.
To what degree does it matter who serves you fries?
Who goes bowling with you? Etc.
Sure, a coffee date is more fun with someone you know well, ideally connecting 3-D or 4-D, allowing full enjoyment of your time together drinking coffee.
But does it really matter who that is?

Not really.
(Ahh, except for those charmingly unique individuals with qualities you just can't get elsewhere. Muahahaha >:P)
I've always felt I could basically get along with anyone -- but then there are 7 billion people, so I don't really want to get along with everyone. They may also be boring other than the fact that we get along (is that just an emotional/supportive bond?)

As it doesn't matter much, just enjoy your relationship with life. Seek the relations you desire with life and worry not where they come from.
As far as individuals go, some will stay for a long time, and others will be with you momentarily. Rejection? That's just not where you'll relate with life from.

[~~)](http://www.stevepavlina.com/blog/2006/08/the-law-of-attraction/) Law of Attraction
The infamed law of woo-woo. This is why I mentioned Model Zero.
Sure, maybe there are hitherto mostly-unknown ways in which the LoA physically works outside your mind. Let's set those aside for now for us skeptics.

Could adopting the LoA be useful to a traditional _physicalist_?
Yes.

Why?

Because for the LoA to "work", you need to maintain consistent, congruent focus and intent on what you want in life.
This could easily marshal your thoughts, attention, and unconscious.

Expecting "synchronicities" or signs that your intention is manifesting in this world, could easily bias your perception to notice opportunities!
And guess what, not taking opportunities is basically an anti-intention. Boo-yah, extra motivation to take action.

So, regardless of the woo-woo reputation, the LoA could definitely be a useful mindset to adapt. Ouch.
