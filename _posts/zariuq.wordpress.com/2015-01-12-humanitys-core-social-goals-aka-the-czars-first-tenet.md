---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-01-12 12:52:45+00:00
layout: post
link: https://zariuq.wordpress.com/2015/01/12/humanitys-core-social-goals-aka-the-czars-first-tenet/
slug: humanitys-core-social-goals-aka-the-czars-first-tenet
title: Humanity's Core Socioeconomic Goals (AKA The CZAR's First Tenet)
wordpress_id: 358
---

What should humanity's basic goals for itself be? Said another way, what principles should humanity take into account when forming its socioeconomic systems?

I ran into a proposal for such a global principle this weekend. It's an ethical combination of utilitarianism and the golden principle. Basically, "**our global philosophy should be that we should try to make the world as happy as possible**" ( http://edge.org/conversation/deep-pragmatism ).

Pretty hard to disagree with, right?
Does it remind you of CZARism? Perhaps not, but it should.
This is equivalent to the [first tenet of CZARism](http://jagatsahaya.weebly.com/): "**we should act for the maximum benefit of all present and future sentient beings at all times.**"
Nor does CZARism claim to be original here. (The next two tenets supplement the first.)

At a glance, this global philosophy seems overreaching and sentimental. Why should I be responsible for the well-being of everyone else?! That's their own damn problem. Oh the world will be dragged downwards if we have to care for everyone.

And the issue gets shoved under the rug there.

What are the implications of being against this global philosophy?

The weak stance is that we should try to make the world reach a base level of well-being. We shouldn't let people horribly suffer, starve or anything if we can help it. But beyond that, it's their life. This is all good and fine. Whether nosy busybodies are appreciated/helpful or not is debatable ;-) [They'll say they're often helpful whether appreciated or not. Hah!]

What about the strong stance though?
The strong stance is that we have no 'responsibility' to even help people reach a base level of well-being. What happens to those who can't manage to do well for themselves? This could be for many reasons. Perhaps they are simply unskilled. Or perhaps they have trouble due to circumstances (and the list of possible causes is too long). Well, they suffer.

Yes, that's what the rejection to a version of this global philosophy (The CZAR's first tenet) amounts to: **we should let some people suffer**.

I'm not saying there's anything wrong with this. There is virtue in natural selection. Let those who don't prosper fade away. I don't think we're very honest about it when we essentially make survival of the fittest our choice for global philosophy though.
(This is why the CZAR is a religion: choosing the well-being of the world as our global philosophy is largely taken on faith, whether we think so or not :p)

Of course, in the real world we have an ugly conglomeration of both philosophies. We can't quite let people suffer, but we can't decide to help them either. And aid ends up being a hacked on attachment. Makes sense though. As said in the article: we instinctively feel morally responsible to help those in need _when we see them up close_, and we don't care much otherwise. Very contradictory. If you set aside the issue of those being closer being in a better position to help, one should _rationally_ choose to either help or ignore those in need in _both_ circumstances. But who ever said humans are rational beings?

The remaining question is whether making a choice one way or the other as a species (or at least as a society :p) will help. Whether it will be more effective. I think the answer is yes to both cases, which represent rather different goals for humanity.

This is because the global philosophy can be woven into our social economic system instead of being an aside. As is, we have our socioeconomic system and then we have ways to help those who would be suffer because we just can't quite leave them alone, though we really want to.

If you choose survival of the fittest, well, you can just ditch aid for those who are suffering. I'll give you the benefit of the doubt and assume you compassionately try to make a system many can participate in. No guarantee everyone will succeed, but let's hope as many as can do. So song as you can fend for yourself without additional burdens. Many things can probably be better streamlined when you just let some people go. Will more be let go as we approach the Singularity and technological unemployment? Perhaps, but that's just the way of the universe ;->

If you choose the well-being of all, you're looking at quite a different culture. Free markets are great for setting prices (though arguably not sufficient). However, setting up the economy so that peolpe can eke out a living on their own is no longer the only primary goal. As a part of the distribution problem, we'll have to work in ways to get sufficient goods to everyone who needs them. I'm a fan of _basic income_ esque methods, but I'm sure there are other alternatives :). Ultimately, this too should simplify and streamline our economic system. And have reaching social effects too.

I wonder what social effects this global goal would bring about. I suspect there are many :).

So, which do you prefer as a core principle for humanity? Survival of the fittist or well-being for all humans?
