---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2013-09-23 14:51:40+00:00
layout: post
link: https://zariuq.wordpress.com/2013/09/23/the-meaning-of-life/
slug: the-meaning-of-life
title: The Meaning of Life
wordpress_id: 46
tags:
- blog
- zariuq.wordpress
- Burger King
- commonsense
- life
- logic
- meaning
- Meaning of life
- meaningless
- Mr. rock
- pointless
- rock
- technological evolution
- universe
---

Okidoki. I'm sick of people talking about how there isn't really a meaning of life. I can't blame them; that is the current popular view of the matter. Throw away religious meanings, introduce relativism, and then how we're but a tiny speck in the universe is the final straw. Boom. There's just no meaning of life. Yes, it's easy to reason like that. But there are meanings of life. I will construct one here to show you. Maybe there are others. Maybe there are better ways to construct this. But I came up with this construction a while back and personally considered it solved -- one solution, that is.

I came up with the idea while thinking a lot about life and the universe. I was in a depressive state after a break-up and wondering about values. I tried to reason out my base values using some logic formalism I pulled out of my ass, but this failed for the obvious reason: logic depends on your assumptions, so I have to start with something to base the values off of anyway. Oh, crap. Well, that got me thinking about the universe and the ever-continuing progressions in it. Thinking about what we have, what our values can be based off of. This leads to one meaning of life.

Mind you, I've seen ideas very similar to this elsewhere. Maybe they just didn't bother calling it the meaning of life. So, the idea isn't entirely original. But, so what. If you see it elsewhere, you'll read this and think, "yep, that it is." That's good. Maybe you'll have some interesting interpretations. And if you don't, I'm glad to show it to you.

And, without further ado, let's start.

**The Story Leading to Meaning of Life:**

Imagine the Universe. The spreading out universe, stars forming, exploding, many different forms of matter solidifying. The universe has spawned many varied and interesting forms of matter. While very slow on our human terms, this progress keeps going, does it not?

Life comes into the picture here. Starting small, life keeps up the progression of spawning various interesting forms. Actually, life embodies this process by reproducing and self-becoming new forms. Life protects itself and regenerates too. That is, it resists continual degradation, unlike Mr. rock.  Mr. rock just degenerates when exposed to the environment; although, the environment can produce new rocks too. Anyway, life makes this process of the universe more robust and progressive.

Life can then be seen as a continuation of a basic pattern of becoming in the Universe onto the next level. A level where the pattern has become self-conscious, but continues nonetheless. And now that biological evolution is slower than technological evolution, this basic Universe process is on the verge of going up a level again. Life brings about neat new ways of organizing the forces of the universe and continuing the process, faster too.

This is a meaning of life. This is a way that life fits into a 'big picture'. This is an idea that can be used to ground our values in what we perceive as reality. This can be used to help guide you if that's what you want out of 'the meaning of life'. But, there's a lot of room for interpretation, so don't expect much help. This may even encourage you to not wipe out the human race, so this may even encourage good morals/ethics. The meaning of life is continuing this basic Universe pattern of .. -- this is where I'm lost for words. I introduce it by a story because it's easier to grasp intuitively than explicitly._
_

This idea seems very commonsensical. It hardly says more than, "The meaning of life is living." But, it does say a bit more. It gives you a guiding principle beyond "just living because that's what life does." Once you grasp this process of the Universe, you get a sense for where life is headed, and then you get a whole spectrum of meaningful activities aiding this process.

For example, inventing the internet contributes a lot to organizing our small corner of the universe more interestingly and making way for new nebulous forms; you're activity meant a lot with respect to the meaning of life. But, then again, just working at the local Burger King, sustaining the status quo and making things run smoother for others also helps advance things -- but less. Meaningful, yes; but not as meaningful. Now what if our Burger King worker keeps up to date with new forms (technological, ideological, or whatnot), and helps with their spread throughout our corner of the universe? Then, lo and behold, our beloved worker's contribution to the progression life is a part of has increased.

There is a lot of room for interpretation. We can postulate how much different ways of living work towards the progression of this meaning of life. And, more importantly, this meaning of life applies to people in all walks of life, not just scientists/politicians at the helm of humanity's ship. This meaning of life gives you pointers as to how to contribute no matter your cog in the machine.

Well, that's my answer to the meaning of life problem. Maybe there are other answers too, but they don't invalidate this one. Who says we can't have more than one. I do say that not anything goes though :P. It could be worded better and further refined.

If you have any interesting interpretations, better ways to word things, or know of someone else saying something _the same thing_ about the meaning of life in a better way, please share. :)
