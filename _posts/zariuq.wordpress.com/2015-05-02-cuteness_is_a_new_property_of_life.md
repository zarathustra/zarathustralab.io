---
navigation: True
subclass: 'post tag-test tag-content'
logo: 'assets/images/ghost.png'
author: zariuq
comments: true
date: 2015-05-02 14:17:28+00:00
layout: post
link: https://zariuq.wordpress.com/2015/05/02/cuteness_is_a_new_property_of_life/
slug: cuteness_is_a_new_property_of_life
title: Cuteness is a new property of life.
wordpress_id: 372
---

So, my chosen 3 grand goals are: live happily ever onward, lead/better organize the world, and create (digital) life.

I have good ideas now for a prototype of some methods for #2

I'm less certain about what "digital life" even means (and what distinguishis this frm various forms of AI, AGI and Autonomous Agents).... Hey, once we flesh this out, perhaps we've already got a plethora to be taxonomized ;-)

The usual "properties of life" go something like this (taken from [here](http://phoenix.lpl.arizona.edu/mars141_content.php)):


<blockquote>

> 
> 
	
>   1. Order
> 
	
>   2. Reproduction
> 
	
>   3. Growth (development)
> 
	
>   4. Metabolism (energy use)
> 
	
>   5. Realtime Adaption (Responsiveness)
> 
	
>   6. Evolutionary Adaption
> 

</blockquote>


The way these properties play out and constrain digital life is surely different.

**Order** is given. Then again, even rocks are ordered.... However, Life in the digiverse is likely to be more on the software level. This makes the constituents of digital life very course-grained compared to the molecules of organic life.

**Reproduction** digitally is so simple that people want to disregard it as important (reference needed :p). Heck, the ease of digital reproduction revolutionized media, and will go on to revolutionize biological reproduction too :>.

The catch is that we don't want to let programs rampantly reproduce in digiverse (the digital world). And we're pretty strong predators. We do a good job of clamping down on viruses.

So reproducing is easy enough, but doing so meaningfully in this tough environment isn't.

**Growth** is common in the digiverse, but usually at human rather than intrinsic hands. Intrinsic growth can be done, albeit getting it effective (at what?) is a difficult engineering task.

Growth faces a similar issue to Reproduction: we don't want cute little life-forms growing all over our digiverse. We'll treat them like weeds.

**Metabolism** is quite different, and will likely be a guiding force in interpretations of practical digital life. Life can be said to center on getting energy. Likewise, my best guess is that digital life's metabolism will be centered on getting processor time. Directing processor cycles into the other properties.

The interesting question is how digital life will come by processor cycles. Plants get energy from light. Animals get energy from other plants and animals. Digital life will get energy from human attention?

Or perhaps through mimicking other processes, such as, yeck, viruses masquerading as anti-virus software... Ut-oh :p

**Realtime adaption** means that the digital life form can change how it responds to stimuli in the environment. Ice is responsive, it responds to heat in the environment by melting, but it can't change that response. Building reliable adaptability into software seems another difficult engineering task.

The extent to which humans have realtime adaption is debated.
Perhaps we simply have more nuanced responses than ice, not necessarily more _adaptable_ responses. On the other hand, once your responses reach a certain sophistication, perhaps they can result in nearly any action (like a universal response machine xDD).

**Evolutionary Adaption** isn't quite necessary. The argument goes that in the long run, you need reproduction and adaption, or your species will die and "cease to be alive." However a being with strong enough realtime adaption wouldn't really need this. Furthermore, I think this is a corollary of reproduction and a harsh environment.

As with humans, evolutionary adaption may be deemed _too slow_. It's faster to make a new version ourselves than to wait for evolution to find a way, whether the life be digital or organic >:D

 Yeah. I suppose _realtime adaption_ is a better time.
The general message here seems to be that appealing to human amusement and attention will be vital for the survival of digital life! 

There's no room for anything else.

The same problem faces organic life. Plants and animals that interfere with humans too much are purged, though not completely. Damn weeds. (Damn computer viruses!) And the necessity of human approval for some of these processes of life seems a bit artificial, but that's the same in organic life in many areas nowadays. Farmers often buy seeds instead of recycling them from past crops. We breed "pure bred" dogs. Etc. There's still much more _wild land_ for organic life.

But, in a sense, the harshness of the environment controlled for reliability that digital life faces is similar to that organic life will increasingly face. The situation of digital life awaits organic life. As we fill up the world, where do the rest of the animals (microbes and other life forms) fit in? Do we create wildlife preserves? Like virtual worlds for mock-organic artificial life to grow in (one could create other kinds of virtual worlds too :>). 

That, or life will find ways to appeal to humans. Like pets and farm animals/plants.

We must note that there is (relatively) very simple organic life. And even there where to make the cut-off between life and non-life is tricky. To what extent does it need to grow and adapt? Are bacteria as "alive" as dogs? Should reproduction really be necessary? Kind of...

Likewise, to what extent each property is necessary for digital life is tricky. To what extent does it need to be _intrinsically_ motivated? Is this just a design choice? 

However, the need to appeal to humans adds some clarity to these constraints. Ultimately, we have to add _cuteness_ or _utility_ to the requirements for (digital) life to make the other requirements plausible among humans.
